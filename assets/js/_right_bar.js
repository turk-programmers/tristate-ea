/*Movable bar*/
var holder;
var topSpace = 10;
var minFooter = 0;
var startTop = 0;
var screenHeight;
var curTop;
$(window).load(function() {
    holder = $('#ea-callbox');
    startTop = holder.offset().top;
    $(window).unbind('scroll').scroll(function() {
        screenHeight = holder.parent().offset().top + holder.parent().height();
        if ($(document).scrollTop() >= startTop - topSpace && holder.parent().height() > holder.height()) {
            if (($(document).scrollTop() + holder.height() + topSpace) >= screenHeight - minFooter) {
                holder.css({
                    position: 'absolute',
                    //top:(screenHeight-minFooter-holder.height()-startTop)+'px'
                    top: '',
                    bottom: '40px'
                });
            } else {
                holder.css({
                    position: 'fixed',
                    top: (topSpace) + 'px',
                    bottom: ''
                });
                curTop = holder.offset().top;
            }
        } else {
            holder.css({
                position: 'static',
                top: ''
            });
        }
    });
    $(window).scroll();

    /*
     * Back link to past step
     */
    $('.ea-callbox-step.back_link').click(function() {
        location.href = root + '/' + $(this).data('url');
    });
});

/*
 * Btn event
 */
$(function() {
    $('.ea-bnt.btn-back').click(function(e) {
        $('#mainform').validationEngine('detach');
        $('#notvalidate').val(1);
        $('#next_form').val($(this).data('prev'));
        $('#mainform').submit();
    });
    $('.ea-bnt.btn-authstd-print').click(function() {
        $('#mainform').validationEngine('detach');
        var print_flag = $('<input type=hidden name="print_flag" value=1 />');
        $('#mainform').append(print_flag);
        $('#mainform').attr('target', '_blank');
        $('#mainform').submit();
        $('#mainform').attr('target', '');
        print_flag.remove();
    });

    $(document).on('click' , '.memberAlert' , function(){
        var $this = $(this);
        $.colorbox({
            inline : true ,
            href : '#noticeMember' ,
            width: 500,
            heihgt: 500,
            scrolling: false
        });

       // $this.toggleClass('memberAlert');

    });


});
