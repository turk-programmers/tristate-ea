var update_select;
var page_tproc = 0;

function changeLogoTo(type) {
    if (type) {
        $('.creditcard-logo > img').each(function() {
            if ($(this).data('type') == type) {
                $(this).attr('src', $(this).data('src'));
            } else {
                $(this).attr('src', $(this).data('gray'));
            }
        });
    } else {
        $('.creditcard-logo > img').each(function() {
            $(this).attr('src', $(this).data('src'));
        });
    }
}
function ccAccepted(accepted) {
    if (accepted) {
        $('input[name="ccnum"]').css('background-image', 'url(' + root + '/assets/images/tick.png)').attr('data-format', 'correct');
        $('input[name="ccnum"]').validationEngine('hide');
    } else {
        $('input[name="ccnum"]').css('background-image', 'none').attr('data-format', 'wrong');
        ;
    }
}
/*function disableForm(){
 $('.graymask').css({
 width:$('.graymask').parent().width()+'px',
 height:$('.graymask').parent().height()+'px',
 display:'block',
 }).parent().css({
 opacity:0.5
 });
 $("#mainform").validationEngine('hideAll').validationEngine('detach');
 $('input', $('.graymask').parent()).val('');
 $('select', $('.graymask').parent()).val('');
 ccAccepted();
 changeLogoTo();
 }
 function enableForm(){
 $('.graymask').css({
 display:'none',
 }).parent().css({
 opacity:1
 });
 $("#mainform").validationEngine();
 }*/
$(function() {
    /*$(".checkbox-option").click(function(){
     if($(this).hasClass('checkbox-option-select')){
     $(this).removeClass('checkbox-option-select');
     enableForm();
     $('input[name=insurance]').val('');
     }else{
     $(this).addClass('checkbox-option-select');
     disableForm();
     $('input[name=insurance]').val('1');
     }
     });*/
    $('input[name=ccnum]').validateCreditCard(function(result) {
        $('#cctype').val('');
        ccAccepted();
        changeLogoTo();
        if (result.card_type != null) {
            //console.log(result.card_type.name);
            changeLogoTo(result.card_type.name);
            $('#cctype').val(result.card_type.name);
            if (result.length_valid && result.luhn_valid) {
                ccAccepted(true);
            }
        }
    }, {accept: ['visa', 'mastercard', 'discover', 'amex']});
    $('.chkcopybillingaddress').click(function() {
        if ($(this).attr('checked')) {
            $('input[name=ccaddress]').val($('input[name=ccaddress]').data('copy'));
            $('input[name=cccity]').val($('input[name=cccity]').data('copy'));
            $('[name=ccstate]').val($('[name=ccstate]').data('copy'));
            $('input[name=cczip]').val($('input[name=cczip]').data('copy'));
        }
    });
    // $('input[name=payfor]').each(function() {
    //     $(this).click(function() {
    //         var obj = $(this);
    //         /*
    //          * Send ajax
    //          */
    //         wattingState();
    //         page_tproc = getTimeNumber();
    //         update_select = $.ajax({
    //             url: root + '/payForChange/' + obj.val(),
    //             data: $.param({
    //                 tProc: page_tproc
    //                         //sets:JSON.stringify(sets)
    //             }),
    //             beforeSend: function() {
    //                 try {
    //                     update_select.abort();
    //                 }
    //                 catch (err) {
    //                 }
    //             },
    //             success: function(res) {
    //                 res = JSON.parse(res);
    //                 if (res.error_code == 'seslost') {
    //                     location.href = 'packages';
    //                 } else {
    //                     if (res['tProc'] < page_tproc) {

    //                     } else {

    //                         // Callback process **************************************

    //                         $('.ea-right-tracker').html(res.tracker);
    //                         completeState();
    //                     }
    //                 }
    //                 //$.pnotify(res.pnotify);
    //             },
    //             type: 'POST'
    //         });
    //     });
    // });
});

/*
 * Function for validateion function call
 */
function checkCCFormat(field, rules, i, options) {
    if (field.val() && field.attr('data-format') == 'wrong') {
        return '* Credit card number is invalid.';
    }
}
function checkCCCVV(field, rules, i, options) {
    if (field.val() && $('#cctype').val()) {
        if ($('#cctype').val() == 'amex') {
            if (field.val().length != 4) {
                return "* Security Code is invalid.";
            }
        } else {
            if (field.val().length != 3) {
                return "* Security Code is invalid.";
            }
        }
    }
}