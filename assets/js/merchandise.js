//global variables that can be used by ALL the function son this page.
var dt = new Date();
var update_select;
var page_tproc = 0;

var imgAddFalse;
var imgAddTrue;

function chooseItem(id, val) {
    $('.quan_' + id).val(val).change();
}
function includeItemForceById(sets, force_id) {
    var setClass = 'includeItem_' + rand(10000, 99999);
    for (var i in sets) {
        obj = $('.quan_' + sets[i]);
        priceObj = $('.product-prices', obj.parent().parent().parent());
        priceObj.html(priceObj.data('price'));
        obj.addClass(setClass).change(function() {
            var thisObj = $(this);
            if (thisObj.data('id') == force_id && thisObj.val() == 0) {
                thisObj.val(1).change();
            }
            /*
             * Action for all items
             */
            var totalItem = 0;
            $('.' + setClass).each(function() {
                totalItem += parseInt($(this).val());
            });
            if (totalItem > 1) {
                var included = false;
                $('.' + setClass).each(function() {
                    var thisPrice = $('.product-prices', $(this).parent().parent().parent());
                    if ($(this).val() == 1 && !included) {
                        included = true;
                        thisPrice.html('Included');
                        $(this).addClass('included');
                    } else {
                        thisPrice.html(thisPrice.data('price'));
                        $(this).removeClass('included');
                    }
                });
                if (!included) {
                    $('.' + setClass).each(function() {
                        if ($(this).val() > 0 && !included) {
                            included = true;
                            $(this).addClass('included');
                        }
                    });
                }
            } else if (totalItem == 1) {
                $('.' + setClass).each(function() {
                    var thisPrice = $('.product-prices', $(this).parent().parent().parent());
                    if ($(this).val() > 0) {
                        thisPrice.html('Included');
                        $(this).addClass('included');
                    } else {
                        thisPrice.html(thisPrice.data('price'));
                        $(this).removeClass('included');
                    }
                });
            } else if (totalItem < 1) {
                $('.' + setClass).each(function() {
                    var thisPrice = $('.product-prices', $(this).parent().parent().parent());
                    thisPrice.html('Included');
                    $(this).addClass('included');
                });
            }
            update_selected();
        });
        obj.change();
    }
}
function includeItemById(sets, included_id) {
    var setClass = 'includeitem_' + rand(10000, 99999);
    for (var i in sets) {
        obj = $('.quan_' + sets[i]);
        priceObj = $('.product-prices', obj.parent().parent().parent());
        priceObj.html(priceObj.data('price'));
        obj.addClass(setClass).change(function() {
            var thisObj = $(this);

            $('.quan_' + included_id).addClass('included');

            /*
             * Action for all items
             */
            var totalItem = 0;
            $('.' + setClass).each(function() {
                totalItem += parseInt($(this).val());
            });
            if ($('.quan_' + included_id).val() > 1) {
                var thisPrice = $('.product-prices', $('.quan_' + included_id).parent().parent().parent());
                thisPrice.html(thisPrice.data('price'));
            } else {
                if (totalItem) {
                    var thisPrice = $('.product-prices', $('.quan_' + included_id).parent().parent().parent());
                    thisPrice.html('Included');
                } else {
                    $('.quan_' + included_id).val(1).change();
                }
            }
            update_selected();
        });
        obj.change();
    }
}
function includeItem(sets) {
    var setClass = 'includeitem_' + rand(10000, 99999);
    for (var i in sets) {
        obj = $('.quan_' + sets[i]);
        priceObj = $('.product-prices', obj.parent().parent().parent());
        priceObj.html(priceObj.data('price'));
        obj.addClass(setClass).change(function() {
            var thisObj = $(this);
            /*
             * Action for all items
             */
            // Clear included in all items
            $('.' + setClass).each(function() {
                var thisPrice = $('.product-prices', $(this).parent().parent().parent());
                thisPrice.html(thisPrice.data('price'));
                $(this).removeClass('included');
            });
            // Find total item count was selected
            var totalItem = 0;
            $('.' + setClass).each(function() {
                totalItem += parseInt($(this).val());
            });

            if (totalItem > 1) {
                var included = false;
                /*
                 * Find the price of the item one we can make an included item
                 */
                var minPriceItem = {
                    price: 10000000,
                    quanObj: null,
                    priceObj: null
                };
                $('.selected .' + setClass).each(function() {
                    var thisPrice = $('.product-prices', $(this).parent().parent().parent());
                    if (parseFloat(thisPrice.data('price')) < minPriceItem.price) {
                        minPriceItem.price = parseFloat(thisPrice.data('price'));
                        minPriceItem.quanObj = $(this);
                        minPriceItem.priceObj = thisPrice;
                    }
                });
                /*
                 * Find all items have the same price as the item one we found
                 */
                var itemsCanBeIncluded = new Array();
                $('.selected .' + setClass).each(function() {
                    var thisPrice = $('.product-prices', $(this).parent().parent().parent());
                    if (thisPrice.data('price') == minPriceItem.priceObj.data('price')) {
                        itemsCanBeIncluded.push($(this));
                    }
                });
                /*
                 * Find one item to be an included item (the closely one has a lowest value)
                 */
                var lowestValueItem = {
                    val: 100000,
                    quanObj: null,
                    priceObj: null
                }
                $(itemsCanBeIncluded).each(function() {
                    var thisPrice = $('.product-prices', $(this).parent().parent().parent());
                    if (parseInt($(this).val()) < lowestValueItem.val) {
                        lowestValueItem.val = parseInt($(this).val());
                        lowestValueItem.quanObj = $(this);
                        lowestValueItem.priceObj = thisPrice;
                    }
                });
                /*
                 * Make the one we get to be included item
                 */
                lowestValueItem.quanObj.addClass('included');
                if (lowestValueItem.quanObj.val() > 1) {
                    lowestValueItem.priceObj.html(minPriceItem.priceObj.data('price'));
                } else {
                    lowestValueItem.priceObj.html('Included');
                }
            } else if (totalItem == 1) {
                $('.' + setClass).each(function() {
                    var thisPrice = $('.product-prices', $(this).parent().parent().parent());
                    if ($(this).val() > 0) {
                        thisPrice.html('Included');
                        $(this).addClass('included');
                    } else {
                        thisPrice.html(thisPrice.data('price'));
                        $(this).removeClass('included');
                    }
                });
            } else if (totalItem < 1) {
                $('.' + setClass).each(function() {
                    var thisPrice = $('.product-prices', $(this).parent().parent().parent());
                    thisPrice.html('Included');
                    $(this).addClass('included');
                });
            }
            update_selected();
        });
        obj.change();
    }
}
function includeItem_bck(sets) {
    var setClass = 'includeitem_' + rand(10000, 99999);
    for (var i in sets) {
        obj = $('.quan_' + sets[i]);
        priceObj = $('.product-prices', obj.parent().parent().parent());
        priceObj.html(priceObj.data('price'));
        obj.addClass(setClass).change(function() {
            var thisObj = $(this);
            /*
             * Action for all items
             */
            var totalItem = 0;
            $('.' + setClass).each(function() {
                totalItem += parseInt($(this).val());
            });
            if (totalItem > 1) {
                var included = false;
                $('.' + setClass).each(function() {
                    var thisPrice = $('.product-prices', $(this).parent().parent().parent());
                    if ($(this).val() == 1 && !included) {
                        included = true;
                        thisPrice.html('Included');
                        $(this).addClass('included');
                    } else {
                        thisPrice.html(thisPrice.data('price'));
                        $(this).removeClass('included');
                    }
                });
                if (!included) {
                    $('.' + setClass).each(function() {
                        if ($(this).val() > 0 && !included) {
                            included = true;
                            $(this).addClass('included');
                        }
                    });
                }
            } else if (totalItem == 1) {
                $('.' + setClass).each(function() {
                    var thisPrice = $('.product-prices', $(this).parent().parent().parent());
                    if ($(this).val() > 0) {
                        thisPrice.html('Included');
                        $(this).addClass('included');
                    } else {
                        thisPrice.html(thisPrice.data('price'));
                        $(this).removeClass('included');
                    }
                });
            } else if (totalItem < 1) {
                $('.' + setClass).each(function() {
                    var thisPrice = $('.product-prices', $(this).parent().parent().parent());
                    thisPrice.html('Included');
                    $(this).addClass('included');
                });
            }
            update_selected();
        });
        obj.change();
    }
}

function forceSelectItem(id) {
    obj = $('.quan_' + id);
    priceObj = $('.product-prices', obj.parent().parent().parent());
    obj.addClass('included').change(function() {
        if ($(this).val() <= 0) {
            $(this).val(1).change();
        } else if ($(this).val() == 1) {
            priceObj.html('Included');
        } else {
            priceObj.html(priceObj.data('price'));
        }
    });
    if (obj.val() <= 0) {
        obj.val(1);
    }
    obj.change();
}
function showSelectedSubtype() {
    var parents = $('.merchandise-box');
    for (k = 0; k < parents.length; k++) {
        var parent = parents.get(k);
        var selected = $($('.product-area .product-box.selected', parent).get(0));
        if (!isEmptyObj(selected)) {
            $('.merchandise-sub-title a', parent).removeClass('show');
            $('.merchandise-sub-title a[data-subtype="' + selected.parent().data('subtype') + '"]', parent).addClass('show');
            $('.product-area', parent).removeClass('show');
            selected.parent().addClass('show');
        }
    }
}
function getSelectedSet() {
    var merchSet = new Array();
    $('.select-quan').each(function() {
        if ($(this).val() > 0) {
            merchSet.push({
                pid: $(this).data('id'),
                quan: $(this).val(),
                included: ($(this).hasClass('included') ? 1 : 0)
            });
        }
    });
    return merchSet;
}
function getMoreItemSet() {
    var moreItem = new Object();
    $('.lookingitem').each(function() {
        if ($(this).val().trim()) {
            moreItem[$(this).data('subtype')] = $(this).val().trim();
            //moreItem.push($(this).val().trim());
        }
    });
    return moreItem;
}
function flower_credit_available(flower_price) {
    var fcredit = flower_credit - flower_price;
    if (fcredit <= 0) {
        fcredit = 0;
    }
    $('.flower_credit > .flower_credit_price').html('$' + fcredit);
}
function update_selected() {
    /*
     * Send ajax
     */
    var servicechecked = new Array();
    $('.service_opt > div > input[type=checkbox]:checked').each(function() {
        servicechecked.push($(this).val());
    });
    wattingState();
    page_tproc = getTimeNumber();
    update_select = $.ajax({
        url: root + '/ajax_update_merch_selected/',
        data: $.param({
            providingurn: ($('.checkboximage2').attr('checked') ? '1' : '0'),
            sets: JSON.stringify(getSelectedSet()),
            moreitems: getMoreItemSet(),
            tProc: page_tproc
        }),
        beforeSend: function() {
            try {
                update_select.abort();
            }
            catch (err) {
            }
        },
        success: function(res) {
            res = JSON.parse(res);
            if (res.error_code == 'seslost') {
                location.href = 'packages';
            } else {
                if (res['tProc'] < page_tproc) {

                } else {

                    // Callback process **************************************

                    $('.ea-right-tracker').html(res.tracker);
                    flower_credit_available(res.flower_prices);
                    completeState();
                }
            }
            //$.pnotify(res.pnotify);
        },
        type: 'POST'
    });
}
function check_opt(selector) {
    var checkbox = $(selector);
    $('img', checkbox.parent()).attr('src', imgAddTrue);
    checkbox.attr('checked', true);
    $(checkbox.data('rel-detail')).show();
}
function uncheck_opt(selector) {
    var checkbox = $(selector);
    $('img', checkbox.parent()).attr('src', imgAddFalse);
    checkbox.attr('checked', false);
    $(checkbox.data('rel-detail')).hide();
}

/*
 * Show the text 'Included' for item that should be included (the first or the two first items)
 */
function showIncludedItem(type, count) {
    if (!count)
        count = 1;

    /*
     * Get include item
     */
    var itemSelected = new Array()
    $('.merchandise-box.' + type + ' .select-quan').each(function() {
        if ($(this).val() > 0) {
            itemSelected.push($(this));
        }
    });
    if (itemSelected.length) {
        var itemIncluded = new Array();
        $(itemSelected).each(function() {
            if (count > 0) {
                if ($(this).val() <= count) {
                    count -= $(this).val();
                    itemIncluded.push($(this));
                }
            }
        });
    } else {
        return false;
    }

    /*
     * Show included text
     */
    $('.merchandise-box.' + type + ' .select-quan').each(function() {
        if ($(this).val() > 0) {
            itemSelected.push($(this));
            var price = $(this).parent().parent().parent().find('.product-prices');
            price.html(price.data('price'));
        }
    });
    $(itemIncluded).each(function() {
        var price = $(this).parent().parent().parent().find('.product-prices');
        price.html('Included');
    });
}

$(function() {
    imgAddFalse = $('img.img_opt_no').attr('src');
    imgAddTrue = $('img.img_opt_yes').attr('src');

    $('input.checkboximage2').each(function() {
        var checkbox = $(this);

        var imgtricker = document.createElement('img');
        imgtricker.src = imgAddFalse;
        $(imgtricker).click(function() {
            if (checkbox.attr('checked')) {
                uncheck_opt('input.checkboximage2');
            } else {
                check_opt('input.checkboximage2');
            }
            update_selected();
        });
        checkbox.after(imgtricker).hide();
    });

    $('.product-quality-select select').change(function() {
        var id = $(this).data('id');
        if ($(this).val() == 0) {
            $(this).parent().parent().parent().parent().removeClass('selected');
        } else {
            $(this).parent().parent().parent().parent().addClass('selected');
        }
        if (id == 25 || id == 27) {
            $('.product-box', $(this).parent().parent().parent().parent().parent()).removeClass('hide');
        }
        //showIncludedItem($(this).data('type'));
        update_selected();
    });

    $('.merchandise-box .merchandise-sub-title > a').each(function() {
        var parent = $(this).parent().parent();
        $(this).unbind('click').click(function(e) {
            e.preventDefault();
            $('.merchandise-sub-title > a', parent).removeClass('show');
            $('.product-area', parent).removeClass('show');
            $(this).addClass('show');
            $('.product-area.' + $(this).data('subtype'), parent).addClass('show');
        });
    });

    $('.lookingitem').blur(function() {
        update_selected();
    });

    $('#mainform').submit(function(e) {
        /*
         * If not validate
         */
        if ($('#notvalidate').val())
            return true;

        /*
         * If validate
         *
         * Set of sections being rquired
         */
        var sectionRequired = new Array();
        if ($('#pkgid').val() == 2) {
            sectionRequired = [
                {
                    section: 'urn',
                    message: 'Item in Cremation Urns section is required'
                }, {
                    section: 'keepsake',
                    message: 'Item in Keepsakes section is required'
                }, {
                    section: 'memorial',
                    message: 'Item in Memorial Package Stationery section is required'
                }, {
                    section: 'flower',
                    message: 'Item in Flowers section is required'
                }
            ];
        } else if ($('#pkgid').val() == 3) {
            sectionRequired = [
                {
                    section: 'urn',
                    message: 'Item in Cremation Urns section is required',
                    itemSet: [24, 26]
                }, {
                    section: 'keepsake',
                    message: 'Item in Keepsakes section is required',
                    itemSet: [25, 27]
                }, {
                    section: 'memorial',
                    message: 'Item in Memorial Package Stationery section is required'
                }
            ];
        }

        /*
         * Perform section required message
         */
        for (var i in sectionRequired) {
            var section = sectionRequired[i]['section'];
            var message = sectionRequired[i]['message'];
            var itemSet = sectionRequired[i]['itemSet'] || false;
            var isSelected = false;
            if (itemSet) {
                for (var i in itemSet) {
                    $('.merchandise-box.' + section + ' .select-quan.quan_' + itemSet[i]).each(function() {
                        if ($(this).val() > 0) {
                            isSelected = true;
                        }
                    });
                }
            } else {
                $('.merchandise-box.' + section + ' .select-quan').each(function() {
                    if ($(this).val() > 0) {
                        isSelected = true;
                    }
                });
            }
            if (!isSelected) {
                e.preventDefault();
                $('.merchandise-box.' + section + ' .message_pointer').validationEngine('showPrompt', '* ' + message);
                scrollTo('.merchandise-box.' + section + ' .message_pointer', -50);
                if (section == 'keepsake') {
                    $('.merchandise-sub-title > a[data-subtype="Keepsake"]').click();
                }
                return false;
            }
        }

    });

    $('.product-img:not(.hasDesc) > img').colorbox();
    $('.product-img.hasDesc > img').colorbox({
        ajax: true
    });
});
