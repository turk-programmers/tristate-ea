//global variables that can be used by ALL the function son this page.
var update_select;
var page_tproc = 0;

var imgAddFalse;
var imgAddTrue;

var membershipQuestionSit = true;

function update_selected() {
    /*
     * Send ajax
     */
    wattingState();
    page_tproc = getTimeNumber();
    update_select = $.ajax({
        url: root + '/ajax_update_vital_selected/',
        data: $.param({
            obitonweb: $('.checkboximage2.chkobitonweb').attr('checked') ? 1 : 0,
            tProc: page_tproc
                    //sets:JSON.stringify(sets)
        }),
        beforeSend: function () {
            try {
                update_select.abort();
            }
            catch (err) {
            }
        },
        success: function (res) {
            res = JSON.parse(res);
            if (res.error_code == 'seslost') {
                location.href = 'packages';
            } else {
                if (res['tProc'] < page_tproc) {

                } else {

                    // Callback process **************************************

                    $('.ea-right-tracker').html(res.tracker);
                    completeState();
                }
            }
            //$.pnotify(res.pnotify);
        },
        type: 'POST'
    });
}
function check_checkbox2(obj) {
    var img = $('> img', $(obj).parent());
    $(obj).attr('checked', true);
    img.attr('src', imgAddTrue);

    if (obj.attr('name') == 'vt_was_decedent_in_armed_force') {
        $('input[name="vt_branch_of_service"]').addClass('validate[required]');
        $('input[name="vt_date_enlisted"]').addClass('validate[required]');
        $('input[name="vt_date_discharged"]').addClass('validate[required]');
        $('input[name="vt_military_serial_no"]').addClass('validate[required]');
    }
    if (obj.hasClass('chkobitonweb')) {
        update_selected();
    }
}
function uncheck_checkbox2(obj) {
    var img = $('> img', $(obj).parent());
    $(obj).attr('checked', false);
    img.attr('src', imgAddFalse);

    if (obj.attr('name') == 'vt_was_decedent_in_armed_force') {
        $('input[name="vt_branch_of_service"]').removeClass('validate[required]');
        $('input[name="vt_date_enlisted"]').removeClass('validate[required]');
        $('input[name="vt_date_discharged"]').removeClass('validate[required]');
        $('input[name="vt_military_serial_no"]').removeClass('validate[required]');
    }
    if (obj.hasClass('chkobitonweb')) {
        update_selected();
    }
}
function checkMemberQuestionSit() {
    return (membershipQuestionSit && $('input[type=radio][name=payfor]:checked').val() == 'full' && $('#pkgtype').val() == 'preneed');
}
function memberQuestionMove() {
    membershipQuestionSit = false;
    $('.membershipQuestion').validationEngine('hide');
}

$(function () {
    imgAddFalse = $('img.img_opt_no').attr('src');
    imgAddTrue = $('img.img_opt_yes').attr('src');

    $('#self_info').live("click" , function(){
        var $this = $(this);

        //Member
        var $dcFirstname = $("input[name='dc_first_name']");
        var $dcLastname = $("input[name='dc_last_name']");
        var $dcEmail = $("input[name='dc_email']");
        var $dcAddress = $("input[name='dc_residence_street']");
        var $dcCity = $("input[name='dc_residence_city']");
        var $dcState = $("#dc_residence_state");
        var $dcZip = $("input[name='dc_residence_zip']");
        var $dcPhone = $("input[name='dc_phone']");

        //Informant
        var $inFirstname = $("input[name='pi_first_name']");
        var $inLastname = $("input[name='pi_last_name']");
        var $inEmail = $("input[name='pi_email']");
        var $inAddress = $("input[name='pi_address']");
        var $inCity = $("input[name='pi_city']");
        var $inState = $("input[name='pi_state']");
        var $inZip = $("input[name='pi_zipcode']");
        var $inPhone = $("input[name='pi_phone']");

        if ( $this.is(":checked"))
        {
            $inFirstname.val($dcFirstname.val());
            $inLastname.val($dcLastname.val());
            $inEmail.val($dcEmail.val());
            $inAddress.val($dcAddress.val());
            $inCity.val($dcZip.val());
            $inState.val($dcState.val());
            $inZip.val($dcZip.val());
            $inPhone.val($dcPhone.val());
        }
        else
        {
            $inFirstname.val('');
            $inLastname.val('');
            $inEmail.val('');
            $inAddress.val('');
            $inCity.val('');
            $inState.val('');
            $inZip.val('');
            $inPhone.val('');
        }

    });


    $('input[name=payfor]').each(function () {
        $(this).click(function () {
            var obj = $(this);
            /*
             * Send ajax
             */
            wattingState();
            page_tproc = getTimeNumber();
            var originOptions = {
                url: root + '/payForChange/' + obj.val(),
                data: $.param({
                    tProc: page_tproc,
                    stepKey: stepKey
                            //sets:JSON.stringify(sets)
                }),
                beforeSend: function () {
                    try {
                        update_select.abort();
                    }
                    catch (err) {
                    }
                    //console.log('send');
                },
                success: function (res) {
                    res = JSON.parse(res);
                    if (res.error_code == 'seslost') {
                        location.href = 'packages';
                    } else {
                        if (res['tProc'] < page_tproc) {

                        } else {

                            // Callback process **************************************
                            //console.log(obj.val());
                            //console.log('success');
                            if (obj.val() != payFor) {
                                memberQuestionMove();
                            }

                            $('.ea-right-tracker').html(res.tracker);
                            completeState();
                        }
                    }
                    //$.pnotify(res.pnotify);
                },
                error: function (xhr, status, error) {
                    //console.log('error');
                    update_select = $.ajax(originOptions);
                },
                type: 'POST'
            };
            update_select = $.ajax(originOptions);
        });
    });

    $('input.checkboximage2').each(function () {
        var checkbox = $(this);
        checkbox.val(1);

        var imgtricker = document.createElement('img');
        imgtricker.src = imgAddFalse;
        if (checkbox.attr('checked')) {
            imgtricker.src = imgAddTrue;
            check_checkbox2(checkbox);
        }
        $(imgtricker).click(function () {
            if (checkbox.attr('checked')) {
                //$(this).attr('src',imgAddFalse);
                //checkbox.attr('checked',false);
                uncheck_checkbox2(checkbox);
            } else {
                //$(this).attr('src',imgAddTrue);
                //checkbox.attr('checked',true);
                check_checkbox2(checkbox);
            }
        });
        checkbox.after(imgtricker).hide();
    });

    $('#mainform').submit(function (e) {
        /*
         * If not validate
         */
        if ($('#notvalidate').val())
            return true;

        if ($('#mainform').validationEngine('validate')) {
            if ($('input[name=dc_residence_street]').length > 0) {
                var fields = ['dc_residence_street', 'dc_residence_state', 'dc_residence_zip'];
                var result = true;
                fields.forEach(function (element, index, array) {
                    if ($('input[name=' + element + ']').val().toUpperCase() === 'NA'
                            || $('input[name=' + element + ']').val().toUpperCase() === 'N/A') {
                        $('input[name = ' + element + ']').validationEngine('showPrompt', 'A valid address is required for the insurance.', 'red', 'topLeft', true).scrollTo(-50);
                        result = false;
                    }
                });

                if (result === false) {
                    return false;
                }
            }
            if (checkMemberQuestionSit()) {
                e.preventDefault();
                memberQuestionMove();
                $('.membershipQuestion').validationEngine('showPrompt', 'Are you sure you want to pay for the full arrangement? If so, click \'Next\'.', 'load', 'topLeft', true).scrollTo();
                return false;
            }
        }

        /*
         * If validate
         */
        /*if($('#pkgtype').val() == 'atneed'){
         var field;
         var pass = true;
         //Has the decedent/person being planned for purchased pre-need funeral insurance through Tri-state Cremation Society?
         field = $('input[name="ms_has_person_being_planned"]');
         if(!field.attr('checked') && pass){
         pass = false;
         e.preventDefault();
         $('> img', field.parent()).validationEngine('showPrompt', '* This field is required.');
         $('> img', field.parent()).click(function(){
         $('> img', field.parent()).validationEngine('hideAll');
         });
         scrollTo($('> img', field.parent()), -50);
         }
         //Would you like us to publish an obituary on our Website?
         field = $('input[name="obit_is_publish_on_our_web"]');
         if(!field.attr('checked') && pass){
         pass = false;
         e.preventDefault();
         $('> img', field.parent()).validationEngine('showPrompt', '* This field is required.');
         $('> img', field.parent()).click(function(){
         $('> img', field.parent()).validationEngine('hideAll');
         });
         scrollTo($('> img', field.parent()), -50);
         }
         }*/
    });
});