
$(function () {

    $('#mainform').submit(function (e) {
        /*
         * If not validate
         */
        if ($('#notvalidate').val())
            return true;

        if ($('#mainform').validationEngine('validate')) {
            if ($('input[name=insured_mailing_address]').length > 0) {
                var fields = ['insured_mailing_address', 'insured_city', 'insured_state', 'insured_zip'];
                var result = true;
                fields.forEach(function (element, index, array) {
                    if ($('input[name=' + element + ']').val().toUpperCase() === 'NA'
                            || $('input[name=' + element + ']').val().toUpperCase() === 'N/A') {
                        $('input[name = ' + element + ']').validationEngine('showPrompt', 'A valid address is required by the insurance.', 'red', 'topLeft', true).scrollTo(-50);
                        result = false;
                    }
                });

                if (result === false) {
                    return false;
                }
            }
        }
    });
});