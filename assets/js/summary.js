function toggle(id,obj) {
	var ele = document.getElementById(id);
	if(ele.style.display == "block") {
    	ele.style.display = "none";
		obj.innerHTML = "view details";
  	}
	else {
		ele.style.display = "block";
		obj.innerHTML = "less details";
	}
}

$(function(){
	$('.product-area').show();
	$('.product-img > img').colorbox();
});
