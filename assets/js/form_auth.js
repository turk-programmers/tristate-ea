$(function(){
	$(".policies").colorbox({inline:true, href:'#policies', width: 800, height:600, scrolling:true});

	$('#init_1').keyup(function(){
		if($(this).val()==''){
			$('#init_2').addClass('validate[required]');
			$('#init_2_items').addClass('validate[required]');
		}else{
			$('#init_2').val('');
			$('#init_2_items').val('');
			$('#init_2').validationEngine('hide')
			$('#init_2_items').validationEngine('hide')
			$('#init_2').removeClass('validate[required]');
			$('#init_2_items').removeClass('validate[required]');
		}
	});
	$('#init_2').keyup(function(){
		if($(this).val()==''){
			$('#init_1').addClass('validate[required]');
			$('#init_2_items').removeClass('validate[required]');
		}else{
			$('#init_1').val('');
			$('#init_1').validationEngine('hide')
			$('#init_1').removeClass('validate[required]');
			$('#init_2_items').addClass('validate[required]');
		}
	});
	$('#init_1').keyup();
	$('#init_2').keyup();

});