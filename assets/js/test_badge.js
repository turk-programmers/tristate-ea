$(function(){
	$('body > div:visible').prepend($('.test_badge'));
});

function propulateForm(param_valuenumber){
	$('input:visible,input.checkboximage2').each(function(){
		var valuenumber = param_valuenumber || 1;
	    if($(this).prop('type') == "text"){
	    	if(!$(this).val() && !$(this).attr('disabled')){
	    		console.log(valuenumber);
	    		if(valuenumber == 2){
			        if($(this).attr('data-value2')){
			            $(this).val($(this).attr('data-value2')).keyup().blur();
			            console.log($(this).attr('data-value2'))
			        }else{
			            valuenumber = 1;
			        }
	    		}
	    		if(valuenumber == 1){
	    			if($(this).attr('data-value')){
			            $(this).val($(this).attr('data-value')).keyup().blur();
			        }else{
			            $(this).val($(this).attr('name')).keyup().blur();
			        }
	    		}
	    	}
	    }else if(($(this).prop('type') == "checkbox" || $(this).prop('type') == "radio") & $(this).data("status") != "not-checked"){
	        $(this).attr('checked',true).click();
	        if(typeof(check_checkbox2) && $(this).hasClass('checkboximage2')){
	        	check_checkbox2($(this));
	        }
	    }
	});
	$('select:visible').each(function(){
		var valuenumber = param_valuenumber || 1;
		if(!$(this).val() && !$(this).attr('disabled')){
		    var sel = $(this);
		    if(valuenumber == 2){
		        if($(this).attr('data-value2')){
		            $(this).val($(this).attr('data-value2')).change();
		        }else{
				    valuenumber = 1;
		        }
    		}
    		if(valuenumber == 1){
    			if($(this).attr('data-value')){
		            $(this).val($(this).attr('data-value')).change();
		        }else{
				    $('option', $(this)).each(function(){
				        if($(this).val()!=''){
				            sel.val($(this).val()).change();
				            return false;
				        }
				    });
		        }
    		}
		}
	});
	$('textarea:visible').each(function(){
		var valuenumber = param_valuenumber || 1;
		if(!$(this).val() && !$(this).attr('disabled')){
			if(valuenumber == 2){
		        if($(this).attr('data-value2')){
		            $(this).val($(this).attr('data-value2')).keyup().blur();
		        }else{
		            valuenumber = 1;
		        }
    		}
    		if(valuenumber == 1){
    			if($(this).attr('data-value')){
		            $(this).val($(this).attr('data-value')).keyup().blur();
		        }else{
		            $(this).val($(this).attr('name')).keyup().blur();
		        }
    		}
		}
	});
	$("#mainform").validationEngine('hide');
}