var obj_service_select;
var tmp_service_select;
var page_tproc = 0;
$(function(){						
	$(".checkbox-option").click(function(){
		if($(this).hasClass('checkbox-option-select')){
			$(this).removeClass('checkbox-option-select');
			$('input[data-id="'+$(this).attr('data-id')+'"]').val("");
			if($(this).attr('data-id') == 5){
				$(".checkbox-option[data-id=4]").removeClass('checkbox-option-select');
				$('input[data-id=4]').val('');
			}
		}else{
			$(this).addClass('checkbox-option-select');
			$('input[data-id="'+$(this).attr('data-id')+'"]').val($(this).attr('data-id'));
			if($(this).attr('data-id') == 4){
				$(".checkbox-option[data-id=5]").addClass('checkbox-option-select');
				$('input[data-id=5]').val(5);
			}
		}
		saveOnTheFly();
	});
	$(".activeOnTheFly").change(function(){
		saveOnTheFly();
	});
	function saveOnTheFly(){
		
		$('#ea .ea-sidebar .sidebar-price').html('<i class="fa fa-refresh fa-spin"></i>');
		$('#ea .button-link .btn-wait').css('display','inline-block');
		$('#ea .button-link .btn-next').css('display','none');
		
		var options = new Array();
		$('.checkbox-option-select:visible').each(function(){
			options.push($(this).attr('data-id'));
		});
		
		page_tproc = getTimeNumber();
		obj_service_select = $.ajax({
	    	url:root+'/ajax_service_select',
	    	data:$.param({
	    		options:JSON.stringify(options),
	    		release:$('select[name="release"]').val(),
	    		weight:$('select[name="weight"]').val(),
	    		dcc:$('select[name="dcc"]').val(),
	    		tProc:page_tproc
	    	}),
	    	beforeSend:function(){
				try{
					obj_service_select.abort();
				}
				catch(err){}
	    	},
	    	success:function(res){
	    		res = JSON.parse(res);
	    		if(res.error_code == 'seslost'){
	    			location.href = 'packages';
	    		}else{
	    			if(res['tProc'] < page_tproc){
	    				
	    			}else{
	    				
	    				// Callback process **************************************
	    				
			    		$('.ea-sidebar .sidebar-price').html(res.value);
						$('#ea .button-link .btn-next').css('display','inline-block');
						$('#ea .button-link .btn-wait').css('display','none');
	    			}
	    		}
	    		//$.pnotify(res.pnotify);
	    	},
	    	type:'POST'
	    });
		
	}
});
