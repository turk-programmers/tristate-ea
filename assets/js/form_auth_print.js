$(function(){
	$('input.text_other').unbind('keypress').keypress(function(){
		$($(this).attr('data-rel')).attr('checked',true);
	});
	$('input:visible,select:visible,textarea:visible').each(function(){
		var fieldClass = 'valuefield';
		if($(this).prop('type') == 'select-one'){
			fieldClass = 'valuefield2';
		}
		if(!$(this).hasClass('noPrintVersion')){
			if($(this).attr('type') == 'radio'){
				if(!$(this).get(0).checked){
					$(this).attr('disabled','disabled');
				}
			}else if($(this).attr('type') == 'checkbox'){
				$(this).attr('disabled', true);
			}else{
				if($(this).hasClass('setdate')){
					if($(this).hasClass('setday')){
						$(this).before("<span class='"+fieldClass+"' style='text-decoration:none;'>"+$(this).val()+','+"&nbsp;</span>");
					}else{
						$(this).before("<span class='"+fieldClass+"' style='text-decoration:none;'>"+$(this).val()+"&nbsp;</span>");
					}
				}else if($(this).hasClass('settime')){
					$(this).before("<span class='"+fieldClass+"' style='text-decoration:none;'>: "+$(this).val()+"&nbsp;</span>");
				}else{
					$(this).before("<span class='"+fieldClass+"' style='text-decoration:none;'>"+$(this).val()+"&nbsp;</span>");
				}
				$(this).hide();
			}
		}
	});
});
