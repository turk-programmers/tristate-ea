<?=$header?>
<form method="post" id="mainform" name="mainform" style="margin:0;">
<input type="hidden" name="scriptaction" 	id="scriptaction"	value="validate" />
<input type="hidden" name="notvalidate" 	id="notvalidate"	value="" />
<input type="hidden" name="next_form" 		id="next_form"		value="<?=$next_form?>" />
<!--Data-->

<input type="hidden" name="pkgid"	 		id="pkgid"			value="<?=$user_session['packageselected']['id']?>" />

<div id="ea" class="">
	<?php
	$this->load->view('message_badge');
	?>
	
	<img class="img_opt_no" src="<?=$root?>/assets/images/btn-choose-merch-no.png" style="display:none;">
	<img class="img_opt_yes" src="<?=$root?>/assets/images/btn-choose-merch-yes.png" style="display:none;">
	
	<div class="ea-step-title-box">
		<h1 class="ea-step-title">step 2: Select MERCHANDISE</h1>
	</div>
	<div id="content-ea" >
		<div id="ea-content-inner">
	    
	    	<div class="providing-cremation-urn-box">
	        	<div class="providing-cremation-urn-text">Will you be providing the cremation urn?</div>
	            <div class="providing-cremation-urn-select"><input class="checkboximage2" name="providingurn" type="checkbox" data-rel-detail="#if-yes-providing" /></div>
	        </div>
	        <div class="if-yes-providing-cremation-urn-box" id="if-yes-providing"  style="display:none;">
	        	<div class="if-yes-providing-cremation-urn-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
	            </div>
	        </div>
	        
            <?php
            foreach($merch_types as $typekey=>$type){
            	?>
		        <div class="merchandise-box <?=$typekey?>" data-type="<?=$typekey?>">
		        	<div class="merchandise-title"><div class="message_pointer"></div><?=$type['name']?></div>
		        	<?php
		        	switch ($typekey) {
						case 'urn':
				        	?>
				            <div class="merchandise-sub-title">
				            	<a href="#" class="show" data-subtype="Metal">Metal Urns</a>   |   <a href="#" data-subtype="Wood">Wood Urns</a>   |   <a href="#" data-subtype="Scattering">Scattering Urns</a> <br>
								<a href="#" data-subtype="Stone">Ceramic, Porcelain & Stone Urns</a>   |   <a href="#" data-subtype="UrnVaults">Urn Vaults</a>
				            </div>
				            <?php
							break;
						
						case 'keepsake':
				        	?>
				            <div class="merchandise-sub-title">
				            	<a href="#" class="show" data-subtype="Jewelry">Jewelry</a>   |   <a href="#" data-subtype="MemoryGlass">Memory Glass</a>   |   <a href="#" data-subtype="Thumbies">Thumbies</a>
				            </div>
				            <?php
							break;
						
						case 'memorial':
							break;
						
						case 'flower':
				        	?>
				            <div class="merchandise-sub-title">
				            	<a href="#" class="show" data-subtype="Plant">Plant</a>   |   <a href="#" data-subtype="Basket">Basket</a>   |   <a href="#" data-subtype="Standing">Standing</a><br>
				            	<a href="#" data-subtype="Urn">Urn</a>   |   <a href="#" data-subtype="Fruit">Fruit</a>
				            	<?php
				            	if(@$user_session['packageselected']['flower_credit']){
				            		?>
					            	<div class="flower_credit">Flower Credit Available: <span class="flower_credit_price">$<?=number_format($user_session['packageselected']['flower_credit'])?></span></div>
				            		<?php
				            	}
				            	?>
				            </div>
				            <?php
							break;
					}
		            foreach($type['items'] as $subtypekey=>$subtype){
		            	$show = '';
						if($subtypekey == 'Metal'){
							$show = 'show';
						}elseif($subtypekey == 'Jewelry'){
							$show = 'show';
						}elseif($subtypekey == 'memorial'){
							$show = 'show';
						}elseif($subtypekey == 'Plant'){
							$show = 'show';
						}
		            	?>
			            <div class="product-area <?=$subtypekey?> <?=$show?>" data-subtype="<?=$subtypekey?>">
			            	<?php
							foreach($subtype as $id=>$item){
								?>
			                    <div class="product-box">
			                    	<div class="big-border">
			                            <div class="product-img"><img title="<?=$item['name']?>" src="<?=$item['thumb_url']?>"/></div>
			                            <div class="product-name"><?=$item['name']?></div>
			                            <div class="product-prices" data-price="<?=$item['price']?>"><?=$item['price']?></div>
			                            <div class="product-quality">QTY:
			                                <span class="product-quality-select">
			                                    <select class="select-quan quan_<?=$id?>" data-id="<?=$id?>" data-type="<?=$typekey?>">
			                                    	<?php
			                                    	for($q=0;$q<=5;$q++){
			                                    		?><option value="<?=$q?>"><?=$q?></option><?php
			                                    	}
			                                    	?>
			                                    </select>
			                                </span>
			                            </div>
			                    	</div>
			                    </div>
			                    <?php
							}
							if($typekey == 'keepsake' and $subtypekey != 'Jewelry'){
								?>
								<div class="venderlink">
									<?php
									if($subtypekey == 'Thumbies'){
										?><a target="_blank" href="https://meadowhillco.jsp-servlet.net/">Visit the Thumbies Website for more selection.</a><?php
									}else{
										?><a target="_blank" href="http://www.memoryglass.com/products.asp">Visit the Memory Glass Website for more selection.</a><?php
									}
									?>
									<br>
									<br>
									Is there an item you would like to purchase that isn't listed in our selection? If so, enter it here:<br>
									<input class="lookingitem <?=$subtypekey?>" data-subtype="<?=$subtypekey?>"></input>
								</div>
								<?php
							}
							?>
			            </div>
		            	<?php
		            }
		            ?>
		        </div>
            	<?php
            }
            ?>
	    </div>
		<?php
		$this->load->view('_right_bar');
		?>
		
	</div>
</div>
<script>
var flower_credit = <?=@$user_session['packageselected']['flower_credit'] ? $user_session['packageselected']['flower_credit'] : 0?>;
</script>
<script src="<?=$cfg['root']?>/assets/js/merchandises.js"></script>
<script>
$(function(){
	<?php
	if(is_array(@$user_session['merch_more_item_looking'])){
		foreach($user_session['merch_more_item_looking'] as $skey=>$val){
			?>
			$('.lookingitem[data-subtype="<?=$skey?>"]').val('<?=$val?>');
			<?php
		}
	}
	if(@$user_session['merch_providing_urn']){
		?>
		check_opt('input.checkboximage2');
		<?php
	}
	$cart = @$user_session['cart'];
	if(is_array($cart) and count($cart)){
		foreach($cart as $type=>$items){
			foreach($items as $id=>$item){
				?>
				chooseItem(<?=$id?>,<?=$item['quan']?>);
				<?php
			}
		}
	}
	
	if(is_array(@$user_session['serviceselected'][5]) and $user_session['packageselected']['id'] < 4){
		$sets = array();
		foreach($merch_types['memorial']['items']['memorial'] as $id=>$item){
			# Separate with the veteran one
			if($id == 61) continue;
			
			$sets[] = '\''.$id.'\'';
		}
	 	?>
	 	includeMemorialItem(new Array(<?=implode(',', $sets)?>));
	 	forceSelectItem(61);
	 	<?php
	}elseif(is_array(@$user_session['serviceselected'][5])){
		?>
		forceSelectItem(61);
		<?php
	}elseif($user_session['packageselected']['id'] < 4){
		$sets = array();
		foreach($merch_types['memorial']['items']['memorial'] as $id=>$item){
			$sets[] = '\''.$id.'\'';
		}
	 	?>
	 	includeMemorialItem(new Array(<?=implode(',', $sets)?>));
	 	<?php
	}
	?>
	showSelectedSubtype();
});
$(window).load(function(){
	<?php
	if(@$gotosection){
		?>
		scrollTo('.merchandise-box.<?=$gotosection?>');
		<?php
	}
	?>
});
</script>
<!--Data-->
</form>
<?php echo $footer?>