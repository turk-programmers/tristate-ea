<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once('/home/funeralnet/public_html/mfscommon/php/dev_check.php');
include_once('/home/funeralnet/public_html/mfscommon/php/CommonFN.php');

class Earrangement extends CI_Controller {

    private $settings;
    private $steps;
    private $packages;
    private $services;
    private $merchs;
    private $merch_types;
    private $summary;
    private $states;
    private $isAdmin = false;
    private $isBam = false;
    private $cfg;
    private $pkgtype;

    # Form prefix
    private $vitalprefix = 'formvital';
    private $insurance1prefix = 'forminsure1';
    private $insurance2prefix = 'forminsure2';
    private $authprefix = 'formauth';
    private $authstdprefix = 'formauth';
    private $paymentprefix = 'formpayment';

    public function __construct() {
        parent::__construct();

        if( is_dev() ){
            opcache_reset();
        }


        # Assign admin flag
        $this->isAdmin = ($this->input->get('hash') != '') ? true : false;
        $this->load->vars(array('isAdmin' => $this->isAdmin));

        # Assign bam flag
        if (!$this->isBam) {
            $this->isBam = preg_match('/member/i', substr($this->uri->segment(1), -6));
        }
        $this->load->vars(array('isBam' => $this->isBam));

        # Start the native session
        $this->start_session();
        $this->set_user_session('ea_version', 1.00);

        # Load config
        $this->config->load('config');
        $this->config->load('earrangement', true);
        $this->cfg = $this->config->item('earrangement');
        $this->load->vars(array('cfg' => $this->cfg));

        # Load model
        $this->load->model('earrangement_model', 'model');

        # Load helper
        $this->load->helper(array(
            'earrangement',
            'form',
            'url',
            'sendmail'
        ));

        # Load libraries
        $this->load->library(array(
            'form_validation',
            'bluefin',
            'session',
        ));

        # Load setting
        $this->settings = $this->model->get_settings();
        if (@$this->settings['test_mode']) {
            if (!is_dev()) {
                $this->settings['test_mode'] = 0;
                $this->settings['test_mode_description'] = 'Test mode is OFF, you are not surfing from dev area.';
            }
        }
        $this->load->vars(array('settings' => $this->settings));

        # Load steps
        $this->steps = $this->get_steps();
        if ($this->isBam) {
            $this->steps = $this->get_steps();
        }
        $this->load->vars(array('steps' => $this->steps));

        # Load state
        $this->states = $this->model->get_state();
        $this->load->vars(array('states' => $this->states));

        # Load prefix variable
        $this->load->vars(array(
            'vitalprefix' => $this->vitalprefix,
            'insurance1prefix' => $this->insurance1prefix,
            'insurance2prefix' => $this->insurance2prefix,
            'authprefix' => $this->authprefix,
            'paymentprefix' => $this->paymentprefix,
        ));



        # Set member fee
        $this->set_user_session('member_fee', $this->settings['member_fee']);

        date_default_timezone_set('America/New_York');

        /*
         * Turn OFF test mode if taken live already
         */
        if (@$this->settings['is_live'] == 1 and ! is_dev()) {
            $this->model->turn_test_mode(false);
            $this->settings = $this->model->get_settings();
        }
    }

    public function index() {
        $this->atneed();
    }

    /*     * **************************************************************************************************
     * DEBUGGING
     * **************************************************************************************************
     */

    public function print_session() {
        ?>
        <style>
            pre {
                background-color: #F0F0F0;
                border: 1px solid #CCCCCC;
                margin-bottom: 30px;
                padding: 10px;
            }
            pre > b {
                font-weight: normal;
            }
        </style>
        <?php
        echo '<pre>';
        echo '<b>SESSION : ' . $this->get_session_name() . '</b> - ';
        echo print_r($this->get_session(), true);
        echo '</pre>';

        echo '<pre>';
        echo '<b>$cfg</b> - ';
        echo print_r($this->cfg, true);
        echo '</pre>';

        echo '<pre>';
        echo '<b>$settings</b> - ';
        echo print_r($this->settings, true);
        echo '</pre>';

        echo '<pre>';
        echo '<b>SERVER</b> - ';
        echo print_r($_SERVER, true);
        echo '</pre>';
    }

    public function print_session_member() {
        $this->print_session();
    }

    public function debug($pkgtype = false) {
        /* $user_session = $this->get_user_session();

          $amount = number_format((rand(100,119)/100),2,'.','');

          $bluefin = $this->cfg['bluefin'];

          $names = array_reverse(explode(' ',$user_session[$this->paymentprefix]['ccname']));
          $fname = array_pop($names);
          $lname = implode(' ',array_reverse($names));

          $this->bluefin->setParam('account_id',$bluefin['account_id']);
          $this->bluefin->setParam('api_accesskey',$bluefin['access_key']);
          $this->bluefin->setParam('ip_address',$_SERVER['REMOTE_ADDR']);
          $this->bluefin->setParam('transaction_type','SALE');
          $this->bluefin->setParam('transaction_amount',$amount);

          $this->bluefin->setParam('tender_type','CARD');
          $this->bluefin->setParam('card_number','4246315157520402');
          $this->bluefin->setParam('card_expiration',substr('0'.$user_session[$this->paymentprefix]['ccexpmonth'],-2).substr($user_session[$this->paymentprefix]['ccexpyear'],-2));

          $this->bluefin->setParam('first_name',$fname);
          $this->bluefin->setParam('last_name',$lname);
          $this->bluefin->setParam('street_address1',$user_session[$this->paymentprefix]['ccaddress'].' '.$user_session[$this->paymentprefix]['ccaddress2']);
          $this->bluefin->setParam('city',$user_session[$this->paymentprefix]['cccity']);
          $this->bluefin->setParam('state',$user_session[$this->paymentprefix]['ccstate']);
          $this->bluefin->setParam('zip',$user_session[$this->paymentprefix]['cczip']);
          $this->bluefin->setParam('country','US');
          $this->bluefin->setParam('email',$user_session[$this->vitalprefix]['pi_email']);
          $this->bluefin->setParam('phone',$user_session[$this->vitalprefix]['pi_phone']);

          $this->bluefin->setParam('transaction_description',$user_session['packageselected']['name'].' - $'.$amount);

          //$this->bluefin->setParam('custom_data',$custom_data);

          //$this->bluefin->setParam('disable_avs','1');
          //$this->bluefin->setParam('disable_cvv2','1');
          //$this->bluefin->setParam('disable_fraudfirewall','1');

          //$result = $this->bluefin->process();
          //$rinfo = $this->bluefin->getResponse();

          ?>
          <pre>
          <?php print_r($result);?>
          <?php print_r($rinfo);?>
          </pre>
          <?php */

        echo date('Y-m-d H:i a', 1402995720);
        echo '<br>';
        echo date('c', 1402995720);
        ?>
        <pre>
            <?php print_r($_SERVER); ?>
        </pre>
        <?php
    }

    public function clear_session() {
        $this->destroy_session();
        $this->go_to('packages');
    }

    public function clear_session_member() {
        $this->destroy_session();
        $this->go_to('become_a_member');
    }

    public function clear_cache() {
        $cache_files = array(
            'application/cache/ea_template.html',
            'application/cache/ea_template_print.html',
        );
        foreach ($cache_files as $path) {
            if (file_exists($path)) {
                if (unlink($path)) {
                    echo 'Cache was cleared successful, "' . $path . '" file was deleted';
                } else {
                    echo 'Cache clearing fail!, "' . $path . '" file was not deleted';
                }
            } else {
                echo '"' . $path . '" file was not created.';
            }
            echo "<br>";
        }
    }

    public function close_test_mode() {
        $this->model->turn_test_mode(false);
        if ($this->isBam) {
            $this->go_to('become_a_member');
        } else {
            $this->go_to('packages');
        }
    }

    public function open_test_mode() {
        if (is_dev()) {
            $this->model->turn_test_mode(true);
        }
        if ($this->isBam) {
            $this->go_to('become_a_member');
        } else {
            $this->go_to('packages');
        }
    }

    public function blank() {
        # Load the step view.
        $this->load_step('blank', '');
    }

    /*     * **************************************************************************************************
     * PACKAGE TYPES
     * **************************************************************************************************
     */

    public function atneed() {
        # Assign package type
        $this->pkgtype = 'atneed';

        # Clear all session
        if (!$this->input->post('scriptaction')) {
            $this->destroy_session();
        }


        $this->set_user_session('pkgtype', $this->pkgtype);
        $this->set_user_session('isbam', false);
        $this->packages();
    }

    public function preneed() {
        # Assign package type
        $this->pkgtype = 'preneed';

        # Clear all session
        if (!$this->input->post('scriptaction')) {
            $this->destroy_session();
        }

        $this->set_user_session('pkgtype', $this->pkgtype);
        $this->set_user_session('isbam', false);
        $this->set_user_session('is_member', 0);
        $this->packages();
    }

    public function become_a_member() {
        # Assign package type
        $this->pkgtype = 'preneed';

        # Clear all session
        if (!$this->input->post('scriptaction')) {
            $this->destroy_session();
        }

        $this->set_user_session('pkgtype', $this->pkgtype);
        $this->set_user_session('isbam', true);
        $this->set_user_session('is_member', 0);
        $this->packages();
    }

    /*     * **************************************************************************************************
     * PACKAGES
     * **************************************************************************************************
     */

    public function packages() {
        # Check session
        $this->check_session();
        $this->pkgtype = $this->get_user_session('pkgtype');

        # Load package
        $this->packages = $this->model->get_packages($this->pkgtype);
        $this->services = $this->model->get_services();
        $option_states = $this->cfg['states'];

        # Assign page variable
        $vars = array(
            'stepkey' => 'pkg',
            'next_form' => 'merchandise',
            'page_title' => 'Select location and cremation package information',
            'packages' => $this->packages,
            'services' => $this->services,
            'option_states' => $option_states,
        );
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        if (!$this->input->post('scriptaction')) {
            # Pre-select package
            //$this->set_user_session('packageselected', $this->packages[4]);
            # Calcelate amont due
            //$this->_calculate_amount_due();
        } else {


            $this->set_user_session('stateselected', @$this->input->post('state'));
            $this->set_user_session('countyselected', @$this->input->post('county'));
            $this->set_user_session('packageselected', @$this->packages[@$this->input->post('pkgid')]);
            if (is_array($this->input->post('service'))) {
                $serviceselected = @$this->input->post('service');
                foreach ($serviceselected as $sid) {
                    $serviceselected[$sid] = $this->services[$sid];
                }
            }
            $this->set_user_session('serviceselected', @$serviceselected);

            $this->form_validation->set_rules('state', 'State', 'required');
            $this->form_validation->set_rules('county', 'County', 'required');
            $this->form_validation->set_rules('pkgid', 'Package', 'required');

            if ($this->input->post('notvalidate')) {
                # go next form
                $this->go_to($this->input->post('next_form'));
            } elseif ($this->form_validation->run()) {
                $this->set_user_session('packageselected', $this->packages[$this->input->post('pkgid')]);
                # go next form
                $this->go_to($this->input->post('next_form'));
            } else {
                $this->load->vars(array('error_message' => validation_errors()));
            }
        }

        # Load the step view.
        $this->load_step('packages', $page_title, array('obj' => $this));
    }

    public function ajax_update_selected() {
        /* ?>
          isBam: <?=$this->isBam?>
          <pre>
          <?php print_r($this->get_user_session());?>
          </pre>
          <?php
          exit(); */
        $rt = array();
        if (!$this->get_user_session('pkgtype')) {
            # Session was lost
            $rt['status'] = 'error';
            $rt['error_code'] = 'seslost';
        } else {
            if (@$_SESSION['tProc'] < $this->input->post('tProc')) {
                $_SESSION['tProc'] = $this->input->post('tProc');

                # Process coding **************************************************
                //$this->set_user_session('is_member', $this->input->post('ismember'));



                # Is providing own urn
                if ($this->input->post('providingurn')) {
                    $this->set_user_session('merch_providing_urn', array(
                        'name' => 'Providing own cremation urn',
                        'price' => 0,
                    ));
                } else {
                    $this->set_user_session('merch_providing_urn', '0');
                }

                # Load package
                $this->packages = $this->model->get_packages($this->get_user_session('pkgtype'));
                $this->services = $this->model->get_services();

                # Update package selected
                if (@$this->packages[$this->input->post('pkg')]['id']) {
                    $this->set_user_session('packageselected', $this->packages[$this->input->post('pkg')]);
                }

                # Update service selected
                if (is_array($this->input->post('sids'))) {
                    $serviceselected = array();
                    foreach ($this->input->post('sids') as $sid) {
                        $serviceselected[$sid] = $this->services[$sid];
                    }
                }
                $this->set_user_session('serviceselected', @$serviceselected);

                # Return coding **************************************************

                $this->_calculate_amount_due();
                $rt['status'] = 'complete';
                $rt['tProc'] = $this->input->post('tProc');
                $rt['tracker'] = $this->load->view('_right_bar_tracker', array(
                    'user_session' => $this->get_user_session(),
                    'services' => $this->services,
                        ), true);
            }
        }

        echo json_encode($rt);
    }

    public function ajax_update_selected_member() {
        $this->ajax_update_selected();
    }

    /*     * **************************************************************************************************
     * MERCHANDISE
     * **************************************************************************************************
     */

    public function merchandise($gotosection = '') {


        # Check session
        $this->check_session();

        if(!$this->get_user_session("packageselected")){
            show_404();
        }

        # Load merch
        $this->fetch_merch_items();

        # Assign page variable
        $vars = array(
            'stepkey' => 'merch',
            'prev_form' => 'packages',
            'next_form' => 'summary',
            'page_title' => 'Select Merchandise',
            'merch_types' => $this->merch_types,
            'merch_items' => $this->merchs,
            'gotosection' => $gotosection,
        );
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        if (!$this->input->post('scriptaction')) {
            # Calcelate amont due
            //$this->_calculate_amount_due();
        } else {

            $this->go_to($this->input->post('next_form'));
        }

        # Load the step view.
        $this->load_step('merchandise', $page_title, array('obj' => $this));
    }

    public function ajax_update_merch_selected() {
        $rt = array();
        if (!$this->get_user_session('pkgtype')) {
            # Session was lost
            $rt['status'] = 'error';
            $rt['error_code'] = 'seslost';
        } else {
            if (@$_SESSION['tProc'] < $this->input->post('tProc')) {
                $_SESSION['tProc'] = $this->input->post('tProc');

                # Process coding **************************************************
                # Load merch
                $this->fetch_merch_items();

                # Update item checked
                if (@$this->input->post('providingurn')) {
                    $this->set_user_session('merch_providing_urn', array(
                        'name' => 'Providing own cremation urn',
                        'price' => 0,
                    ));
                } else {
                    $this->set_user_session('merch_providing_urn', '0');
                }

                # Update more item put
                $this->set_user_session('merch_more_item_looking', $this->input->post('moreitems'));

                # Update item selected
                $merchandiseselected = array();
                $flower_prices = 0;
                $sets = json_decode($this->input->post('sets'), true);
                if (is_array($sets)) {
                    foreach ($sets as $item) {
                        $sitem = $this->merchs[$item['pid']];
                        $sitem['quan'] = $item['quan'];
                        $sitem['included'] = $item['included'];
                        $merchandiseselected[$this->merchs[$item['pid']]['typekey']][$item['pid']] = $sitem;
                        if ($sitem['type'] == 'Flower') {
                            $flower_prices += ($sitem['price'] * ($sitem['quan'] - $sitem['included']));
                        }
                    }
                }
                $this->set_user_session('cart', $merchandiseselected);

                # Return coding **************************************************

                $this->_calculate_amount_due();
                $rt['status'] = 'complete';
                $rt['flower_prices'] = $flower_prices;
                $rt['tProc'] = $this->input->post('tProc');
                $rt['tracker'] = $this->load->view('_right_bar_tracker', array(
                    'user_session' => $this->get_user_session(),
                    'merchs' => $this->merchs,
                    'merch_type' => $this->merch_types,
                        ), true);
                $rt['tracker'] = utf8_encode($rt['tracker']);
            }
        }

        echo json_encode($rt);
    }

    public function ajax_view_large_image($id) {
        $merch_items = $this->get_session('merch_items');
        $this->load->view('large_image', array(
            'id' => $id,
            'item' => $merch_items[$id],
        ));
    }

    private function get_image_url($type, $id) {
        $url = $this->cfg['merch_image_url'];
        $url = str_replace('[type]', $type, $url);
        $url = str_replace('[id]', $id, $url);
        return $url;
    }

    private function get_thumb_url($type, $id) {
        $url = $this->cfg['merch_thumb_url'];
        $url = str_replace('[type]', $type, $url);
        $url = str_replace('[id]', $id, $url);
        return $url;
    }

    private function fetch_merch_items() {
        # Fetch merch all items
        $this->merchs = $this->get_session('merch_items');
        if (!$this->merchs) {
            $this->merchs = $this->model->get_merch_items();
            $this->set_session('merch_items', $this->merchs);
        }
        foreach ($this->merchs as $pid => $item) {
            $this->merchs[$pid]['image_url'] = $this->get_image_url($item['type'], $pid);
            $this->merchs[$pid]['thumb_url'] = $this->get_thumb_url($item['type'], $pid);
        }

        # Fetch merch types
        $this->merch_types = $this->cfg['merch_types'];

        # Urn items
        $urnItems = $this->get_session('urn_items');
        if (!$urnItems or 1) {
            $urnItems = $this->model->get_products('urn');
            $this->set_session('urn_items', $urnItems);
        }
        $items = array(
            'all' => array(),
                //'Metal'=>array(),
                //'Wood'=>array(),
                //'Scattering'=>array(),
                //'Stone'=>array(),
                //'UrnVaults'=>array(),
        );
        foreach ($urnItems as $pid => $item) {
            if ($item['subtype'] == 'Scattering')
                continue;
            if ($item['type'] == 'UrnVaults')
                continue;

            $this->merchs[$pid]['typekey'] = 'urn';
            $this->merchs[$pid]['subtypekey'] = 'all';
            $items['all'][$pid] = $this->merchs[$pid];

            if ($item['subtype'] == 'Metal') {
                //$this->merchs[$pid]['subtypekey'] = 'Metal';
                //$items['Metal'][$pid] = $this->merchs[$pid];
            } elseif ($item['subtype'] == 'Wood') {
                //$this->merchs[$pid]['subtypekey'] = 'Wood';
                //$items['Wood'][$pid] = $this->merchs[$pid];
            } elseif ($item['subtype'] == 'Scattering') {
                //$this->merchs[$pid]['subtypekey'] = 'Scattering';
                //$items['Scattering'][$pid] = $this->merchs[$pid];
            } elseif ($item['subtype'] == 'Ceramic' or $item['subtype'] == 'Porcelain' or $item['subtype'] == 'Stone') {
                //$this->merchs[$pid]['subtypekey'] = 'Stone';
                //$items['Stone'][$pid] = $this->merchs[$pid];
            } elseif ($item['type'] == 'UrnVaults') {
                //$this->merchs[$pid]['subtypekey'] = 'UrnVaults';
                //$items['UrnVaults'][$pid] = $this->merchs[$pid];
            }
        }
        $this->merch_types ['urn']['items'] = $items;

        # Scrattering Urn items
        $urnItems = $this->get_session('urn_items');
        if (!$urnItems) {
            $urnItems = $this->model->get_products('urn');
            $this->set_session('urn_items', $urnItems);
        }
        $items = array(
            'all' => array(),
        );
        foreach ($urnItems as $pid => $item) {
            if ($item['subtype'] != 'Scattering')
                continue;
            if ($pid == 69)
                continue;

            $this->merchs[$pid]['typekey'] = 'urn';
            $this->merchs[$pid]['subtypekey'] = 'all';
            $items['all'][$pid] = $this->merchs[$pid];
        }
        $this->merch_types ['scattering']['items'] = $items;

        # Keepsake items
        $keepsakeItems = $this->get_session('keepsake_items');
        if (!$keepsakeItems or 1) {
            $keepsakeItems = $this->model->get_products('keepsake');
            $this->set_session('keepsake_items', $keepsakeItems);
        }
        $items = array(
            'all' => array(),
        );
        foreach ($keepsakeItems as $pid => $item) {
            //if($item['type'] == 'Jewelry') continue;

            $this->merchs[$pid]['typekey'] = 'keepsake';
            $this->merchs[$pid]['subtypekey'] = 'all';
            $items['all'][$pid] = $this->merchs[$pid];
        }
        $this->merch_types ['keepsake']['items'] = $items;

        # Keepsake Jewelry items
        $jewelryItems = $this->get_session('jewelry_items');
        if (!$jewelryItems) {
            $jewelryItems = $this->model->get_products('jewelry');
            $this->set_session('keepsake_items', $jewelryItems);
        }
        $items = array(
            'all' => array(),
        );
        foreach ($jewelryItems as $pid => $item) {
            //if($item['type'] != 'Jewelry') continue;

            $this->merchs[$pid]['typekey'] = 'jewelry';
            $this->merchs[$pid]['subtypekey'] = 'all';
            $items['all'][$pid] = $this->merchs[$pid];
        }
        $this->merch_types ['jewelry']['items'] = $items;

        # Memorial items
        $memorialPackagePtationaries = $this->get_session('memorial_items');
        if (!$memorialPackagePtationaries) {
            $memorialPackagePtationaries = $this->model->get_products('memorial');
            $this->set_session('memorial_items', $memorialPackagePtationaries);
        }
        $items = array();
        foreach ($memorialPackagePtationaries as $pid => $item) {
            $this->merchs[$pid]['typekey'] = 'memorial';
            $this->merchs[$pid]['subtypekey'] = 'memorial';
            $items['memorial'][$pid] = $this->merchs[$pid];
        }
        $this->merch_types ['memorial']['items'] = $items;

        # Flower items
        //$flowerItems = $this->model->get_flowers();
        $flowerItems = $this->get_session('flower_items');
        if (!$flowerItems) {
            $flowerItems = $this->model->get_products('flower');
            $this->set_session('flower_items', $flowerItems);
        }
        $items = array(
            'Plant' => array(),
            'Basket' => array(),
            'Standing' => array(),
            'Urn' => array(),
            'Fruit' => array(),
        );
        foreach ($flowerItems as $pid => $item) {
            $this->merchs[$pid]['typekey'] = 'flower';
            if ($item['subtype'] == 'Plant') {
                $this->merchs[$pid]['subtypekey'] = 'Plant';
                $items['Plant'][$pid] = $this->merchs[$pid];
            } elseif ($item['subtype'] == 'Basket') {
                $this->merchs[$pid]['subtypekey'] = 'Basket';
                $items['Basket'][$pid] = $this->merchs[$pid];
            } elseif ($item['subtype'] == 'Urn') {
                $this->merchs[$pid]['subtypekey'] = 'Urn';
                $items['Urn'][$pid] = $this->merchs[$pid];
            } elseif ($item['subtype'] == 'Fruit') {
                $this->merchs[$pid]['subtypekey'] = 'Fruit';
                $items['Fruit'][$pid] = $this->merchs[$pid];
            } elseif ($item['subtype'] == 'Standing') {
                $this->merchs[$pid]['subtypekey'] = 'Standing';
                $items['Standing'][$pid] = $this->merchs[$pid];
            }
        }
        $this->merch_types ['flower']['items'] = $items;
        if ($this->get_user_session('packageselected=>id') == 2) {
            $items = array(
                'gold_items' => array(),
            );
            foreach ($flowerItems as $pid => $item) {
                if (in_array($item['pid'], array(1, 2, 10, 8))) {
                    $items['gold_items'][$pid] = $this->merchs[$pid];
                }
            }
            $this->merch_types ['flower']['items'] = $items;
        }
    }

    /*     * **************************************************************************************************
     * OVERVIEW
     * **************************************************************************************************
     */

    public function summary() {
        # Check session
        $this->check_session();

        # Load merch
        $this->fetch_merch_items();

        $this->_notify_message_preparation();


        # Assign page variable
        $vars = array(
            'stepkey' => 'summary',
            'prev_form' => 'merchandise',
            'next_form' => 'vital_information',
            'page_title' => 'Order Summary',
            'merch_types' => $this->merch_types,
            'merch_items' => $this->merchs,
        );
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        if (!$this->input->post('scriptaction')) {
            # Calcelate amont due
        } else {

            $this->go_to($this->input->post('next_form'));
        }


        # Load the step view.
        $this->load_step('summary', $page_title, array('obj' => $this,));
    }

    /*     * **************************************************************************************************
     * PAPERWORK
     * **************************************************************************************************
     */

    public function vital_information() {
        # Check session
        $this->check_session();

        if(!$this->get_user_session("packageselected")){
            show_404();
        }


        # Assign page variable
        $vars = array(
            'stepkey' => 'form',
            'prev_form' => 'summary',
            'next_form' => 'authorization_infomation',
            'page_title' => 'Paperwork - Vital Statistics',
            'fieldprefix' => $this->vitalprefix,
        );

        if ($this->get_user_session('pkgtype') == 'preneed') {
            // $vars['next_form'] = 'insurance_form_1';
            $vars['next_form'] = 'payment';
        }
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        # Check pronoun
        $vars = array(
            'pronoun' => 'decedent',
            'pronoun_cap' => 'Decedent',
        );
        if ($this->get_user_session('pkgtype') == 'preneed') {
            $vars = array(
                'pronoun' => 'member',
                'pronoun_cap' => 'Member',
            );
        }
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        if (!$this->input->post('scriptaction')) {

            # set default of pay for option
            if (!@$this->get_user_session('payfor') or @ $user_session['pkgtype'] == 'atneed') {
                $this->payForChange('full', true);
            }
        } else {
            # Save all field into session
            $this->set_user_session($fieldprefix, $this->input->post());

            # Set validation rules
            $this->form_validation->set_rules('dc_first_name', 'First Name', 'required');
            // $this->form_validation->set_rules('dc_last_name', 'Last Name', 'required');
            // $this->form_validation->set_rules('dc_sex', 'Sex', 'required');
            // $this->form_validation->set_rules('dc_rec', 'Race', 'required');
            // $this->form_validation->set_rules('dc_dob_month', 'Date of Birth', 'required');
            // $this->form_validation->set_rules('dc_dob_day', 'Date of Birth', 'required');
            // $this->form_validation->set_rules('dc_dob_year', 'Date of Birth', 'required');
            // $this->form_validation->set_rules('dc_pob_city', 'Place of Birth', 'required');
            // $this->form_validation->set_rules('dc_pob_state', 'Place of Birth', 'required');
            // $this->form_validation->set_rules('dc_pob_country', 'Place of Birth', 'required');
            // if( $this->get_user_session('pkgtype') == 'atneed' ){
            //     $this->form_validation->set_rules('dc_dod_month', 'Date of Death', 'required');
            //     $this->form_validation->set_rules('dc_dod_day', 'Date of Death', 'required');
            //     $this->form_validation->set_rules('dc_dod_year', 'Date of Death', 'required');
            //     $this->form_validation->set_rules('dc_city_of_death', 'City of Death', 'required');
            //     $this->form_validation->set_rules('dc_state_of_death', 'State of Death', 'required');
            //     $this->form_validation->set_rules('dc_location_of_death', 'Location of Death', 'required');
            //     $this->form_validation->set_rules('dc_pod', 'Name of the Place of Death', 'required');
            //     $this->form_validation->set_rules('dc_county_of_death', 'County of Death', 'required');
            // }
            // $this->form_validation->set_rules('dc_edu_pri', 'Education', 'required');
            // $this->form_validation->set_rules('dc_edu_col', 'Education', 'required');

            // $this->form_validation->set_rules('dc_occupation', 'Usual Occupation (most of life)', 'required');
            // $this->form_validation->set_rules('ssn', 'SSN', 'required');
            // $this->form_validation->set_rules('dc_business', 'Kind of Business', 'required');
            // $this->form_validation->set_rules('dc_company', 'Company', 'required');
            // $this->form_validation->set_rules('dc_marital', 'Marital Status', 'required');

            // $this->form_validation->set_rules('dc_residence_street', 'Residence - Street Address', 'required');
            // $this->form_validation->set_rules('dc_residence_city', 'City/Town', 'required');
            // $this->form_validation->set_rules('dc_residence_citylimit', 'Inside City Limits', 'required');
            // $this->form_validation->set_rules('dc_residence_county', 'County', 'required');
            // $this->form_validation->set_rules('dc_residence_state', 'State', 'required');
            // $this->form_validation->set_rules('dc_residence_zip', 'Zip Code', 'required');
            // $this->form_validation->set_rules('dc_length_residence_city', 'Length of Residence In County', 'required');
            // $this->form_validation->set_rules('dc_father_name', 'Father\'s Full Name', 'required');
            // $this->form_validation->set_rules('dc_mother_name', 'Mother\'s Full Maiden Name', 'required');

            // $this->form_validation->set_rules('pi_first_name', 'First Name', 'required');
            // $this->form_validation->set_rules('pi_last_name', 'Last Name', 'required');
            // $this->form_validation->set_rules('pi_relationship', 'Relationship To Deceased', 'required');
            // $this->form_validation->set_rules('pi_email', 'Email Address', 'required');
            // $this->form_validation->set_rules('pi_address', 'Address', 'required');
            // $this->form_validation->set_rules('pi_city', 'City', 'required');
            // $this->form_validation->set_rules('pi_state', 'State', 'required');
            // $this->form_validation->set_rules('pi_zipcode', 'Zip Code', 'required');
            // $this->form_validation->set_rules('pi_phone', 'Telephone Numberห', 'required');

            if ($this->form_validation->run() == FALSE){
                $this->load->vars(array('error_message' => validation_errors()));
                $this->go_to('vital_information');
            }

            $this->order_archive();

            # Confirm member question was answered
            if ($this->get_user_session('pkgtype') == 'preneed' and ! $this->get_user_session('isbam')) {
                $this->set_user_session('memberQuestionAnswered', true);
            }

            if ($this->get_user_session('payfor') == 'member' and ! @$this->input->post('notvalidate')) {
                $this->go_to('payment');
            } else {
                $this->go_to($this->input->post('next_form'));
            }
        }

        # Load the step view.
        $this->load_step('form_vital', $page_title);
    }

    public function vital_information_print($hash = false) {

        # Check session
        //$this->check_session();
        # Load session from database
       // $hash = $this->input->get('hash');
        //echo htmlspecialchars($this->input->get('hash'));
        parse_str($_SERVER['QUERY_STRING'], $params);
     
        if (CommonFN::get($params['hash']) == "") {
            $this->go_to('packages');
            exit;
        } else {
            $order = $this->model->get_order_by_hash($params['hash']);

            $session = $this->decode_session($order['session']);
           
            $this->set_session('user', $session);
        }

        # Assign page variable
        $vars = array(
            'stepkey' => 'form',
            'prev_form' => 'overview',
            'next_form' => 'authorization_infomation',
            'page_title' => 'Paperwork - Vital Statistics',
            'fieldprefix' => $this->vitalprefix,
        );
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        # Check pronoun
        $vars = array(
            'pronoun' => 'decedent',
            'pronoun_cap' => 'Decedent',
        );
        if ($this->get_user_session('pkgtype') == 'preneed') {
            $vars = array(
                'pronoun' => 'member',
                'pronoun_cap' => 'Member',
            );
        }
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        # Load the step view.
        $this->load_step('form_vital', $page_title, array('print' => 1));
    }

    public function insurance_form_1() {
        # Check session
        $this->check_session();

        if(!$this->get_user_session("packageselected")){
            show_404();
        }


        # Assign page variable
        $vars = array(
            'stepkey' => 'form',
            'prev_form' => 'vital_information',
            'next_form' => 'payment',
            'page_title' => 'Paperwork - Insurance Form 1',
            'fieldprefix' => $this->insurance1prefix,
        );
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        # Check pronoun
        $vars = array(
            'pronoun' => 'decedent',
            'pronoun_cap' => 'Decedent',
        );
        if ($this->get_user_session('pkgtype') == 'preneed') {
            $vars = array(
                'pronoun' => 'member',
                'pronoun_cap' => 'Member',
            );
        }
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        if (!$this->input->post('scriptaction')) {

            $vital = $this->get_user_session($this->vitalprefix);
            $this->get_user_session($fieldprefix . '=>insured_name') or $this->set_user_session($fieldprefix . '=>insured_name', @$vital['dc_first_name'] . ' ' . @$vital['dc_middle_name'] . ' ' . @$vital['dc_last_name']);
            $this->get_user_session($fieldprefix . '=>insured_name2') or $this->set_user_session($fieldprefix . '=>insured_name2', @$vital['dc_first_name'] . ' ' . @$vital['dc_middle_name'] . ' ' . @$vital['dc_last_name']);
            $this->get_user_session($fieldprefix . '=>insured_sex') or $this->set_user_session($fieldprefix . '=>insured_sex', @$vital['dc_sex']);
            $this->get_user_session($fieldprefix . '=>insured_birth_month') or $this->set_user_session($fieldprefix . '=>insured_birth_month', @$vital['dc_dob_month']);
            $this->get_user_session($fieldprefix . '=>insured_birth_day') or $this->set_user_session($fieldprefix . '=>insured_birth_day', @$vital['dc_dob_day']);
            $this->get_user_session($fieldprefix . '=>insured_birth_year') or $this->set_user_session($fieldprefix . '=>insured_birth_year', @$vital['dc_dob_year']);

            $month = date("m", strtotime(@$vital['dc_dob_month']));
            $born = @$vital['dc_dob_year']."-".$month."-".@$vital['dc_dob_day'];
            $age = date_diff(date_create($born), date_create('today') )->y;
            $this->get_user_session($fieldprefix . '=>insured_age') or $this->set_user_session($fieldprefix . '=>insured_age', $age );

            $this->get_user_session($fieldprefix . '=>insured_mailing_address') or $this->set_user_session($fieldprefix . '=>insured_mailing_address', @$vital['dc_residence_street']);
            $this->get_user_session($fieldprefix . '=>insured_city') or $this->set_user_session($fieldprefix . '=>insured_city', @$vital['dc_residence_city']);
            $this->get_user_session($fieldprefix . '=>insured_state') or $this->set_user_session($fieldprefix . '=>insured_state', @$vital['dc_residence_state']);
            $this->get_user_session($fieldprefix . '=>insured_zip') or $this->set_user_session($fieldprefix . '=>insured_zip', @$vital['dc_residence_zip']);
            $this->get_user_session($fieldprefix . '=>insured_phone') or $this->set_user_session($fieldprefix . '=>insured_phone', @$vital['pi_phone']);
            $this->get_user_session($fieldprefix . '=>insured_beneficiaries') or $this->set_user_session($fieldprefix . '=>insured_beneficiaries', 'Estate of ' . @$vital['dc_first_name'] . ' ' . @$vital['dc_last_name']);
            //$this->get_user_session($fieldprefix.'=>insured_signed_at')   or $this->set_user_session($fieldprefix.'=>insured_signed_at', date('H:i'));
            $this->get_user_session($fieldprefix . '=>insured_signed_date') or $this->set_user_session($fieldprefix . '=>insured_signed_date', date('F j, Y'));
            $this->get_user_session($fieldprefix . '=>insured_signed_insured') or $this->set_user_session($fieldprefix . '=>insured_signed_insured', @$vital['dc_first_name'] . ' ' . @$vital['dc_middle_name'] . ' ' . @$vital['dc_last_name']);
            $this->order_archive();
        } else {
            # Save all field into session
            $this->set_user_session($fieldprefix, $this->input->post());

            # Set validation rules
            $this->form_validation->set_rules('insured_name', 'Full Name', 'required');
            $this->form_validation->set_rules('insured_name2', 'INSURED�?S NAME', 'required');
            $this->form_validation->set_rules('insured_ssn', 'Social Security #', 'required');
            $this->form_validation->set_rules('insured_sex', 'Sex', 'required');
            $this->form_validation->set_rules('insured_birth_month', 'Date of Birth', 'required');
            $this->form_validation->set_rules('insured_birth_day', 'Date of Birth', 'required');
            $this->form_validation->set_rules('insured_birth_year', 'Date of Birth', 'required');
            $this->form_validation->set_rules('insured_age', 'Age', 'required');
            $this->form_validation->set_rules('insured_mailing_address', 'Mailing Address', 'required');
            $this->form_validation->set_rules('insured_city', 'City', 'required');
            $this->form_validation->set_rules('insured_state', 'State', 'required');
            $this->form_validation->set_rules('insured_zip', 'Zip Code', 'required');
            $this->form_validation->set_rules('insured_phone', 'Telephone Number', 'required');
            $this->form_validation->set_rules('insured_beneficiaries', 'Beneficiaries', 'required');
            $this->form_validation->set_rules('insured_signed_insured', 'Zip Code', 'required');
            $this->form_validation->set_rules('insured_signed_at', 'Insured', 'required');

            if ($this->input->post('notvalidate')) {
                # go next form
                $this->go_to($this->input->post('next_form'));
            } elseif ($this->form_validation->run()) {
                $this->order_archive();
                # go next form
                $this->go_to($this->input->post('next_form'));
            } else {
                $this->load->vars(array('error_message' => validation_errors()));
            }
        }

        # Load the step view.
        $this->load_step('form_insurance_1', $page_title);
    }

    public function insurance_form_1_print() {
        # Check session
        //$this->check_session();
        # Load session from database
        $hash = @$this->input->get('hash');
        if (!$hash) {
            $this->go_to('packages');
            exit;
        } else {
            $order = $this->model->get_order_by_hash($hash);
            $session = $this->decode_session($order['session']);
            $this->set_session('user', $session);
        }

        # Assign page variable
        $vars = array(
            'stepkey' => 'form',
            'prev_form' => 'vital_information',
            'next_form' => 'insurance_form_2',
            'page_title' => 'Paperwork - Insurance Form 1',
            'fieldprefix' => $this->insurance1prefix,
        );
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        # Check pronoun
        $vars = array(
            'pronoun' => 'decedent',
            'pronoun_cap' => 'Decedent',
        );
        if ($this->get_user_session('pkgtype') == 'preneed') {
            $vars = array(
                'pronoun' => 'member',
                'pronoun_cap' => 'Member',
            );
        }
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        # Load the step view.
        $this->load_step('form_insurance_1_print', $page_title, array('print' => 1));
    }

    public function insurance_form_2() {
        # Check session
        $this->check_session();

        if(!$this->get_user_session("packageselected")){
            show_404();
        }


        # Assign page variable
        $vars = array(
            'stepkey' => 'form',
            'prev_form' => 'insurance_form_1',
            'next_form' => 'payment',
            'page_title' => 'Paperwork - Insurance Form 2',
            'fieldprefix' => $this->insurance2prefix,
        );
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        # Check pronoun
        $vars = array(
            'pronoun' => 'decedent',
            'pronoun_cap' => 'Decedent',
        );
        if ($this->get_user_session('pkgtype') == 'preneed') {
            $vars = array(
                'pronoun' => 'member',
                'pronoun_cap' => 'Member',
            );
        }
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        if (!$this->input->post('scriptaction')) {

            $ins1 = $this->get_user_session($this->insurance1prefix);
            !$this->get_user_session($fieldprefix . '=>ra_insured') and $this->set_user_session($fieldprefix . '=>ra_insured', @$ins1['pi_first_name'] . ' ' . @$ins1['pi_middle_name'] . ' ' . @$ins1['pi_middle_name']);
            !$this->get_user_session($fieldprefix . '=>ra_fh') and $this->set_user_session($fieldprefix . '=>ra_fh', @$this->settings['client_company_name_full']);
            !$this->get_user_session($fieldprefix . '=>ra_city_state') and $this->set_user_session($fieldprefix . '=>ra_city_state', @$this->settings['client_address_city'] . ', ' . @$this->settings['client_address_state']);
            !$this->get_user_session($fieldprefix . '=>ra_phone') and $this->set_user_session($fieldprefix . '=>ra_phone', @$this->settings['client_phone_local']);

            $this->order_archive();
        } else {
            # Save all field into session
            $this->set_user_session($fieldprefix, $this->input->post());

            # Set validation rules
            $this->form_validation->set_rules('ra_insured', 'Insured', 'required');
            $this->form_validation->set_rules('ra_fh', 'Funeral Home', 'required');
            $this->form_validation->set_rules('ra_city_state', 'Address (City & State)', 'required');
            $this->form_validation->set_rules('ra_phone', 'Telephone Number (if known)', 'required');
            $this->form_validation->set_rules('ra_signature', 'Signature of Policyowner', 'required');

            if ($this->input->post('notvalidate')) {
                # go next form
                $this->go_to($this->input->post('next_form'));
            } elseif ($this->form_validation->run()) {
                $this->order_archive();
                # go next form
                $this->go_to($this->input->post('next_form'));
            } else {
                $this->load->vars(array('error_message' => validation_errors()));
            }
        }

        # Load the step view.
        $this->load_step('form_insurance_2', $page_title);
    }

    public function insurance_form_2_print() {
        # Check session
        //$this->check_session();
        # Load session from database
        $hash = @$this->input->get('hash');
        if (!$hash) {
            $this->go_to('packages');
            exit;
        } else {
            $order = $this->model->get_order_by_hash($hash);
            $session = $this->decode_session($order['session']);
            $this->set_session('user', $session);
        }

        # Assign page variable
        $vars = array(
            'stepkey' => 'form',
            'prev_form' => 'insurance_form_1',
            'next_form' => 'payment',
            'page_title' => 'Paperwork - Insurance Form 2',
            'fieldprefix' => $this->insurance2prefix,
        );
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        # Check pronoun
        $vars = array(
            'pronoun' => 'decedent',
            'pronoun_cap' => 'Decedent',
        );
        if ($this->get_user_session('pkgtype') == 'preneed') {
            $vars = array(
                'pronoun' => 'member',
                'pronoun_cap' => 'Member',
            );
        }
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        # Load the step view.
        $this->load_step('form_insurance_2', $page_title, array('print' => 1));
    }

    public function authorization_infomation() {
        # Check session
        $this->check_session();

        if(!$this->get_user_session("packageselected")){
            show_404();
        }


        # Assign page variable
        $vars = array(
            'stepkey' => 'form',
            'prev_form' => 'vital_information',
            'next_form' => 'payment',
            'page_title' => 'Paperwork - Cremation Authorization Form',
            'fieldprefix' => $this->authprefix,
        );
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        # Check pronoun
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        if (!$this->input->post('scriptaction')) {

            //$this->set_user_session($fieldprefix, '');

            $vital = $this->get_user_session($this->vitalprefix);
            !$this->get_user_session($fieldprefix . '=>decedent_name') and $this->set_user_session($fieldprefix . '=>decedent_name', @$vital['dc_first_name'] . ' ' . @$vital['dc_middle_name'] . ' ' . @$vital['dc_last_name']);
            !$this->get_user_session($fieldprefix . '=>fd_addres') and $this->set_user_session($fieldprefix . '=>fd_addres', @$vital['pi_address']);
            !$this->get_user_session($fieldprefix . '=>fd_city') and $this->set_user_session($fieldprefix . '=>fd_city', @$vital['pi_city']);
            !$this->get_user_session($fieldprefix . '=>fd_state') and $this->set_user_session($fieldprefix . '=>fd_state', @$vital['pi_state']);
            !$this->get_user_session($fieldprefix . '=>fd_zip') and $this->set_user_session($fieldprefix . '=>fd_zip', @$vital['pi_zipcode']);
            !$this->get_user_session($fieldprefix . '=>aa_hisher') and $this->set_user_session($fieldprefix . '=>aa_hisher', @$vital['pi_relationship']);
            !$this->get_user_session($fieldprefix . '=>aa_stateof') and $this->set_user_session($fieldprefix . '=>aa_stateof', ucwords(strtolower(@$this->get_user_session('stateselected'))));

            !$this->get_user_session($fieldprefix . '=>sa_name') and $this->set_user_session($fieldprefix . '=>sa_name', @$vital['pi_first_name'] . ' ' . @$vital['pi_last_name']);
            !$this->get_user_session($fieldprefix . '=>sa_name_again') and $this->set_user_session($fieldprefix . '=>sa_name_again', @$vital['pi_first_name'] . ' ' . @$vital['pi_last_name']);
            !$this->get_user_session($fieldprefix . '=>sa_relationship') and $this->set_user_session($fieldprefix . '=>sa_relationship', @$vital['pi_relationship']);
            !$this->get_user_session($fieldprefix . '=>sa_email') and $this->set_user_session($fieldprefix . '=>sa_email', @$vital['pi_email']);
            !$this->get_user_session($fieldprefix . '=>sa_phone') and $this->set_user_session($fieldprefix . '=>sa_phone', @$vital['pi_phone']);
            !$this->get_user_session($fieldprefix . '=>sa_address') and $this->set_user_session($fieldprefix . '=>sa_address', @$vital['pi_address']);
            !$this->get_user_session($fieldprefix . '=>sa_city') and $this->set_user_session($fieldprefix . '=>sa_city', @$vital['pi_city']);
            !$this->get_user_session($fieldprefix . '=>sa_state') and $this->set_user_session($fieldprefix . '=>sa_state', @$vital['pi_state']);
            !$this->get_user_session($fieldprefix . '=>sa_zip') and $this->set_user_session($fieldprefix . '=>sa_zip', @$vital['pi_zipcode']);

            !$this->get_user_session($fieldprefix . '=>sa_this') and $this->set_user_session($fieldprefix . '=>sa_this', date('jS'));
            !$this->get_user_session($fieldprefix . '=>sa_dayof_month') and $this->set_user_session($fieldprefix . '=>sa_dayof_month', date('F'));
            !$this->get_user_session($fieldprefix . '=>sa_dayof_year') and $this->set_user_session($fieldprefix . '=>sa_dayof_year', date('Y'));

            $this->order_archive();
        } else {
            # Save all field into session
            $this->set_user_session($fieldprefix, $this->input->post());

            # Set validation rules
            $this->form_validation->set_rules('decedent_name', 'Decedent Name', 'required');
            /* $this->form_validation->set_rules('init_1', 'INITIALS OF AUTHORIZING AGENT', 'required');
              $this->form_validation->set_rules('init_2', 'INITIALS OF AUTHORIZING AGENT', 'required');
              $this->form_validation->set_rules('init_2_items', 'List of devices', 'required'); */
            $this->form_validation->set_rules('fd_addres', 'Final Desposition Address', 'required');
            $this->form_validation->set_rules('fd_city', 'Final Desposition Address', 'required');
            $this->form_validation->set_rules('fd_state', 'Final Desposition Address', 'required');
            $this->form_validation->set_rules('fd_zip', 'Final Desposition Address', 'required');
            //$this->form_validation->set_rules('fd_other_instruction', 'Final Desposition Address', 'required');
            $this->form_validation->set_rules('init_3', 'INITIALS OF AUTHORIZING AGENT', 'required');
            #$this->form_validation->set_rules('aa_hisher', 'Relationship', 'required');
            #$this->form_validation->set_rules('aa_capacityof', 'Relationship', 'required');
            $this->form_validation->set_rules('aa_stateof', 'State', 'required');
            $this->form_validation->set_rules('init_4', 'INITIALS OF AUTHORIZING AGENT', 'required');
            $this->form_validation->set_rules('sa_executedat', 'Executed at', 'required');
            $this->form_validation->set_rules('sa_this', 'Executed at', 'required');
            $this->form_validation->set_rules('sa_dayof_month', 'Executed at', 'required');
            $this->form_validation->set_rules('sa_dayof_year', 'Executed at', 'required');
            $this->form_validation->set_rules('sa_name', 'Enter name', 'required');
            $this->form_validation->set_rules('sa_name_again', 'Enter name again', 'required');
            $this->form_validation->set_rules('sa_relationship', 'Relationship to Decedent', 'required');
            $this->form_validation->set_rules('sa_email', 'E-mail', 'required');
            $this->form_validation->set_rules('sa_phone', 'Phone number', 'required');
            $this->form_validation->set_rules('sa_address', 'Address', 'required');
            $this->form_validation->set_rules('sa_city', 'City', 'required');
            $this->form_validation->set_rules('sa_state', 'State', 'required');
            $this->form_validation->set_rules('sa_zip', 'Zip Code', 'required');

            if ($this->input->post('notvalidate')) {
                # go next form
                $this->go_to($this->input->post('next_form'));
            } elseif ($this->form_validation->run()) {
                $this->order_archive();
                # go next form
                $this->go_to($this->input->post('next_form'));
            } else {
                $this->load->vars(array('error_message' => validation_errors()));
            }
        }


        # Load the step view.
        $this->load_step('form_auth', $page_title);
    }

    public function authorization_infomation_standalone($new = false) {

        # Check to clear session if the form is opened for a new submitted
        if ($new) {
            $this->destroy_session();
            $this->go_to('authorization_infomation_standalone');
            return;
        }

        # Set standalone flag
        $this->set_user_session('standalone', true);

        # Email configure
        $email_from = $this->settings['email_atneed_from'];
        $email_to = $this->settings['email_atneed_to'];

        # Get step for standalone page
        $this->steps = $this->get_steps_for_authstd();

        # Assign page variable
        $vars = array(
            'stepkey' => 'authstd',
            'next_form' => 'authorization_infomation_standalone_thankyou',
            'page_title' => 'Cremation Authorization Form',
            'fieldprefix' => $this->authstdprefix,
            'steps' => $this->steps,
        );
        $this->load->vars($vars);
        extract($vars);

        # Check pronoun
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        if (!$this->input->post('scriptaction')) {

        } else {
            /*
             * Save all field into session
             */
            //$old_user_session = $this->get_session('user');
            //$this->set_user_session($fieldprefix, $this->input->post());
            if (!$this->order_completed()) {
                $this->set_user_session($fieldprefix, $this->input->post());
                //$this->set_session('user', $old_user_session);
            }

            /*
             * Perform submitting
             */
            if ($this->input->post('print_flag')) {
                /*
                 * Save abandon archive and go to print page
                 */
                $this->order_archive_standalone();
                $this->go_to('authorization_infomation_print?hash=' . $this->get_user_session('hash'));
            } else {
                if (!$this->order_completed()) {
                    /*
                     * Prepare email template information
                     *
                     * Authorization Agent information
                     */
                    $this->putMesTbl('[clear]');
                    $this->putMesTbl('Name', $this->get_user_session($fieldprefix . '=>sa_name'));
                    $this->putMesTbl('Relationship', $this->get_user_session($fieldprefix . '=>sa_relationship'));
                    $this->putMesTbl('E-mail', $this->get_user_session($fieldprefix . '=>sa_email'));
                    $this->putMesTbl('Phone number', $this->get_user_session($fieldprefix . '=>sa_phone'));
                    $sa_address = array();
                    $this->get_user_session($fieldprefix . '=>sa_address') and $sa_address[] = $this->get_user_session($fieldprefix . '=>sa_address');
                    $this->get_user_session($fieldprefix . '=>sa_city') and $sa_address[] = '<br>' . $this->get_user_session($fieldprefix . '=>sa_city') . ',';
                    $this->get_user_session($fieldprefix . '=>sa_state') and $sa_address[] = $this->get_user_session($fieldprefix . '=>sa_state');
                    $this->get_user_session($fieldprefix . '=>sa_zip') and $sa_address[] = $this->get_user_session($fieldprefix . '=>sa_zip');
                    $authagenttable = $this->putMesTbl('Address', implode(' ', $sa_address));
                    $this->set_user_session('authagenttable', $authagenttable);
                    /*
                     * Other information
                     */
                    $this->set_user_session('deceased_name', $this->get_user_session($fieldprefix . '=>decedent_name'));
                    $this->set_user_session('phonenumber', $this->settings['client_phone_local']);
                    $informant_email = $this->get_user_session($this->authstdprefix . '=>sa_email');
                    $user_session = $this->get_user_session();
                    /*
                     * Send emails
                     *
                     * for client -----------------------------------
                     */
                    $from = $email_from;
                    $subject = $this->settings['client_company_name_short'] . " - Cremation Authorization Form.";
                    $message = $this->template('assets/templates/authstd.tmpl.html', $user_session);
                    if (count($email_to)) {
                        foreach ($email_to as $v) {
                            $to = $v;
                            if(is_debugger($informant_email)){
                                $to = $informant_email;
                            }
                            send_mail($from, $to, $subject, $message);
                        }
                    }
                    /*
                     * for customer -----------------------------------
                     */
                    $to = $informant_email;
                    $subject = "Thank you for your submission.";
                    send_mail($from, $to, $subject, $message);
                    /*
                     * Save archive
                     */
                    $this->order_archive_standalone('Complete');
                }
                /*
                 * Go to next page
                 */
                $this->go_to($this->input->post('next_form'));
            }
        }


        # Load the step view.
        $this->load_step('form_auth', $page_title);
    }

    public function authorization_infomation_standalone_thankyou() {
        # Check session
        if (!$this->get_user_session('standalone') and ! $this->get_user_session('order_archive_id')) {
            $this->go_to('authorization_infomation_standalone');
        }

        # Get step for standalone page
        $this->steps = $this->get_steps_for_authstd();

        # Assign page variable
        $vars = array(
            'stepkey' => 'thankyou',
            'page_title' => 'Thank you',
            'steps' => $this->steps,
        );
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        # Load the step view.
        $this->load_step('authorization_infomation_standalone_thankyou', $page_title, array('not_calculate_amount_due' => true,));
    }

    public function authorization_infomation_print() {
        # Check session
        //$this->check_session();
        # Load session from database
        //$hash = @$this->input->get('hash');

        parse_str($_SERVER['QUERY_STRING'], $params);
     
        if (CommonFN::get($params['hash']) == "") {
            $this->go_to('packages');
            exit;
        } else {
            $order = $this->model->get_order_by_hash($params['hash']);
            $session = $this->decode_session($order['session']);
            $this->set_session('user', $session);
        }

        # Assign page variable
        $vars = array(
            'stepkey' => 'form',
            'prev_form' => 'vital_information',
            'next_form' => 'payment',
            'page_title' => 'Paperwork - Cremation Authorization Form',
            'fieldprefix' => $this->authprefix,
        );
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        # Check pronoun
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        # Load the step view.
        $this->load_step('form_auth_print', $page_title, array('print' => 1));
    }

    public function ajax_update_vital_selected() {
        $rt = array();
        if (!$this->get_user_session('pkgtype')) {
            # Session was lost
            $rt['status'] = 'error';
            $rt['error_code'] = 'seslost';
        } else {
            if (@$_SESSION['tProc'] < $this->input->post('tProc')) {
                $_SESSION['tProc'] = $this->input->post('tProc');

                # Process coding **************************************************
                # Update selected
                if (@$this->input->post('obitonweb')) {
                    $this->set_user_session('obitonweb', array(
                        'name' => 'Publish obituary on website',
                        'price' => $this->settings['publish_obit_onweb_fee'],
                    ));
                    $this->set_user_session($this->vitalprefix . '=>obit_is_publish_on_our_web', 1);
                } else {
                    $this->set_user_session('obitonweb', '');
                    $this->set_user_session($this->vitalprefix . '=>obit_is_publish_on_our_web', 0);
                }

                # Return coding **************************************************

                $this->_calculate_amount_due();
                $rt['status'] = 'complete';
                $rt['tProc'] = $this->input->post('tProc');
                $rt['tracker'] = $this->load->view('_right_bar_tracker', array(
                    'user_session' => $this->get_user_session(),
                    'services' => $this->services,
                        ), true);
            }
        }

        echo json_encode($rt);
    }

    /*     * **************************************************************************************************
     * CHECKOUT
     * **************************************************************************************************
     */

    public function payment() {
        # Check session
        $this->check_session();
        # Check Form vital and Cremation Auth detail
        if(!$this->get_user_session("formvital")){
            show_404();
        }

        if(!$this->get_user_session("isbam")){
            if($this->get_user_session("pkgtype")!="preneed"){
                if(!$this->get_user_session("formauth")){
                    show_404();
                }
            }
            else{
                // if(!$this->get_user_session("forminsure1")){
                //     show_404();
                // }
            }
        }
        # Assign page variable
        $vars = array(
            'stepkey' => 'payment',
            // 'prev_form' => 'insurance_form_1',
            'prev_form' => 'vital_information',
            'next_form' => 'confirmation',
            'page_title' => 'PAYMENT',
            'fieldprefix' => $this->paymentprefix,
        );
        if ($this->get_user_session('pkgtype') == 'atneed') {
            $vars['prev_form'] = 'authorization_infomation';
        }
        if ($this->get_user_session('payfor') == 'member') {
            $vars['prev_form'] = 'vital_information';
        }
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }
        # now This client have only credit card payment
        $this->set_user_session($fieldprefix . '=>payment_method', 'cc');
        if (!$this->input->post('scriptaction')) {
            $this->_calculate_amount_due(true);
            $this->order_archive();
        } else {
            # Save all field into session
            $this->set_user_session($fieldprefix, $this->input->post());

            if($this->input->post('payfor')){
                $this->payForChange($this->input->post('payfor'), true);
            }

            # Modify CCNUM
            $ccnum = $this->get_user_session($fieldprefix . '=>ccnum');
            $ccnum = str_replace('-', '', $ccnum);
            $ccnum = str_replace(' ', '', $ccnum);
            $this->set_user_session($fieldprefix . '=>ccnum', $ccnum);

            //if($this->get_user_session('pkgtype') == 'atneed'){
            # Payment Method for atneed
            $this->set_user_session($fieldprefix . '=>payment_method', 'cc');

            $this->order_archive();

            # go next form
            $this->go_to($this->input->post('next_form'));
            /* }else{
              # Payment Method for preneed
              $this->set_user_session($fieldprefix.'=>payment_method', 'nopay');
              $this->set_user_session($fieldprefix.'=>pay_text', 'No Payment');
              if($this->input->post('contactme')){
              $this->set_user_session($fieldprefix.'=>payment_method', 'contact');
              $this->set_user_session($fieldprefix.'=>pay_text', 'Contact for Payment ('.$this->input->post('contactme_phone').')');
              }

              $this->order_archive();
              # go next form
              $this->go_to($this->input->post('next_form'));
              } */
        }

        # Load the step view.
        //if($this->get_user_session('pkgtype') == 'atneed'){
        $this->load_step('form_payment', $page_title, array('include_membership' => true,));
        /* }else{
          $this->load_step('form_payment_preneed', $page_title);
          } */
    }

    public function payForChange($option, $force = false) {
        $rt = array();
        if (!$this->get_user_session('pkgtype')) {
            # Session was lost
            $rt['status'] = 'error';
            $rt['error_code'] = 'seslost';
        } else {
            if (@$_SESSION['tProc'] < $this->input->post('tProc') or $force) {
                $_SESSION['tProc'] = $this->input->post('tProc');

                # Process coding **************************************************
                $optiontext = 'Membership Fee Only';
                if ($option != 'member') {
                    $option = 'full';
                    $optiontext = 'Pay in Full for the Arrangement';
                }

                $this->set_user_session('payfor', $option);
                $this->set_user_session('payfortext', $optiontext);

                # Return coding **************************************************

                $this->_calculate_amount_due();
                $rt['status'] = 'complete';
                $rt['tProc'] = $this->input->post('tProc');
                $rt['option'] = $option;
                $params = array(
                    'user_session' => $this->get_user_session(),
                    'services' => $this->services,
                );
                if ($this->input->post('stepKey')) {
                    $params['stepkey'] = $this->input->post('stepKey');
                }
                $rt['tracker'] = $this->load->view('_right_bar_tracker', $params, true);
            }
        }

        if (!$force) {
            echo json_encode($rt);
        }
    }

    /*     * **************************************************************************************************
     * CONFIRMATION
     * **************************************************************************************************
     */

    public function confirmation() {
        # Check session
        $this->check_session();
        if(!$this->get_user_session("formvital")){
            show_404();
        }

        if(!$this->get_user_session("isbam")){
            if($this->get_user_session("pkgtype")!="preneed"){
                if(!$this->get_user_session("formauth")){
                    show_404();
                }
            }
            else{
                // if(!$this->get_user_session("forminsure1")){
                //     show_404();
                // }
            }
        }
        /*
         * Drump session
         */
        $user_session = $this->get_user_session();

        /*
         * Email configure
         */
        $email_from = $this->settings['email_' . @$user_session['pkgtype'] . '_from'];
        $email_to = $this->settings['email_' . @$user_session['pkgtype'] . '_to'];
        $informant_email = $user_session[$this->vitalprefix]['pi_email'];
        # Check if order was commpleted
        if (!$this->order_completed()) {

            if (!$this->get_user_session('uniqid')) {
                $user_session = $this->set_user_session('uniqid', $this->generate_uniq_id());
            }
            //$this->order_archive();
            //echo " payment_method: ".$this->get_user_session($this->paymentprefix . '=>payment_method')." " ;
            if ($this->get_user_session($this->paymentprefix . '=>payment_method') == 'cc') {
                //$amount = getTestPrice($_SESSION['summary']['total']);
                $amount = $user_session['summary']['total'];
                if (@$user_session['payfor'] == 'member') {
                    $amount = $user_session['summary']['total_member'];
                }
                if(is_debugger($informant_email)){
                    $amount = rand(100, 119) / 100;
                }
                $amount = number_format($amount, 2, '.', '');

                if (strtolower($user_session[$this->paymentprefix]['ccname']) != 'funeralnet') {

                    /* -------------------------------------------------------------------------------------
                     * Begin area for charging credit card
                     * -------------------------------------------------------------------------------------
                     */
                    if( $user_session[$this->paymentprefix]['ccname'] == '' or $user_session[$this->paymentprefix]['ccname'] == '' or $user_session[$this->paymentprefix]['ccexpmonth'] == '' or $user_session[$this->paymentprefix]['ccexpyear'] == '' or $user_session[$this->paymentprefix]['cccvv'] == '' ){
                        show_404() ;
                    }

                    $bluefin = $this->cfg['bluefin'];

                    $names = array_reverse(explode(' ', $user_session[$this->paymentprefix]['ccname']));
                    $fname = array_pop($names);
                    $lname = implode(' ', array_reverse($names));

                    $this->bluefin->setParam('account_id', $bluefin['account_id']);
                    $this->bluefin->setParam('api_accesskey', $bluefin['access_key']);
                    $this->bluefin->setParam('ip_address', $_SERVER['REMOTE_ADDR']);
                    $this->bluefin->setParam('transaction_type', 'SALE');
                    $this->bluefin->setParam('transaction_amount', $amount);

                    $this->bluefin->setParam('tender_type', 'CARD');
                    $this->bluefin->setParam('card_number', $user_session[$this->paymentprefix]['ccnum']);
                    $this->bluefin->setParam('card_expiration', substr('0' . $user_session[$this->paymentprefix]['ccexpmonth'], -2) . substr($user_session[$this->paymentprefix]['ccexpyear'], -2));

                    $this->bluefin->setParam('first_name', $fname);
                    $this->bluefin->setParam('last_name', $lname);
                    $this->bluefin->setParam('street_address1', $user_session[$this->paymentprefix]['ccaddress'] . ' ' . $user_session[$this->paymentprefix]['ccaddress2']);
                    $this->bluefin->setParam('city', $user_session[$this->paymentprefix]['cccity']);
                    $this->bluefin->setParam('state', $user_session[$this->paymentprefix]['ccstate']);
                    $this->bluefin->setParam('zip', $user_session[$this->paymentprefix]['cczip']);
                    $this->bluefin->setParam('country', 'US');
                    $this->bluefin->setParam('email', $user_session[$this->vitalprefix]['pi_email']);
                    $this->bluefin->setParam('phone', $user_session[$this->vitalprefix]['pi_phone']);

                    $this->bluefin->setParam('transaction_description', ucfirst($user_session['pkgtype']) . ' - ' . $user_session['packageselected']['name'] . ' - $' . $amount);

                    $result = $this->bluefin->process();
                    $rinfo = $this->bluefin->getResponse();

                    if ($result) {
                        $user_session = $this->set_user_session('ccorder', array(
                            'result' => 'Accepted',
                            'rinfo' => $rinfo,
                        ));
                    } else {
                        $user_session = $this->set_user_session('ccorder', array(
                            'result' => 'Declined',
                            'rinfo' => $rinfo,
                        ));
                    }

                    /* -------------------------------------------------------------------------------------
                     * End area for charging credit card
                     * -------------------------------------------------------------------------------------
                     */

                    # Write log file
                    $payment_form = $user_session[$this->paymentprefix];
                    $payment_form['ccnum'] = '****' . substr($payment_form['ccnum'], -4);
                    $payment_form['cccvv'] = '***';
                    $logdata = array(
                        'order_archive_id' => $user_session['order_archive_id'],
                        'uniqid' => $user_session['uniqid'],
                        'summary' => $user_session['summary'],
                        'paymentform' => $payment_form,
                        'ccorder' => $user_session['ccorder'],
                    );
                    $this->savePaymentLog($logdata);
                } else if(strtolower($user_session[$this->paymentprefix]['ccname']) == 'funeralnet' and is_debugger($informant_email)) {
                    $user_session = $this->set_user_session('ccorder', array(
                        'result' => 'Accepted',
                        'reason' => 'Test Transaction',
                    ));
                }
                else{
                    $user_session = $this->set_user_session('ccorder', array(
                        'result' => 'Declined',
                        'reason' => 'No dev',
                    ));
                }
            }
            else {
                $user_session = $this->set_user_session('ccorder', array(
                    'result' => 'Accepted',
                    'reason' => 'No Payment Transaction',
                ));
            }


            # Write log file
            $logdata = array(
                'order_archive_id' => $user_session['order_archive_id'],
                'uniqid' => $user_session['uniqid'],
                'package_name' => $user_session['packageselected']['name'],
                'informant_name' => $user_session[$this->vitalprefix]['pi_first_name'] . ' ' . $user_session[$this->vitalprefix]['pi_last_name'],
                'informant_phone' => $user_session[$this->vitalprefix]['pi_phone'],
                'informant_email' => $user_session[$this->vitalprefix]['pi_email'],
                'payment_method' => $this->get_user_session($this->paymentprefix . '=>payment_method'),
                'pay_amount' => $amount,
                'summary' => $user_session['summary'],
            );
            $this->saveTransactionLog($logdata);

            if (preg_match("/Accepted/i", $user_session['ccorder']['result'])) {
                # for CC accepted

                $mail_messages = array();
                $this->set_user_session('typetext', @$user_session['pkgtype'] == 'atneed' ? 'At-Need' : 'Pre-Need');
                if ($this->isBam) {
                    $this->set_user_session('typetext', 'Become a Member');
                }

                $this->set_user_session('paymenttypetext', 'Credit Card');
                if (@$user_session[$this->paymentprefix]['insurance']) {
                    $this->set_user_session('paymenttypetext', 'Requesting Insurance Options');
                } elseif ($user_session[$this->paymentprefix]['payment_method'] != 'cc') {
                    $this->set_user_session('paymenttypetext', $this->get_user_session($this->paymentprefix . '=>pay_text'));
                }

                $this->set_user_session('pronoun', @$user_session['pkgtype'] == 'atneed' ? 'Decedent' : 'Member');
                $this->set_user_session('funeralhome', $this->settings['client_company_name_full']);
                $this->set_user_session('amountpaid', '$' . number_format(@$user_session['summary']['total'], 2) . ' (' . @$user_session['payfortext'] . ')');
                if (@$user_session['payfor'] == 'member') {
                    $this->set_user_session('amountpaid', '$' . number_format(@$user_session['summary']['total_member'], 2) . ' (' . @$user_session['payfortext'] . ')');
                }
                if ($this->get_user_session($this->paymentprefix . '=>payment_method') != 'cc') {
                    $this->set_user_session('amountpaid', '$0.00');
                }
                $this->set_user_session('funeralhomeaddress', $this->settings['client_address_street'], ' ' . $this->settings['client_address_city'] . ' ' . $this->settings['client_address_state'] . ' ' . $this->settings['client_address_zip']);
                $this->set_user_session('phonenumbernonlocal', $this->settings['client_phone_nonlocal']);
                $this->set_user_session('faxnumber', $this->settings['client_fax']);
                $this->set_user_session('funeralhomeemail', $this->settings['client_email']);
                $this->set_user_session('locationselected', $user_session['countyselected'] . ', ' . $user_session['stateselected']);

                $this->set_user_session('client_address_city', $this->settings['client_address_city']);
                $this->set_user_session('client_address_state', $this->settings['client_address_state']);
                $this->set_user_session('client_address_zip', $this->settings['client_address_zip']);

                $user_session = $this->set_user_session('phonenumber', $this->settings['client_phone_local']);

                # perform send sms
                /* $_SESSION['smsmassage'] = array();
                  foreach($smscfg['to'] as $to){
                  $msg = $_SESSION['isbam'] ? $smscfg['messagebam'] : $smscfg['message'];
                  $msg = str_replace('[url]', getBLShortenUrl('http://www.legacycremationfuneral.com/ecommerce/admin/order_archive/php/overview.php?id='.$_SESSION['order_archive_id']), $msg);
                  //$rt = send_sms($smscfg['from'], $to, $msg, 'eArrangement', $client['id']);
                  $_SESSION['smsmassage'][] = $rt;
                  } */

                # for client -----------------------------------
                $from = $email_from;
                if($user_session['pkgtype'] == 'atneed'){
                    $subjectTitle = 'At-need';
                }
                else if($user_session['pkgtype'] == "preneed" and $user_session['isbam'] == 1){
                    $subjectTitle = 'Becoming a Member';
                }
                else{
                    $subjectTitle = 'Pre-need';
                }


                $subject = $this->settings['client_company_name_short'] . " - " . $subjectTitle . " Order";
                $message = $this->template('assets/templates/' . @$user_session['pkgtype'] . '.tmpl.html', $user_session);
                if (count($email_to)) {
                    foreach ($email_to as $v) {
                        $to = $v;
                        if(is_debugger($informant_email)){
                            $to = $informant_email;
                        }
                        send_mail($from, $to, $subject, $message);
                    }
                }

                # for customer -----------------------------------
                $to = $informant_email;
                $subject = "Thank you for " . $subjectTitle . " of ".$this->settings['client_company_name_short'];
                send_mail($from, $to, $subject, $message);

                # Securing credit card fields
                $ccnum = @$user_session[$this->paymentprefix]['ccnum'];
                $this->set_user_session($this->paymentprefix . '=>ccnum', '****' . substr($ccnum, -4));
                $user_session = $this->set_user_session($this->paymentprefix . '=>cccvv', '***');

                # update complete order to order archive table
                $this->order_archive("Complete");
                //$this->order_archive();
                //if( !is_debugger($informant_email) ){
                    $this->go_to('thankyou');
                //}

            } else {
                # for CC duplicate, declined, error, etc.
                $this->go_to('payment/error');
            }
        } else {
            # Write log file
            $logdata = array(
                'order_archive_id' => $user_session['order_archive_id'],
                'card_number' => $user_session[$this->paymentprefix]['ccnum'],
                'description' => 'Customer tried to pay again with same arrangement.',
            );
            $this->saveTransactionLog($logdata, 'assets/logs/mistake.log');
            //if( !is_debugger($informant_email) ){
                $this->go_to('thankyou');
            //}
        }
    }

    /*     * **************************************************************************************************
     * CONFIRMATION
     * **************************************************************************************************
     */

    public function thankyou() {
        # Check session
        $this->check_session();
        # Check complete order
        if (!$this->order_completed()) {
            show_404() ;
        }
        # Assign page variable
        $vars = array(
            'stepkey' => 'thankyou',
            'page_title' => 'Thank you',
        );
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        # Load the step view.
        $this->load_step('thankyou', $page_title, array('not_calculate_amount_due' => true,));
    }

    /*     * **************************************************************************************************
     * SOGS
     * **************************************************************************************************
     */

    public function sogs() {
        # In case it was viewed by admin
        # Load session from database
         parse_str($_SERVER['QUERY_STRING'], $params);
    
        if (CommonFN::get($params['hash']) != "") {

            $order = $this->model->get_order_by_hash($params['hash']);
  
            $session = $this->decode_session($order['session']);
            $this->set_session('user', $session);
        }

        # Drump session
        $user_session = $this->get_user_session();

        //        if (!$this->get_user_session('purchasedateint')) {
        //            $this->set_user_session('purchasedateint', time());
        //        }

        if ($user_session['purchasedateint'] > strtotime('2014-08-18 18:00:00')) {
            $this->sogs_v2();
        } else {
            $this->sogs_v1();
        }
    }

    public function sogs_v1() {
        # Drump session
        $user_session = $this->get_user_session();

        # Service Items
        $totalsprice = 0;
        $sogs_services = array();
        $sogs_services[] = array(
            'name' => 'Basic services of Funeral Director and Staff',
            'price' => '',
            'includein' => array(1, 2, 3, 4),
        );
        $sogs_services[] = array(
            'name' => 'Embalming Care',
            'price' => '',
            'includein' => array(1, 2, 3, 4),
        );
        $sogs_services[] = array(
            'name' => 'Dressing/Casketing/Cosmetology',
            'price' => '',
            'includein' => array(1, 2, 3, 4),
        );
        $sogs_services[] = array(
            'name' => 'Refrigeration',
            'price' => '',
            'includein' => array(1, 2, 3, 4),
        );
        $sprice = '';
        if (@$user_session['serviceselected']['2']['id']) {
            $sprice = number_format($user_session['serviceselected']['2']['price'], 2);
            $totalsprice += $user_session['serviceselected']['2']['price'];
        }
        $sogs_services[] = array(
            'name' => 'Facilities and Equipment',
            'price' => $sprice,
            'includein' => array(1, 2, 3, 4),
        );
        $sogs_services[] = array(
            'name' => 'Transportation',
            'price' => '',
            'includein' => array(1, 2, 3, 4),
        );
        $sogs_services[] = array(
            'name' => 'Cremation Care',
            'price' => '',
            'includein' => array(1, 2, 3, 4),
        );
        $sprice = '';
        if (@$user_session['serviceselected']['1']['id']) {
            $sprice = number_format($user_session['serviceselected']['1']['price'], 2);
            $totalsprice += $user_session['serviceselected']['1']['price'];
        }
        $sogs_services[] = array(
            'name' => 'Witnessing Cremation',
            'price' => $sprice,
            //'includein' => array(1,2,3,4),
            'includein' => array(),
        );
        $sname = 'Viewing/Visitation ( ) Hours';
        if ($user_session['packageselected']['id'] == 1) {
            $sname = 'Viewing/Visitation ( 2 ) Hours';
        } elseif ($user_session['packageselected']['id'] == 2) {
            $sname = 'Viewing/Visitation ( 1 ) Hours';
        }
        $sogs_services[] = array(
            'name' => $sname,
            'price' => '',
            'includein' => array(1, 2),
        );
        $sogs_services[] = array(
            'name' => 'Funeral Ceremony',
            'price' => '',
            'includein' => array(1),
        );
        $sogs_services[] = array(
            'name' => 'Memorial Services',
            'price' => '',
            'includein' => array(2),
        );
        $sogs_services[] = array(
            'name' => 'Graveside Service',
            'price' => '',
            'includein' => array(1),
        );
        $sogs_services[] = array(
            'name' => 'Transfer of Remains',
            'price' => '',
            'includein' => array(1, 2, 3, 4),
        );
        $sogs_services[] = array(
            'name' => 'Funeral Coach',
            'price' => '',
            'includein' => array(1),
        );
        $sogs_services[] = array(
            'name' => 'Forwarding of Remains',
            'price' => '',
            'includein' => array(),
        );
        $sogs_services[] = array(
            'name' => 'Receiving of Remains',
            'price' => '',
            'includein' => array(),
        );
        if (is_array(@$user_session['serviceselected'])) {
            foreach ($user_session['serviceselected'] as $sid => $s) {
                if ($sid < 3)
                    continue;
                $sogs_services[] = array(
                    'name' => $s['name_short'],
                    'price' => number_format($s['price'], 2),
                    'includein' => array(),
                );
                $totalsprice += $s['price'];
            }
        }
        if (is_array(@$user_session['obitonweb'])) {
            $sogs_services[] = array(
                'name' => $user_session['obitonweb']['name'],
                'price' => number_format($user_session['obitonweb']['price'], 2),
                'includein' => array(),
            );
            $totalsprice += $user_session['obitonweb']['price'];
        }
        if (@$user_session['payfor'] == 'full' and @ $user_session['pkgtype'] == 'preneed') {
            $totalsprice += $user_session['summary']['total_member'];
        }

        # Merchandise Items
        $totalmprice = 0;
        $sogs_merchs = array();
        $items = '';
        if (is_array(@$user_session['cart']['urn'])) {
            foreach ($user_session['cart']['urn'] as $pid => $item) {

                # Price calculate
                $price = ($item['price'] * ($item['quan'] - $item['included']));
                $totalmprice += $price;

                # Price formating
                $pricef = number_format($price, 2);
                if ($item['included'] and $price <= 0) {
                    $pricef = 'Included';
                }

                $items[] = array(
                    'name' => $item['quan'] . 'x ' . $item['name'],
                    'price' => $pricef,
                );
            }
        }
        $sogs_merchs[] = array(
            'name' => 'Urn',
            'includein' => array(1, 2, 3, 4),
            'items' => $items,
        );
        $items = '';
        if (is_array(@$user_session['cart']['keepsake'])) {
            foreach ($user_session['cart']['keepsake'] as $pid => $item) {

                # Price calculate
                $price = ($item['price'] * ($item['quan'] - $item['included']));
                $totalmprice += $price;

                # Price formating
                $pricef = number_format($price, 2);
                if ($item['included'] and $price <= 0) {
                    $pricef = 'Included';
                }

                $items[] = array(
                    'name' => $item['quan'] . 'x ' . $item['name'],
                    'price' => $pricef,
                );
            }
        }
        $sogs_merchs[] = array(
            'name' => 'Keepsake/Jewelry',
            'includein' => array(),
            'items' => $items,
        );
        $sogs_merchs[] = array(
            'name' => 'Limousine',
            'includein' => array(),
        );
        $items = '';
        if (is_array(@$user_session['cart']['memorial'])) {
            foreach ($user_session['cart']['memorial'] as $pid => $item) {

                # Price calculate
                $price = ($item['price'] * ($item['quan'] - $item['included']));
                $totalmprice += $price;

                # Price formating
                $pricef = number_format($price, 2);
                if ($item['included'] and $price <= 0) {
                    $pricef = 'Included';
                }

                $items[] = array(
                    'name' => $item['quan'] . 'x ' . $item['name'],
                    'price' => $pricef,
                );
            }
        }
        $sogs_merchs[] = array(
            'name' => 'Prayer Cards/Memorial Folders',
            'includein' => array(1, 2, 3),
            'items' => $items,
        );
        $sogs_merchs[] = array(
            'name' => 'Register Book',
            'includein' => array(1, 2, 3),
        );
        $sogs_merchs[] = array(
            'name' => 'Service Programs',
            'includein' => array(),
        );
        $sogs_merchs[] = array(
            'name' => 'Hairdresser',
            'includein' => array(),
        );
        $sogs_merchs[] = array(
            'name' => 'Video Tribute',
            'includein' => array(1),
        );
        $items = '';
        $flower_credit = @$user_session['packageselected']['flower_credit'];
        if (is_array(@$user_session['cart']['flower'])) {
            foreach ($user_session['cart']['flower'] as $pid => $item) {

                # Price calculate
               // $price = ($item['price'] * ($item['quan'] - $item['included']));
                $price = $item['price'] * $item['quan'];
                if ($price > $flower_credit) {
                    $price -= $flower_credit;
                    $flower_credit = 0;
                } else {
                    $flower_credit -= $price;
                    $price = 0;
                }
                $totalmprice += $price;

                # Price formating
                $pricef = number_format($price, 2);
                if ($item['included'] and $price <= 0) {
                    $pricef = 'Included';
                } elseif ($item['type'] == 'Flower' and $price <= 0) {
                    $pricef = 'Credited';
                }

                $items[] = array(
                    'name' => $item['quan'] . 'x ' . $item['name'],
                    'price' => $pricef,
                );
            }
        }
        $sogs_merchs[] = array(
            'name' => 'Florist',
            'includein' => array(1, 2),
            'items' => $items,
        );
        $sogs_merchs[] = array(
            'name' => 'Cemetery/Inscriptions',
            'includein' => array(),
        );
        $sogs_merchs[] = array(
            'name' => 'Shipping Container/Air tray',
            'includein' => array(),
        );
        if (is_array(@$user_session['merch_providing_urn'])) {
            $sogs_merchs[] = array(
                'name' => $user_session['merch_providing_urn']['name'],
                'price' => number_format($user_session['merch_providing_urn']['price'], 2),
                'includein' => array(),
            );
        }

        # Generate variable for SOGS.
        $this->load->vars(array(
            'pkg' => $user_session['packageselected']['id'],
            'deceased_name' => $user_session[$this->vitalprefix]['dc_first_name'] . ' ' . $user_session[$this->vitalprefix]['dc_middle_name'] . ' ' . $user_session[$this->vitalprefix]['dc_last_name'],
            'totalsprice' => $totalsprice,
            'sogs_services' => $sogs_services,
            'totalmprice' => $totalmprice,
            'sogs_merchs' => $sogs_merchs,
            'vitalprefix' => $this->vitalprefix,
            'insurance1prefix' => $this->insurance1prefix,
            'insurance2prefix' => $this->insurance2prefix,
            'authprefix' => $this->authprefix,
            'paymentprefix' => $this->paymentprefix,
            'user_session' => $this->get_user_session(),
            'root' => $this->cfg['root'],
        ));
        # Load the view.
        $this->load->view('sogs_v1');
    }

    public function sogs_v2() {
        # Drump session
        $user_session = $this->get_user_session();

        # Service Items
        $totalsprice = 0;
        $sogs_services = array();
        $sogs_services[] = array(
            'name' => 'Basic services of Funeral Director and Staff',
            //'price' => '2,145.00', https://funeralnet.teamwork.com/tasks/5283051
            //'price' => '1,895.00', https://funeralnet.teamwork.com/tasks/8730092
            'price' => '2,145.00',
            'includein' => array(2, 3, 4),
        );
        $totalsprice += (float)str_replace("," , "" , $sogs_services[0]['price']);
        // if($user_session['pkgtype'] == "atneed"){
        //     //$sogs_services[0]['price'] = '2,145.00'; https://funeralnet.teamwork.com/tasks/8730092
        //    // $totalsprice += (float)str_replace("," , "" , $sogs_services[0]['price']);
        //     //$sogs_services[0]['price'] = number_forma;

        // } else{
        //     $totalsprice += 1895 + 250;
        // }
        //$totalsprice+=2145;
        //
        $sogs_services[] = array(
            'name' => 'Embalming Care',
            'price' => '',
            'includein' => array(),
        );
        $sogs_services[] = array(
            'name' => 'Dressing/Casketing/Cosmetology',
            'price' => '',
            'includein' => array(),
        );
        $sogs_services[] = array(
            'name' => 'Refrigeration',
            'price' => '100.00',
            'includein' => array(2, 3, 4),
        );
        $totalsprice+=100;
        $sprice = '';
        if (@$user_session['serviceselected']['2']['id']) {
            //$sprice = number_format($user_session['serviceselected']['2']['price'], 2);
            //$totalsprice += $user_session['serviceselected']['2']['price'];
        }
        $sogs_services[] = array(
            'name' => 'Facilities and Equipment',
            'price' => $sprice,
            'includein' => array(),
        );
        $sogs_services[] = array(
            'name' => 'Transportation',
            'price' => '',
            'includein' => array(),
        );
        $sogs_services[] = array(
            'name' => 'Cremation Fee',
            'price' => '395.00',
            'includein' => array(2, 3, 4),
        );
        $totalsprice+=395;
        $sprice = '';
        if (@$user_session['serviceselected']['1']['id']) {
            //$sprice = number_format($user_session['serviceselected']['1']['price'], 2);
            //$totalsprice += $user_session['serviceselected']['1']['price'];
        }
        $sogs_services[] = array(
            'name' => 'Witnessing Cremation',
            'price' => $sprice,
            'includein' => array(),
        );
        $sogs_services[] = array(
            'name' => 'Viewing/Visitation ( 1 ) Hours',
            'price' => '',
            'includein' => array(),
        );
        $sogs_services[] = array(
            'name' => 'Funeral Ceremony',
            'price' => '',
            'includein' => array(),
        );
        $sogs_services[] = array(
            'name' => 'Memorial Services',
            'price' => '',
            'includein' => array(),
        );
        $sogs_services[] = array(
            'name' => 'Graveside Service',
            'price' => '',
            'includein' => array(),
        );
        $sogs_services[] = array(
            'name' => 'Transfer of Remains',
            'price' => '395.00',
            'includein' => array(2, 3, 4),
        );
        $totalsprice+=395;
        $sogs_services[] = array(
            'name' => 'Funeral Coach',
            'price' => '',
            'includein' => array(),
        );
        $sogs_services[] = array(
            'name' => 'Forwarding of Remains',
            'price' => '',
            'includein' => array(),
        );
        $sogs_services[] = array(
            'name' => 'Receiving of Remains',
            'price' => '',
            'includein' => array(),
        );
        if (is_array(@$user_session['serviceselected']) and @ $user_session['serviceselected'][6]) {
            $sogs_services[] = array(
                'name' => 'Obituary on Website',
                'price' => number_format(@$user_session['serviceselected'][6]['price'], 2),
                'includein' => array(2, 3),
            );
            $totalsprice+=@$user_session['serviceselected'][6]['price'];
        } else {
            $sogs_services[] = array(
                'name' => 'Obituary on Website',
                'price' => @$user_session['packageselected']['id'] == 4 ? '' : '20.00',
                'includein' => array(2, 3),
            );
            if (@$user_session['packageselected']['id'] != 4) {
                $totalsprice+=20;
            }
        }
        if (is_array(@$user_session['serviceselected'])) {
            foreach ($user_session['serviceselected'] as $sid => $s) {
                if ($sid < 3)
                    continue;
                if ($sid == 6)
                    continue;
                $sogs_services[] = array(
                    'name' => $s['name_short'],
                    'price' => number_format($s['price'], 2),
                    'includein' => array(),
                );
                $totalsprice += $s['price'];
            }
        }
        //        if (is_array(@$user_session['obitonweb'])) {
        //            $sogs_services[] = array(
        //                'name' => $user_session['obitonweb']['name'],
        //                'price' => number_format($user_session['obitonweb']['price'], 2),
        //                'includein' => array(),
        //            );
        //            $totalsprice += $user_session['obitonweb']['price'];
        //        }
        if (@$user_session['pkgtype'] == 'preneed') {
            $sogs_services[] = array(
                'name' => 'Membership Fee',
                'price' => number_format($user_session['summary']['total_member'], 2),
                'includein' => array(),
            );
            $totalsprice += $user_session['summary']['total_member'];
        } else {
            $sogs_services[] = array(
                'name' => 'Membership Fee',
                'price' => '',
                'includein' => array(),
            );
        }

        # Merchandise Items
        $totalmprice = 0;
        $sogs_merchs = array();

        # Urn items
        $items = '';
        if (@$user_session['packageselected']['id'] == 4) {
            $items[] = array(
                'name' => '1x Basic Urn (Black Acrylic)',
                'price' => '50.00',
            );
            $totalmprice += 50;
        }
        if (is_array(@$user_session['cart']['urn'])) {
            foreach ($user_session['cart']['urn'] as $pid => $item) {
                # This performing is not for scattering urn
                if ($item['subtype'] == 'Scattering') {
                    continue;
                }

                # Price calculate
                //$price = ($item['price'] * ($item['quan'] - $item['included']));
                $price = ($item['price'] * $item['quan']);
                if ($item['included'] and $price <= 0 and @ $user_session['packageselected']['id'] <= 3) {
                   //$price = 225; # Included price of Gold and Silver pacakge.
                }
                $totalmprice += $price;

                # Price formating
                $pricef = number_format($price, 2);
                if ($item['included'] and $price <= 0) {
                    //$pricef = 'Included';
                }

                $items[] = array(
                    'name' => $item['quan'] . 'x ' . $item['name'],
                    'price' => $pricef,
                );
            }
        }
        $sogs_merchs[] = array(
            'name' => 'Urn',
            'includein' => array(),
            'items' => $items,
        );

        # Scattering urn items
        $items = '';
        if (is_array(@$user_session['cart']['urn'])) {
            foreach ($user_session['cart']['urn'] as $pid => $item) {
                # This performing is just for scattering urn
                if ($item['subtype'] != 'Scattering') {
                    continue;
                }

                # Price calculate
                //$price = ($item['price'] * ($item['quan'] - $item['included']));
                $price = ($item['price'] * $item['quan']);
                $totalmprice += $price;

                # Price formating
                $pricef = number_format($price, 2);
                if ($item['included'] and $price <= 0) {
                    $pricef = 'Included';
                }

                $items[] = array(
                    'name' => $item['quan'] . 'x ' . $item['name'],
                    'price' => $pricef,
                );
            }
        }
        $sogs_merchs[] = array(
            'name' => 'Scattering Urn',
            'includein' => array(),
            'items' => $items,
        );

        # Keepsake items
        $items = '';
        if (is_array(@$user_session['cart']['keepsake'])) {
            foreach ($user_session['cart']['keepsake'] as $pid => $item) {

                # Price calculate
                //$price = ($item['price'] * ($item['quan'] - $item['included']));
                $price = $item['price'] * $item['quan'];
                if ($item['included'] and $price <= 0 and @ $user_session['packageselected']['id'] <= 3) {
                   // $price = 75; # Included price of Gold and Silver pacakge.
                }
                $totalmprice += $price;

                # Price formating
                $pricef = number_format($price, 2);
                if ($item['included'] and $price <= 0) {
                    //$pricef = 'Included';
                }

                $items[] = array(
                    'name' => $item['quan'] . 'x ' . $item['name'],
                    'price' => $pricef,
                );
            }
        }
        if (is_array(@$user_session['cart']['jewelry'])) {
            foreach ($user_session['cart']['jewelry'] as $pid => $item) {

                # Price calculate
                //$price = ($item['price'] * ($item['quan'] - $item['included']));
                $price = $item['price'] * $item['quan'];
                $totalmprice += $price;

                # Price formating
                $pricef = number_format($price, 2);
                if ($item['included'] and $price <= 0) {
                    $pricef = 'Included';
                }

                $items[] = array(
                    'name' => $item['quan'] . 'x ' . $item['name'],
                    'price' => $pricef,
                );
            }
        }
        $sogs_merchs[] = array(
            'name' => 'Keepsake/Jewelry',
            'includein' => array(),
            'items' => $items,
        );
        $sogs_merchs[] = array(
            'name' => 'Alternative Container',
            'price' => '100.00',
            'includein' => array(2, 3, 4),
        );
        $totalmprice += 100;

        # Memorial items
        $items = '';
        if (is_array(@$user_session['cart']['memorial'])) {
            foreach ($user_session['cart']['memorial'] as $pid => $item) {

                # Price calculate
               // $price = ($item['price'] * ($item['quan'] - $item['included']));
                $price = $item['price'] * $item['quan'];
                if ($item['included'] and $price <= 0 and @ $user_session['packageselected']['id'] <= 3) {
                    //$price = 250; # Included price of Gold and Silver pacakge.
                }
                $totalmprice += $price;

                # Price formating
                $pricef = number_format($price, 2);
                if ($item['included'] and $price <= 0) {
                    //$pricef = 'Included';
                }

                $items[] = array(
                    'name' => $item['quan'] . 'x ' . $item['name'],
                    'price' => $pricef,
                );
            }
        }
        $sogs_merchs[] = array(
            'name' => 'Memorial Package',
            'includein' => array(),
            'items' => $items,
        );
        $sogs_merchs[] = array(
            'name' => 'Hairdresser',
            'includein' => array(),
        );
        $sogs_merchs[] = array(
            'name' => 'Video Tribute',
            'includein' => array(1),
        );

        # Flower items
        $items = '';
        $flower_credit = @$user_session['packageselected']['flower_credit'];
        if (is_array(@$user_session['cart']['flower'])) {
            foreach ($user_session['cart']['flower'] as $pid => $item) {

                # Price calculate
                /* $price = ($item['price'] * ($item['quan'] - $item['included']));
                  if ($price > $flower_credit) {
                  $price -= $flower_credit;
                  $flower_credit = 0;
                  } else {
                  $flower_credit -= $price;
                  $price = 0;
                  }
                  $totalmprice += $price;

                  # Price formating
                  $pricef = number_format($price, 2);
                  if ($item['included'] and $price <= 0) {
                  $pricef = 'Included';
                  } elseif ($item['type'] == 'Flower' and $price <= 0) {
                  $pricef = 'Credited';
                  } */

                # Price calculate
               // $price = ($item['price'] * ($item['quan'] - $item['included']));
                $price = $item['price'] * $item['quan'];
                if ($item['included'] and $price <= 0 and @ $user_session['packageselected']['id'] <= 2) {
                    //$price = 65; # Included price of Gold and Silver pacakge.
                }
                $totalmprice += $price;

                # Price formating
                $pricef = number_format($price, 2);
                if ($item['included'] and $price <= 0) {
                    //$pricef = 'Included';
                }

                $items[] = array(
                    'name' => $item['quan'] . 'x ' . $item['name'],
                    'price' => $pricef,
                );
            }
        }
        $sogs_merchs[] = array(
            'name' => 'Florist',
            'includein' => array(1, 2),
            'items' => $items,
        );
        $sogs_merchs[] = array(
            'name' => 'Cemetery/Inscriptions',
            'includein' => array(),
        );
        $sogs_merchs[] = array(
            'name' => 'Shipping Container/Air tray',
            'includein' => array(),
        );
        if (is_array(@$user_session['merch_providing_urn'])) {
            $sogs_merchs[] = array(
                'name' => $user_session['merch_providing_urn']['name'],
                'price' => number_format($user_session['merch_providing_urn']['price'], 2),
                'includein' => array(),
            );
        }

        # Cash Advance Items
        $totalaprice = 0;
        $sogs_advances = array();
        $sogs_advances[] = array(
            'name' => 'Cemetery Opening & Closing',
            'price' => '',
            'includein' => array(),
        );
        $sogs_advances[] = array(
            'name' => 'Cemetery Overtime',
            'price' => '',
            'includein' => array(),
        );
        $sogs_advances[] = array(
            'name' => 'Newspaper Funeral Notice',
            'price' => '',
            'includein' => array(),
        );
        $sogs_advances[] = array(
            'name' => 'Other Funeral Notice',
            'price' => '',
            'includein' => array(),
        );
        $sogs_advances[] = array(
            'name' => 'Church Honorarium',
            'price' => '',
            'includein' => array(),
        );
        $sogs_advances[] = array(
            'name' => 'Clergy Honorarium',
            'price' => '',
            'includein' => array(),
        );
        $sogs_advances[] = array(
            'name' => 'Organist',
            'price' => '',
            'includein' => array(),
        );
        $sogs_advances[] = array(
            'name' => 'Soloist',
            'price' => '',
            'includein' => array(),
        );
        if (@ $user_session['packageselected']['id'] == 2) {
            $sogs_advances[] = array(
                'name' => 'Death Certificate Copies(2)',
               'price' => '12.00',
                //'price' => '',
                'includein' => array(2, 3, 4),
            );
            $totalaprice+=12;
            // Added included cert for Bronze pkg : https://funeralnet.teamwork.com/tasks/2439092
        } elseif (@ $user_session['packageselected']['id'] == 3
               /* or ( $user_session['purchasedateint'] > strtotime('2014-10-17 00:00:00') and @ $user_session['packageselected']['id'] == 4)*/) {
            $sogs_advances[] = array(
                'name' => 'Death Certificate Copies(1)',
        //      'price' => '',
                'price' => '6.00',
                'includein' => array(2, 3, 4),
            );
            $totalaprice+=6;
        } else {
            $sogs_advances[] = array(
                'name' => 'Death Certificate Copies',
                'price' => '',
                'includein' => array(),
            );
        }
        $sogs_advances[] = array(
            'name' => 'Other Funeral Home Charge',
            'price' => '',
            'includein' => array(),
        );
        $sogs_advances[] = array(
            'name' => 'Airfare',
            'price' => '',
            'includein' => array(),
        );
        $sogs_advances[] = array(
            'name' => 'Pallbearers',
            'price' => '',
            'includein' => array(),
        );
        $sogs_advances[] = array(
            'name' => 'Other Cash Advanced',
            'price' => '',
            'includein' => array(),
        );

        # Generate variable for SOGS.
        $this->load->vars(array(
            'pkg' => $user_session['packageselected']['id'],
            'deceased_name' => $user_session[$this->vitalprefix]['dc_first_name'] . ' ' . $user_session[$this->vitalprefix]['dc_middle_name'] . ' ' . $user_session[$this->vitalprefix]['dc_last_name'],
            'totalsprice' => $totalsprice,
            'sogs_services' => $sogs_services,
            'totalmprice' => $totalmprice,
            'sogs_merchs' => $sogs_merchs,
            'totalaprice' => $totalaprice,
            'sogs_advances' => $sogs_advances,
            'vitalprefix' => $this->vitalprefix,
            'insurance1prefix' => $this->insurance1prefix,
            'insurance2prefix' => $this->insurance2prefix,
            'authprefix' => $this->authprefix,
            'paymentprefix' => $this->paymentprefix,
            'user_session' => $this->get_user_session(),
            'root' => $this->cfg['root'],
        ));
        # Load the view.
        $this->load->view('sogs_v2');
    }

    /*     * **************************************************************************************************
     * PRIVATE FUNCTIONS
     * **************************************************************************************************
     */

    private function load_step($target, $page_title = '', $params = array()) {

        # Calculate summary before load the view.
        if (!@$params['not_calculate_amount_due']) {
            $this->_calculate_amount_due(@$params['include_membership']);
            $this->_notify_message_preparation();
        }

        # Generate variable from user session.
        $this->load->vars(array(
            'vitalprefix' => $this->vitalprefix,
            'insurance1prefix' => $this->insurance1prefix,
            'insurance2prefix' => $this->insurance2prefix,
            'authprefix' => $this->authprefix,
            'paymentprefix' => $this->paymentprefix,
            'user_session' => $this->get_user_session(),
            'root' => $this->cfg['root'],
            'isBam' => $this->isBam,
            'isAdmin' => $this->isAdmin,
        ));

        # Generate wrapper.
        $wrapper = $this->get_wrapper(array(
            'page_title' => $page_title,
            'print' => @$params['print'] ? 1 : 0,
        ));

        # Display page step
        $this->load->view($target, array(
            'header' => $wrapper['header'],
            'footer' => $wrapper['footer'],
            'params' => $params,
        ));
    }

    private function get_wrapper($params = array()) {
        $full_domain = $this->settings['subdomain'] . '.' . $this->settings['domain'];

        if (@$params['print']) {
            $wrapper = $this->get_wrapper_cache('http://' . $full_domain . '/ea/ea_template_print', $params);
        } else {
            $wrapper = $this->get_wrapper_cache('http://' . $full_domain . '/ea/ea_template', $params);
        }
        $wrapper = explode('<!--CONTENT_PLACEHOLDER-->', $wrapper);

        if (!@$params['print']) {
            $test_badge = $this->load->view('test_badge', array(
                'pkgtypename' => $this->get_user_session('pkgtype'),
                'user_session' => $this->get_user_session(),
            ), true);
            $wrapper[1] = $test_badge . $wrapper[1];
        }

        return array(
            'header' => $wrapper[0],
            'footer' => $wrapper[1],
        );
    }

    private function get_wrapper_cache($wrapper_link, $params = array()) {
        $wrapper = '';

        // Get wrapper filename
        $filename = array_reverse(explode('/', $wrapper_link));
        $filename = $filename[0];

        $cache_filename = 'application/cache/' . $filename . '.html';
        $cache_time = 24 * 3600; // Time in seconds to keep a page cached
        // Check Cache
        $cache_created = (file_exists($cache_filename)) ? filemtime($cache_filename) : 0;
        // If cache valid, get it
        if ((time() - $cache_created) < $cache_time) {
            //readfile($cache_filename);
            $wrapper = file_get_contents($cache_filename);
        }
        // If no cache, get wrapper and cache it
        //if (empty($wrapper) or @ $this->settings['test_mode']) {
        if (empty($wrapper)) {
            $wrapper = file_get_contents($wrapper_link);

            /*
             * Modify wrapper
             */
            $wrapper = $this->modify_wrapper($wrapper, $params);

            file_put_contents($cache_filename, $wrapper);
            @chmod($cache_filename, 0775);
            @chown($cache_filename, $this->settings['client_id']);
        }
        return $wrapper;
    }

    private function modify_wrapper($wrapper, $params = array()) {

        # Modify page title
        $page_title = $this->settings['page_title'];
        $page_title = str_replace('[page_title]', $params['page_title'], $page_title);
        $wrapper = str_replace('TITLE_PLACEHOLDER', $page_title, $wrapper);

        $string_header = '';

        # Admin and Print CSS
        $css = '<style>';
        if ($this->isAdmin) {
            $css .= '.noadmin, .noAdmin { display:none !important; }';
        } else {
            $css .= '.adminonly, .adminOnly { display:none !important; }';
        }
        if ($this->isBam) {
            $css .= '.nobam, .noBam { display:none !important; }';
        } else {
            $css .= '.bamonly, .bamOnly { display:none !important; }';
        }
        $css .= '</style>';
        $string_header .= $css;

        if (@$params['print']) {
            # Add CSS files
            $string_header .= '<link rel="stylesheet" type="text/css" href="' . $this->cfg['root'] . '/assets/css/ea_print.css" />';
            # Add JS files

            $string_header .= '<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>';
            $string_header .= '<script src="https://secure.funeralnet.com/js/reconfigformat.js"></script>';
        } else {
            # Add CSS files
            $string_header .= '<link rel="stylesheet" type="text/css" href="' . $this->cfg['root'] . '/assets/css/font-awesome.css" />';
            //$string_header .=  '<link rel="stylesheet" type="text/css" href="'.$this->cfg['root'].'/assets/css/colorbox.css" />';
            $string_header .= '<link rel="stylesheet" type="text/css" href="/css/colorbox/colorbox.css" />';
            $string_header .= '<link rel="stylesheet" type="text/css" href="' . $this->cfg['root'] . '/assets/css/jquery.bxslider.css" />';
            $string_header .= '<link rel="stylesheet" type="text/css" href="' . $this->cfg['root'] . '/assets/css/validationEngine.jquery.css" />';
            $string_header .= '<link rel="stylesheet" type="text/css" href="' . $this->cfg['root'] . '/assets/css/ea.css" />';
            # Add JS files
            $string_header .= '<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>';
            $string_header .= '<script src="' . $this->cfg['root'] . '/assets/js/jquery.bxslider.min.js"></script>';
            $string_header .= '<script src="' . $this->cfg['root'] . '/assets/js/jquery.validationEngine-en.js"></script>';
            $string_header .= '<script src="' . $this->cfg['root'] . '/assets/js/jquery.validationEngine.js"></script>';
            //$string_header .=  '<script src="'.$this->cfg['root'].'/assets/js/jquery.colorbox-min.js"></script>';
            $string_header .= '<script src="/js/jquery.colorbox.js"></script>';
            $string_header .= '<script src="' . $this->cfg['root'] . '/assets/js/jquery.printThis.js"></script>';
            $string_header .= '<script src="' . $this->cfg['root'] . '/assets/js/jquery.placeholder.js"></script>';
            $string_header .= '<script src="' . $this->cfg['root'] . '/assets/js/jquery.balloon.min.js"></script>';
            $string_header .= '<script src="https://secure.funeralnet.com/js/reconfigformat.js"></script>';
            $string_header .= '<script src="' . $this->cfg['root'] . '/assets/js/script.js"></script>';
            $string_header .= '<script src="' . $this->cfg['root'] . '/assets/js/util.js"></script>';
        }

        # Add scripts
        $script = '<script>'
                . 'var root = "' . $this->cfg['root'] . '";'
                . '$("input, textarea").placeholder();'
                . '</script>';
        $string_header .= $script;

        $wrapper = str_replace('</head>', $string_header . '</head>', $wrapper);

        # Set document.domain
        //$domain = array_reverse(explode('.', $_SERVER['SERVER_NAME']));
        //$domain = implode('.', array($domain[1], $domain[0]));
        //$wrapper = str_replace('document.domain="utahcremationsociety.com";', 'document.domain="' . $domain . '";', $wrapper);

        return $wrapper;
    }

    # Summary calculation function

    private function _calculate_amount_due($include_membership = false) {
        $this->summary = array();

        /*
         * Drump session
         */
        $user_session = $this->get_user_session();

        /*
         * Package price
         */
        $this->summary['package'] = isset($user_session['packageselected']['price']) ? $user_session['packageselected']['price'] : NULL;
        // if($user_session['pkgtype'] == 'atneed' and  isset($user_session['is_member']) and $user_session['is_member'] == 1){
        //      $this->summary['package'] = @$user_session['packageselected']['member_price'];
        // }
        /*
        if (@$user_session['pkgtype'] == 'preneed' or @ $user_session['is_member']) {
            $this->summary['package'] = @$user_session['packageselected']['member_price'];
        }*/

        /*
         * Service
         */
        $services = $this->get_user_session('serviceselected');
        if (is_array($services) and count($services)) {
            foreach ($services as $i => $service) {
                @$this->summary['service'] += $service['price'];
            }
        }
        if (is_array(@$this->get_user_session('obitonweb'))) {
            @$this->summary['service'] += $this->get_user_session('obitonweb=>price');
        }

        /*
         * Merchandise
         */
        if (is_array(@$this->get_user_session('merch_providing_urn'))) {
            @$this->summary['merchandise'] += $this->get_user_session('merch_providing_urn=>price');
        }
        $flower_credit = $this->get_user_session('packageselected');
        $flower_credit = $flower_credit['flower_credit'];
        $cart = $this->get_user_session('cart');
        if (is_array($cart) and count($cart)) {
            foreach ($cart as $type => $items) {
                foreach ($items as $id => $item) {
                    $price = ($item['price'] * ($item['quan'] - $item['included']));
                    if ($item['type'] == 'Flower') {
                        if ($price > $flower_credit) {
                            $price -= $flower_credit;
                            $flower_credit = 0;
                        } else {
                            $flower_credit -= $price;
                            $price = 0;
                        }
                    }
                    @$this->summary['merchandise'] += $price;
                }
            }
        }

        # Calculate summary
        $this->summary['subtotal'] = array_sum($this->summary);
        $this->summary['taxrate'] = $this->settings['client_sales_tax'];
        $this->summary['tax'] = (@$this->summary['merchandise'] * $this->summary['taxrate'] / 100);
        $this->summary['total'] = $this->summary['subtotal'] + $this->summary['tax'];

        # Membership fee
        $this->summary['total_member'] = ( isset($user_session['pkgtype']) and $user_session['pkgtype'] == 'preneed' )
                                         ? $this->get_user_session('member_fee') : 0;

        if(@$this->get_user_session('isbam') and @$this->summary['total_member']){
            $this->summary['total'] += $this->summary['total_member'];
        }elseif ($this->get_user_session('pkgtype') == 'preneed') {
            $this->summary['total'] += $this->summary['total_member'];
        }


        $this->set_user_session('summary', $this->summary);
    }

    # Message creating function

    private function _notify_message_preparation() {
        $user_session = $this->get_user_session();

        /*         * **********************************************************************************
         * ARRANGEMENT TABLE
         * ***********************************************************************************
         */
        $this->putMesTbl('[clear]');
        $this->putAdminTbl('[clear]');
        # Package price
        $this->putMesTbl('Package Selected');
        $this->putAdminTbl('Package Selected');

        $summaryPackage = $user_session['summary']['total_member'];
        if (@$user_session['is_member']) {

            //Price of package = membership + package for the email only
            // $summaryPackage += @$user_session['packageselected']['member_price'];
            // $this->putMesTbl(@$user_session['packageselected']['name'], '$' . number_format($summaryPackage, 2));
            // $this->putAdminTbl(@$user_session['packageselected']['name'], '$' . number_format(@$user_session['packageselected']['member_price'], 2), array(
            //     'action' => 'showdetail',
            //     'detail' => @$user_session['packageselected']['description'],
            // ));
        } else {
            $summaryPackage += @$user_session['packageselected']['price'];
            $this->putMesTbl(@$user_session['packageselected']['name'], '$' . number_format($summaryPackage, 2));
            $this->putAdminTbl(@$user_session['packageselected']['name'], '$' . number_format(@$user_session['packageselected']['price'], 2), array(
                'action' => 'showdetail',
                'detail' => @$user_session['packageselected']['description'],
            ));
        }
        if (@$user_session['payfor'] == 'full' and @ $user_session['pkgtype'] == 'preneed') {
           //$this->putMesTbl('Membership Fee', '$' . number_format($user_session['summary']['total_member'], 2));
           $this->putAdminTbl('Membership Fee', '$' . number_format($user_session['summary']['total_member'], 2));
        }

        # Service
        if (is_array(@$user_session['serviceselected'])) {
            $this->putMesTbl();
            $this->putMesTbl('Additional Options Selected');
            $this->putAdminTbl();
            $this->putAdminTbl('Additional Options Selected');
            foreach ($user_session['serviceselected'] as $s) {
                $this->putMesTbl($s['name_short'], '$' . number_format($s['price'], 2));
                if (@$s['description2']) {
                    $this->putAdminTbl($s['name_short'], '$' . number_format($s['price'], 2), array(
                        'action' => 'showdetail',
                        'detail' => $s['description2'],
                    ));
                } else {
                    $this->putAdminTbl($s['name_short'], '$' . number_format($s['price'], 2));
                }
            }
        }
        if (is_array(@$this->get_user_session('obitonweb'))) {
            $this->putMesTbl($this->get_user_session('obitonweb=>name'), '$' . number_format($this->get_user_session('obitonweb=>price'), 2));
            $this->putAdminTbl($this->get_user_session('obitonweb=>name'), '$' . number_format($this->get_user_session('obitonweb=>price'), 2));
        }

        # Merchandises
        $this->putMesTbl();
        $this->putMesTbl('Merchandise Selected');
        $this->putAdminTbl();
        $this->putAdminTbl('Merchandise Selected');
        if (is_array(@$this->get_user_session('merch_providing_urn'))) {
            $this->putMesTbl($this->get_user_session('merch_providing_urn=>name'), '$' . number_format($this->get_user_session('merch_providing_urn=>price'), 2));
            $this->putAdminTbl($this->get_user_session('merch_providing_urn=>name'), '$' . number_format($this->get_user_session('merch_providing_urn=>price'), 2));
        }

        $flower_credit = @$user_session['packageselected']['flower_credit'];
        $cart = @$user_session['cart'];
        if (is_array($cart) and count($cart)) {
            foreach ($cart as $type => $items) {
                foreach ($items as $id => $item) {
                    # Price calculate
                    $price = ($item['price'] * ($item['quan'] - $item['included']));
                    if ($item['type'] == 'Flower') {
                        if ($price > $flower_credit) {
                            $price -= $flower_credit;
                            $flower_credit = 0;
                        } else {
                            $flower_credit -= $price;
                            $price = 0;
                        }
                    }

                    # Price formating
                    $pricef = '$' . number_format($price, 2);
                    if ($item['included'] and $price <= 0) {
                        $pricef = 'Included';
                    } elseif ($item['type'] == 'Flower' and $price <= 0) {
                        $pricef = 'Not Charged';
                    }

                    $this->putMesTbl($item['quan'] . 'x ' . $item['name'], $pricef);
                    $full = 'https://fnetclients.s3.amazonaws.com/mastercart/clients/' . $this->settings['client_id'] . '/pictures/' . $item['type'] . '/' . $id . '_image.jpg';
                    $thumb = 'https://fnetclients.s3.amazonaws.com/mastercart/clients/' . $this->settings['client_id'] . '/pictures/' . $item['type'] . '/' . $id . '_thumbnail.jpg';
                    $this->putAdminTbl($item['quan'] . 'x ' . $item['name'], $pricef, array(
                        'action' => 'showlargeimage',
                        'url' => $full,
                        'thumb' => $thumb,
                    ));
                }
            }
        }
        if (is_array(@$user_session['merch_more_item_looking'])) {
            foreach ($user_session['merch_more_item_looking'] as $item) {
                $this->putMesTbl($item, '$TBD');
                $this->putAdminTbl($item, '$TBD');
            }
        }

        # Calculate summary
        $this->putMesTbl();
        $this->putMesTbl('Summary');
        //$this->putMesTbl('Subtotal','$'.number_format(@$user_session['summary']['subtotal'],2));
        //$this->putMesTbl('Sales Tax ('.@$user_session['summary']['taxrate'].'% on Merchandise)','$'.number_format(@$user_session['summary']['tax'],2));
        $this->putMesTbl('Total', '$' . number_format(@$user_session['summary']['total'], 2));
        $this->putAdminTbl();
        $this->putAdminTbl('Summary');
        //$this->putAdminTbl('Subtotal','$'.number_format(@$user_session['summary']['subtotal'],2));
        //$this->putAdminTbl('Sales Tax ('.@$user_session['summary']['taxrate'].'% on Merchandise)','$'.number_format(@$user_session['summary']['tax'],2));
        $this->putAdminTbl('Total', '$' . number_format(@$user_session['summary']['total'], 2));

        $this->set_user_session('arrangementtable', $this->putMesTbl());

        /*         * **********************************************************************************
         * DECEDENT TABLE
         * ***********************************************************************************
         */
        $this->putMesTbl('[clear]');
        # Legal name
        $legal_name = array();
        @$user_session[$this->vitalprefix]['dc_first_name'] and $legal_name[] = $user_session[$this->vitalprefix]['dc_first_name'];
        @$user_session[$this->vitalprefix]['dc_middle_name'] and $legal_name[] = $user_session[$this->vitalprefix]['dc_middle_name'];
        @$user_session[$this->vitalprefix]['dc_last_name'] and $legal_name[] = $user_session[$this->vitalprefix]['dc_last_name'];
        $legal_name = implode(' ', $legal_name);
        $this->putMesTbl('Name', $legal_name);

        if ( isset($user_session['pkgtype']) and $user_session['pkgtype'] == 'preneed')
        {
            $this->putMesTbl('Telephone Number' , @$user_session[$this->vitalprefix]['dc_phone'] );
            $this->putMesTbl('Email' , @$user_session[$this->vitalprefix]['dc_email'] );
        }


        # Sex
        $this->putMesTbl('Sex', @$user_session[$this->vitalprefix]['dc_sex']);
        # Date of birth
        $dob = @$user_session[$this->vitalprefix]['dc_dob_month'] . ' ' .
                @$user_session[$this->vitalprefix]['dc_dob_day'] . ', ' .
                @$user_session[$this->vitalprefix]['dc_dob_year'];
        $this->putMesTbl('Date of birth', $dob);
        # Date of death
        if (@$user_session['pkgtype'] == 'atneed') {
            $dod = @$user_session[$this->vitalprefix]['dc_dod_month'] . ' ' .
                    @$user_session[$this->vitalprefix]['dc_dod_day'] . ', ' .
                    @$user_session[$this->vitalprefix]['dc_dod_year'];
            $this->putMesTbl('Date of death', $dod);
        }
        # Birth place
        $this->putMesTbl('Birth place', @$user_session[$this->vitalprefix]['dc_pob_city'] . ', ' . @$user_session[$this->vitalprefix]['dc_pob_state'] . ' ' . @$user_session[$this->vitalprefix]['dc_pob_country']);
        # Marital status
        $this->putMesTbl('Marital status', @$user_session[$this->vitalprefix]['dc_marital']);

        $this->set_user_session('decedenttable', $this->putMesTbl());



        /*         * **********************************************************************************
         * CONTACT TABLE
         * ***********************************************************************************
         */
        $this->putMesTbl('[clear]');
        # Name
        $name = array();
        @$user_session[$this->vitalprefix]['pi_first_name'] and $name[] = @$user_session[$this->vitalprefix]['pi_first_name'];
        @$user_session[$this->vitalprefix]['pi_last_name'] and $name[] = @$user_session[$this->vitalprefix]['pi_last_name'];
        $this->putMesTbl('Name', implode(' ', $name));
        # Relationship
        $this->putMesTbl('Relationship', @$user_session[$this->vitalprefix]['pi_relationship']);
        # Address
        $this->putMesTbl('Address', @$user_session[$this->vitalprefix]['pi_address']);
        $this->putMesTbl('[colspan]', @$user_session[$this->vitalprefix]['pi_city'] . ', ' .
                @$user_session[$this->vitalprefix]['pi_state'] . ' ' .
                @$user_session[$this->vitalprefix]['pi_zipcode']
        );
        # Email
        $this->putMesTbl('Email', @$user_session[$this->vitalprefix]['pi_email']);
        # Phone
        $this->putMesTbl('Phone', @$user_session[$this->vitalprefix]['pi_phone']);

        $this->set_user_session('contacttable', $this->putMesTbl());
    }

    # Check if session was lost

    private function check_session() {

        if ($this->isAdmin)
            return;

        # Check session
        if (!$this->get_user_session('pkgtype')) {
            $this->go_to('atneed');
            exit;
        }
    }

    # Redirect function

    private function go_to($next_form) {
        # Build an absolute URL
        $url = $this->proper_url(array('page' => $next_form, 'secure' => $this->cfg['secure']));
        //echo $url;
        # Load the next page.
        header('Location: ' . $url);
        exit;
    }

    # Generate url

    private function proper_url($params) {
        $page = $params['page'];

        # Determine if the page should be secure or not.
        $protocol = 'http';
        if (!empty($params['secure']))
            $protocol = 'https';

        # Strip off any kind of path from the page name.
        //$page = preg_replace('/^.*?([^\/]+)$/', "\$1", $page);
        # Handle a page that specifies the document root.
        //$page = preg_replace("/^\/+$/", "", $page);
        # Determine the subdirectory we are working out of.
        /* $subdir = $_SERVER['PHP_SELF'];
          $subdir = preg_replace('/^(.+)\/[^\/]+/', '$1', $subdir);
          $subdir = explode('/', $subdir);
          foreach($subdir as $i=>$dir){
          if($dir == 'index.php'){
          unset($subdir[$i]);
          }elseif(trim($dir) == ''){
          unset($subdir[$i]);
          }
          }
          $subdir = implode('/', $subdir); */
        $subdir = $this->cfg['root'];

        # Build an absolute URL
        if (substr($page, 0, 1) == '/') {
            $url = $protocol . '://' . $_SERVER['SERVER_NAME'] . $page;
        } else {
            $url = $protocol . '://' . $_SERVER['SERVER_NAME'] . $subdir . '/' . $page;
        }

        # Return the absolute URL.
        //echo $url;
        return $url;
    }

    # return overview table for notification email.

    private function putMesTbl($label = "", $val = "[default]") {

        /* --- Usage -------------------
          putMesTbl('[clear]');                     # clear all buffer
          putMesTbl('Title');                       # title row
          putMesTbl('Label','Value');               # normal row
          putMesTbl('[colspan]','.....');           # merge row (for too long value)
          putMesTbl('[indent]','.....');            # indent row (description)
          putMesTbl('[indentitalic]','.....');  # indent row (italic description)
          putMesTbl();                          # blank row
          $_SESSION['buffer1'] = putMesTbl();       # get buffer

         */

        static $message = '';

        if (preg_match("/\[clear\]/i", $label)) {
            $message = '';
            return;
        }

        if ($label and $val != "[default]") {
            if (preg_match("/\[colspan\]/i", $label)) {
                $message.='
                <tr align=left valign=top>
                    <td colspan=2 align=right>' . $val . '</td>
                </tr>
                ';
            } elseif (preg_match("/\[indent\]/i", $label)) {
                $message.='
                <tr align=left valign=top>
                    <td colspan=2 class="indent">' . $val . '</td>
                </tr>
                ';
            } elseif (preg_match("/\[indentitalic\]/i", $label)) {
                $message.='
                <tr align=left valign=top>
                    <td colspan=2 class="indent"><i>' . $val . '</i></td>
                </tr>
                ';
            } else {
                $message.='
                <tr align=left valign=top>
                    <td>' . $label . '</td>
                    <td align=right>' . $val . '</td>
                </tr>
                ';
            }
        } elseif ($label) {
            $message.='
            <tr align=left>
                <td colspan=2><b>' . $label . '</b></td>
            </tr>
            ';
        } elseif ($val != "[default]") {
            $message.='
            <tr align=left valign=top>
                <td>&nbsp;</td>
                <td align=right>' . $val . '</td>
            </tr>
            ';
        } else {
            $message.='<tr><td>&nbsp;</td></tr>';
        }

        return '<style>
                .indent{
                    text-indent:15px;
                }
                </style>
                <table width=500>' . $message . '</table>';
    }

    private function putAdminTbl($label = "", $val = "[default]", $param = array()) {

        /* --- Usage -------------------
          putAdminTbl('[clear]');                   # clear all buffer
          putAdminTbl('Title');                     # title row
          putAdminTbl('Label','Value');             # normal row
          putAdminTbl('[colspan]','.....');         # merge row (for too long value)
          putAdminTbl('[indent]','.....');          # indent row (description)
          putAdminTbl('[indentitalic]','.....');        # indent row (italic description)
          putAdminTbl();                                # blank row
         *
          /*--- For sending more parameter -------------------------
          putAdminTbl('Title', 'Value', array(param...));           # send parameter as array(must have 'action' as one of members)

          # For action is "showdetail"
          'action' => 'showdetail',
          'detail' => $detail,

          # For action is "showlargeimage"
          'action' => 'showlargeimage',
          'url' => $large_image_url,
          'thumb' => $thumb_image_url,
         */


        $row = array();

        if (preg_match("/\[clear\]/i", $label)) {
            $this->set_user_session('admintable', array());
            return;
        }

        if ($label and $val != "[default]") {
            if (preg_match("/\[colspan\]/i", $label)) {
                $row = array(
                    'type' => 'row-colspan',
                    'value' => $val,
                );
            } elseif (preg_match("/\[indent\]/i", $label)) {
                $row = array(
                    'type' => 'row-indent',
                    'value' => $val,
                );
            } elseif (preg_match("/\[indentitalic\]/i", $label)) {
                $row = array(
                    'type' => 'row-indent-italic',
                    'value' => $val,
                );
            } else {
                $row = array(
                    'type' => 'row-normal',
                    'label' => $label,
                    'value' => $val,
                );
            }
        } elseif ($label) {
            $row = array(
                'type' => 'row-title',
                'value' => $label,
            );
        } elseif ($val != "[default]") {
            $row = array(
                'type' => 'row-value',
                'value' => $val,
            );
        } else {
            $row = array(
                'type' => 'row-empty',
            );
        }

        $row['param'] = $param;

        $this->set_user_session('admintable=>[]', $row);
    }

    /*
     * Manage session functions
     */

    private function get_session_name() {
        if ($this->cfg['session_using'] == 'ci') {
            return $this->config->item('sess_cookie_name');
        } else {
            return session_name();
        }
    }

    private function start_session() {
        if ($this->cfg['session_using'] == 'ci') {
            if ($this->isAdmin) {
                $this->config->set_item('sess_cookie_name', 'ea_ci_admin_session');
            } else if ($this->isBam) {
                //$this->config->set_item('sess_cookie_name', 'ea_ci_bam_session');
                $this->config->set_item('sess_cookie_name', 'ea_ci_session');
            } else {
                $this->config->set_item('sess_cookie_name', 'ea_ci_session');
            }
        }

        # Native session will be used always
        session_id();
        if ($this->isAdmin) {
            session_name("ea_native_admin_session");
        } else if ($this->isBam) {
            //session_name("ea_native_bam_session");
            session_name("ea_native_session");
        } else {
            session_name("ea_native_session");
        }
        session_start();
    }

    private function set_user_session($key, $val) {
        $key = explode('=>', $key);

        if ($this->cfg['session_using'] == 'ci') {
            $ses_string = '$this->session->userdata["user"]';
        } else {
            $ses_string = '$_SESSION["user"]';
        }

        foreach ($key as $k) {
            if ($k == '[]') {
                $ses_string .= '[]';
            } else {
                $ses_string .= '["' . $k . '"]';
            }
        }
        $ses_string .= ' = $val;';
        eval($ses_string);

        if ($this->cfg['session_using'] == 'ci') {
            $this->session->sess_write();
        }

        return $this->get_user_session();
    }

    public function get_user_session($key = '') {

        if ($this->cfg['session_using'] == 'ci') {
            $ses_string = '$this->session->userdata["user"]';
        } else {
            $ses_string = '$_SESSION["user"]';
        }

        if ($key != '') {
            $key = explode('=>', $key);
            foreach ($key as $k) {
                if ($k == '[]') {
                    $ses_string .= '[]';
                } else {
                    $ses_string .= '["' . $k . '"]';
                }
            }
        }

        $syntax = '$rt = @' . $ses_string . ' ? ' . $ses_string . ' : false;';
        //echo $syntax;
        eval($syntax);
        return $rt;
    }

    public function get_user_session_old($key = '') {
        if ($this->cfg['session_using'] == 'ci') {
            if (empty($key)) {
                return @$this->session->userdata['user'] ? $this->session->userdata['user'] : false;
            } else {
                return @$this->session->userdata['user'][$key] ? $this->session->userdata['user'][$key] : false;
            }
        } else {
            if (empty($key)) {
                return @$_SESSION['user'] ? $_SESSION['user'] : false;
            } else {
                return @$_SESSION['user'][$key] ? $_SESSION['user'][$key] : false;
            }
        }
    }

    private function set_session($key, $val) {
        $key = explode('=>', $key);

        if ($this->cfg['session_using'] == 'ci') {
            $ses_string = '$this->session->userdata';
        } else {
            $ses_string = '$_SESSION';
        }

        foreach ($key as $k) {
            if ($k == '[]') {
                $ses_string .= '[]';
            } else {
                $ses_string .= '["' . $k . '"]';
            }
        }
        $ses_string .= ' = $val;';
        eval($ses_string);

        if ($this->cfg['session_using'] == 'ci') {
            $this->session->sess_write();
        }

        return $this->get_session();
    }

    public function get_session($key = '') {
        if ($this->cfg['session_using'] == 'ci') {
            if (empty($key)) {
                return @$this->session->userdata ? $this->session->userdata : false;
            } else {
                return @$this->session->userdata[$key] ? $this->session->userdata[$key] : false;
            }
        } else {
            if (empty($key)) {
                return @$_SESSION ? $_SESSION : false;
            } else {
                return @$_SESSION[$key] ? $_SESSION[$key] : false;
            }
        }
    }

    private function destroy_session() {
        if ($this->cfg['session_using'] == 'ci') {
            $this->session->sess_destroy();
        } else {
            $_SESSION = array();
        }
    }

    /*
     * Order archive functions
     */

    private function order_completed() {
        if ($this->get_user_session('order_archive_id')) {
            $order = $this->model->get_order_by_id($this->get_user_session('order_archive_id'));
            //echo $order['status'] ;
            if (strtolower($order['status']) == 'complete') {
                return true;
            }
        }
        return false;
    }

    private function order_archive($status = "Abandon") {

        # If the order was completed the record shouldn't be updating anymore.
        if ($this->order_completed())
            return;

        # Purchase date
        if (!$this->get_user_session('purchasedateint')) {
            $this->set_user_session('purchasedateint', time());
        }
        $times = $this->get_user_session('purchasedateint');

        # In case of admin everything do not need to be updating.
        if ($this->isAdmin) {
            return;
        }

        # This session will be saved in archive record
        $session_save = $this->encode_session();

        # Preparing data to save to archive record
        $user_session = $this->get_user_session();

        # Legal name
        $legal_name = array();
        @$user_session[$this->vitalprefix]['dc_first_name'] and $legal_name[] = @$user_session[$this->vitalprefix]['dc_first_name'];
        @$user_session[$this->vitalprefix]['dc_middle_name'] and $legal_name[] = @$user_session[$this->vitalprefix]['dc_middle_name'];
        @$user_session[$this->vitalprefix]['dc_last_name'] and $legal_name[] = @$user_session[$this->vitalprefix]['dc_last_name'];
        $legal_name = implode(' ', $legal_name);

        # Package type
        $pkgtype = @$user_session['pkgtype'];
        if (@$user_session['isbam']) {
            $pkgtype = 'bam';
        }

        # Price
        $price = $user_session['summary']['total'];
        $paid = $user_session['summary']['total'];
        if (@$user_session['payfor'] == 'member') {
            $paid = $user_session['summary']['total_member'];
        }
        if (strtolower($status) != 'complete') {
            $paid = 0;
        }

        # Payment method
        $paymentmethod = @$user_session[$this->paymentprefix]['payment_method'] ? $user_session[$this->paymentprefix]['payment_method'] : '-';
        if ($paymentmethod != 'cc') {
            $paid = 0;
        }
        $hash = '';
        if ($this->get_user_session('hash')) {
            $hash = $this->get_user_session('hash');
        } else {
            $hash = $this->get_hash_code();
            $this->set_user_session('hash', $hash);
        }
        $data = array(
            "uniq" => trim($this->get_user_session('uniqid')),
            "firstname" => trim($legal_name),
            "pkgtype" => $pkgtype,
            "package" => $this->isBam ? 'Become a Member' : trim(@$user_session['packageselected']['name']),
            "price" => $price,
            "paid" => $paid,
            "paymentmethod" => $paymentmethod,
            "purchasedate" => date("Y-m-d H:i:s", $times),
            "session" => $session_save,
            "hash" => $hash,
            "status" => $status,
        );
        if ($this->get_user_session('order_archive_id')) {
            $this->model->update_order_archive($data, $this->get_user_session('order_archive_id'));
        } else {
            $order_archive_id = $this->model->add_order_archive($data);
            $this->set_user_session('order_archive_id', $order_archive_id);
        }
    }

    private function order_archive_standalone($status = 'Abandon') {

        # If the order was completed the record shouldn't be updating anymore.
        if ($this->order_completed())
            return false;

        # Submitted date
        if (!$this->get_user_session('purchasedateint')) {
            $this->set_user_session('purchasedateint', time());
        }
        $times = $this->get_user_session('purchasedateint');

        # In case of admin everything do not need to be updating.
        if ($this->isAdmin) {
            return false;
        }

        # This session will be saved in archive record
        $session_save = $this->encode_session();

        # Preparing data to save to archive record
        $user_session = $this->get_user_session();

        # Legal name
        $legal_name = array();
        @$user_session[$this->authstdprefix]['decedent_name'] and $legal_name[] = @$user_session[$this->authstdprefix]['decedent_name'];
        $legal_name = implode(' ', $legal_name);

        $hash = $this->get_user_session('hash');
        if (!$hash) {
            $hash = $this->get_hash_code();
            $this->set_user_session('hash', $hash);
        }

        $data = array(
            "uniq" => trim($this->get_user_session('uniqid')),
            "firstname" => trim($legal_name),
            "pkgtype" => '-',
            "package" => 'Cremation Authorization Form',
            "price" => 0,
            "paid" => 0,
            "paymentmethod" => '-',
            "purchasedate" => date("Y-m-d H:i:s", $times),
            "session" => $session_save,
            "hash" => $hash,
            "status" => $status,
        );
        if ($this->get_user_session('order_archive_id')) {
            $this->model->update_order_archive($data, $this->get_user_session('order_archive_id'));
        } else {
            $order_archive_id = $this->model->add_order_archive($data);
            $this->set_user_session('order_archive_id', $order_archive_id);
        }
        return true;
    }

    private function generate_uniq_id() {
        $table = 'order_archive';
        $digit = 6; # Number of digit for order id (Include year number for 2 first digit)

        $space = "";
        for ($i = 1; $i <= $digit; $i++) {
            $space.="0";
        }

        $uniqid = date("y") . substr($space . "1", (($digit - 2) * -1));

        $last_uniq = $this->model->get_last_uniq_id();
        if ($last_uniq) {
            $uniqid = substr($last_uniq, (($digit - 2) * -1)) + 1;
            $uniqid = date("y") . substr($space . $uniqid, (($digit - 2) * -1));
        }

        return $uniqid;
    }

    private function saveTransactionLog($arr = array(), $path = "assets/logs/transaction.log") {
        if (!count($arr))
            return false;

        if ($logf = fopen($path, 'a')) { # a/w
            $text = "=== ";
            $text .= date('Y-m-d H:i:s');
            $text .= " | ";
            $text .= "Order Archive ID : " . $this->get_user_session('order_archive_id');
            $text .= " | ";
            $text .= "Order ID : " . $this->get_user_session('uniqid');
            $text .= " =====X=X=X=X=X=X=X=X=============================\n";
            $text .= print_r($arr, true);
            $text .= "\n\n\n";

            flock($logf, LOCK_EX);
            fwrite($logf, $text);
            flock($logf, LOCK_UN);  # Release the write lock.
            fclose($logf);
        } else {
            die("Cannot open log file.");
        }
    }

    private function savePaymentLog($arr = array(), $path = "assets/logs/payment.log") {
        if (!count($arr))
            return false;

        if ($logf = fopen($path, 'a')) { # a/w
            $text = "=== ";
            $text .= date('Y-m-d H:i:s');
            $text .= " =====X=X=X=X=X=X=X=X=============================\n";
            $text .= print_r($arr, true);
            $text .= "\n\n\n";

            flock($logf, LOCK_EX);
            fwrite($logf, $text);
            flock($logf, LOCK_UN);  # Release the write lock.
            fclose($logf);
        } else {
            die("Cannot open log file.");
        }
    }

    private function template($file, $arr = array()) {
        $html = '';

        $template = fopen($file, 'r') or die("Template : Couldn't open for reading \"$file\"\n");
        while (!feof($template)) {
            $html .= fgets($template, 1024);
        }
        fclose($template);

        $html = @preg_replace('/\$(\w+)\$/e', '$arr["$1"]', $html);

        return $html;
    }

    private function encode_session() {
        $encryption_key = $this->settings['encryption_key'];
        $session_save = $this->get_user_session();
        $session_save = json_encode($session_save);
        $session_save = fn_encrypt($session_save, $encryption_key);
        return $session_save;
    }

    private function decode_session($session) {
        $encryption_key = $this->settings['encryption_key'];
        $session = fn_decrypt($session, $encryption_key);
        $session = json_decode($session, true);
        return $session;
    }

    private function get_hash_code() {
        $data = crypt(rand(100000, 999999)) .
                crypt(rand(100000, 999999)) .
                crypt(rand(100000, 999999)) .
                crypt(rand(100000, 999999));
        return base64_encode(crypt($data));
    }

    private function get_steps() {
        $steps = $this->cfg['step'];
        $no = 0;
        foreach ($steps as $i => $v) {
            $no++;
            $steps[$i]['no'] = $no;
        }
        return $steps;
    }

    private function get_steps_for_authstd() {
        $steps = $this->cfg['authstd_step'];
        $no = 0;
        foreach ($steps as $i => $v) {
            $no++;
            $steps[$i]['no'] = $no;
        }
        return $steps;
    }

}
