<?= $header ?>
<form method="post" id="mainform" name="mainform" style="margin:0;">
    <input type="hidden" name="scriptaction" 	id="scriptaction"	value="validate" />
    <input type="hidden" name="notvalidate" 	id="notvalidate"	value="" />
    <input type="hidden" name="next_form" 		id="next_form"		value="<?= $next_form ?>" />
    <input type="hidden" name="cctype" 			id="cctype"			value="" />
    <!--Data-->
    <div id="ea">
        <img src="<?= $root ?>/assets/images/ccicon-visa-gray.png" style="display:none;"/>
        <img src="<?= $root ?>/assets/images/ccicon-mastercard-gray.png" style="display:none;"/>
        <img src="<?= $root ?>/assets/images/ccicon-discover-gray.png" style="display:none;"/>
        <?php
        if (@$this->uri->segment(2) == 'error') {
            $this->load->vars(array(
                'error_message' => 'An error occurred during the transaction.<br>Please make sure you filled the correct credit card information or try another card.',
            ));
        }
        $this->load->view('message_badge');
        ?>

        <div class="ea-step-title-box">
            <h1 class="ea-step-title">step 5:  Checkout</h1>
        </div>

        <div id="content-ea" >
            <div id="ea-content-inner">
                <div class="form-text-content">
                    Please enter your credit card details below to complete the process. <br><strong>All fields are required.</strong>
                </div>
                <div class="form-text-content">
                    <i>Forms must be completed and membership fees paid and received by our office at least 24 hours prior to a members passing to receive the member pricing.</i></div>

                <div class="form-box">
                    <div class="payment-sub-title">Credit Card Billing Information</div>
                    <div class="form-field-box">
                        <div class="form-lable">Name on Card:</div>
                        <div class="form-field">
                            <?php $fieldname = "ccname"; ?>
                            <input type="text" size="30" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" placeholder="Exactly as it appears on the card" class="validate[required]" data-value="FuneralNet">
                        </div>
                    </div>
                    <div class="form-text-content-center">
                        <input type="checkbox" class="chkcopybillingaddress" /> Copy billing address from Informant's information
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Billing Address:</div>
                        <?php $fieldname = "ccaddress"; ?>
                        <div class="form-field"><input type="text" size="30" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" data-copy="<?= @$user_session[$vitalprefix]['pi_address'] ?>" class="validate[required]" ></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Address 2:</div>
                        <div class="form-field">
                            <?php $fieldname = "ccaddress2"; ?>
                            <input type="text" size="30" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>"  placeholder="For Apt or Suite (optional)">
                        </div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">City:</div>
                        <?php $fieldname = "cccity"; ?>
                        <div class="form-field"><input type="text" size="30" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" data-copy="<?= @$user_session[$vitalprefix]['pi_city'] ?>" class="validate[required]" ></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">State:</div>
                        <div class="form-field">
                            <?php $fieldname = "ccstate"; ?>
                            <select class="input-dropdown validate[required]" name="<?= $fieldname ?>" data-copy="<?= @$user_session[$vitalprefix]['pi_state'] ?>">
                                <option value="">Select State</option>
                                <?php $sel = @array($user_session[$fieldprefix][$fieldname] => 'selected'); ?>
                                <?php
                                foreach ($cfg['state_codes'] as $st) {
                                    ?><option value="<?= $st['code'] ?>" <?= @$sel[$st['code']] ?>><?= $st['name'] ?></option><?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Zip/Postal Code:</div>
                        <?php $fieldname = "cczip"; ?>
                        <div class="form-field"><input type="text" size="30" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" data-copy="<?= @$user_session[$vitalprefix]['pi_zipcode'] ?>" class="validate[required]" data-value="55555" ></div>
                    </div>

                </div>

                <div class="form-box">
                    <div class="payment-sub-title">Secure Credit Card Payment</div>
                    <div class="form-field-box">
                        <div class="form-lable">Credit Card Number:</div>
                        <div class="form-field">
                            <?php $fieldname = "ccnum"; ?>
                            <input type="text" size="30" name="<?= $fieldname ?>" placeholder="No dashes or spaces" class="validate[required,funcCall[checkCCFormat]]" data-value="4111111111111111">
                        </div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Expiration Date:</div>
                        <div class="form-field">
                            <?php $fieldname = 'ccexp' . 'month'; ?>
                            <?php $sel = @array($user_session[$fieldprefix][$fieldname] => "selected"); ?>
                            <select name="<?= $fieldname ?>" class="input-dropdown validate[required]">
                                <OPTION value="">Month</OPTION>
                                <?php
                                for ($i = 1; $i <= 12; $i++) {
                                    $month = date("F", mktime(0, 0, 0, $i, 1, 2000))
                                    ?><OPTION value="<?= substr('0' . $i, -2) ?>"><?= $month ?></OPTION><?php
                                    }
                                    ?>
                            </SELECT>
                            <?php $fieldname = 'ccexp' . 'year'; ?>
                            <?php $sel = @array($user_session[$fieldprefix][$fieldname] => "selected"); ?>
                            <SELECT name="<?= $fieldname ?>" class="input-dropdown validate[required]">
                                <OPTION value="">Year</OPTION>
                                <?php
                                for ($i = date("Y"); $i <= (date("Y") + 10); $i++) {
                                    ?><OPTION value="<?= substr($i, -2) ?>"><?= $i ?></OPTION><?php
                                    }
                                    ?>
                            </SELECT>
                        </div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Security Code:</div>
                        <div class="form-field">
                            <?php $fieldname = "cccvv"; ?>
                            <input type="text" size="12" name="<?= $fieldname ?>" placeholder="CVV" class="validate[required,funcCall[checkCCCVV]]" data-value="444"><br>
                            <span class="payment-text-small">The last 3 digits displayed on the back of the card</span>
                        </div>
                    </div>

                    <div class="creditcard-logo">
                        <img src="<?= $root ?>/assets/images/ccicon-visa.png" title="Visa" data-type="visa" data-src="<?= $root ?>/assets/images/ccicon-visa.png" data-gray="<?= $root ?>/assets/images/ccicon-visa-gray.png">
                        <img src="<?= $root ?>/assets/images/ccicon-mastercard.png" title="Mastercard" data-type="mastercard" data-src="<?= $root ?>/assets/images/ccicon-mastercard.png" data-gray="<?= $root ?>/assets/images/ccicon-mastercard-gray.png">
                        <img src="<?= $root ?>/assets/images/ccicon-discover.png" title="Discover" data-type="discover" data-src="<?= $root ?>/assets/images/ccicon-discover.png" data-gray="<?= $root ?>/assets/images/ccicon-discover-gray.png">
                    </div>
                    <div class="comodo-logo"><a href="#"></a></div>

                </div>

                <?php
                /*
                  $fieldname="payfor";
                  if(@$user_session['pkgtype'] == 'preneed'){
                  ?>
                  <?php $sel=@array($user_session[$fieldname]=>"checked");?>
                  <div class="form-box">
                  <div class="form-field-box">
                  <div class="form-field">
                  <label>
                  <input type="radio" name="<?=$fieldname?>" <?=@$sel['full']?> value="full" class="validate[required]">&nbsp;&nbsp;
                  $<?=number_format($user_session['summary']['total'],2)?> - Pay the full balance.
                  </label>
                  </div>
                  </div>
                  <div class="form-field-box">
                  <div class="form-field">
                  <label>
                  <input type="radio" name="<?=$fieldname?>" <?=@$sel['member']?> value="member" class="validate[required]">&nbsp;&nbsp;
                  $<?=number_format($settings['member_fee'],2)?> - Just pay for Membership, pay the full balance later.
                  </label>
                  </div>
                  </div>
                  </div>
                  <?php
                  }else{ ?>
                  <input type="hidden" name="<?=$fieldname?>" value="full">&nbsp;&nbsp;
                  <?php }
                 */
                ?>

            </div>

            <?php
            $this->load->view('_right_bar');
            ?>

        </div>

    </div>
    <script src="<?= $cfg['root'] ?>/assets/js/jquery.creditCardValidator.js"></script>
    <script src="<?= $cfg['root'] ?>/assets/js/form_payment.js"></script>
    <!--Data-->
</form>
<?=
$footer?>