<?= $header ?>
<!--Data-->
<div id="ea">
    <?php
    $this->load->view('message_badge');
    ?>
    <div class="ea-step-title-box">
        <h1 class="ea-step-title">Thank You</h1>
    </div>

    <?php
    $price = @$user_session['summary']['total'];
    if (@$user_session['payfor'] == 'member') {
        $price = @$user_session['summary']['total_member'];
    }
    ?>

    <div id="content-ea" >

        <link rel="stylesheet" href="/themes/tristate/normalize.css">
        <link rel="stylesheet" href="/themes/tristate/main.css">
        <link rel="stylesheet" href="/themes/tristate/typography.css">
        <link rel="stylesheet" href="<?= $cfg['root'] ?>/assets/css/ea.css" /></head>

        <div id="ea-content-inner">
            <div class="form-text-content">
                Thank you for submitting our On-line Cremation Authorization Form.<br>
<br>
                For additional information or immediate assistance, please contact us directly at: <?= @$settings['client_phone_local'] ?>.
            </div>
        </div>

        <?php
        $this->load->view('_right_bar');
        ?>

    </div>
</div>
<script src="<?= $cfg['root'] ?>/assets/js/thankyou.js"></script>
<!--Data-->
<?=
$footer?>