<?= $header ?>
<script>
    var payFor = "<?= $user_session['payfor'] ?>";
    var stepKey = "<?= $stepkey ?>";
</script>
<form method="post" id="mainform" name="mainform" style="margin:0;">
    <input type="hidden" name="scriptaction" 	id="scriptaction"	value="validate" />
    <input type="hidden" name="notvalidate" 	id="notvalidate"	value="" />
    <input type="hidden" name="next_form" 		id="next_form"		value="<?= @$next_form ?>" />
    <!--Data-->

    <input type="hidden" name="pkgtype"	 		id="pkgtype"		value="<?= @$user_session['pkgtype'] ?>" />

    <div id="ea" class="">
        <?php
        $this->load->view('message_badge');
        ?>

        <img class="img_opt_no" src="<?= $root ?>/assets/images/btn-choose-merch-no.png" style="display:none;">
        <img class="img_opt_yes" src="<?= $root ?>/assets/images/btn-choose-merch-yes.png" style="display:none;">

        <div class="ea-step-title-box">
            <h1 class="ea-step-title">step 4:  PAPERWORK</h1>
        </div>
        <div id="content-ea" >

            <div id="ea-content-inner">
                <div class="form-title">Vital Statistics</div>
                <div class="form-box">
                    <div class="form-sub-title">Vital Statistics for Death Certificate</div>
                    <div class="form-field-box">
                        <div class="form-lable"><span class="asterisk">*</span>First Name:</div>
                        <?php $fieldname = "dc_first_name"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="validate[required]" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable"><span class="asterisk">*</span>Last Name:</div>
                        <?php $fieldname = "dc_last_name"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="validate[required]" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Middle Name:</div>
                        <?php $fieldname = "dc_middle_name"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable"><span class="asterisk">*</span>Sex:</div>
                        <?php $fieldname = "dc_sex"; ?>
                        <div class="form-field">
                            <select name="<?= $fieldname ?>" class="validate[required]">
                                <?php $sel = @array($user_session[$fieldprefix][$fieldname] => 'selected'); ?>
                                <option value="">Choose</option>
                                <option value="Male" <?= @$sel['Male'] ?>>Male</option>
                                <option value="Female" <?= @$sel['Female'] ?>>Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable"><span class="asterisk">*</span>Race:</div>
                        <?php $fieldname = "dc_rec"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="validate[required]" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable"><span class="asterisk">*</span>Date of Birth:</div>
                        <div class="form-field">
                            <?php $fieldname = 'dc_dob_' . 'month'; ?>
                            <?php $sel = @array($user_session[$fieldprefix][$fieldname] => "selected"); ?>
                            <select name="<?= $fieldname ?>" id="<?= $fieldprefix ?><?= $fieldname ?>" class="validate[required]">
                                <OPTION value="">Month</OPTION>
                                <?php
                                for ($i = 1; $i <= 12; $i++) {
                                    $month = date("F", mktime(0, 0, 0, $i, 1, 2000))
                                    ?><OPTION value="<?= $month ?>" <?= @$sel[$month] ?>><?= $month ?></OPTION><?php
                                    }
                                    ?>
                            </SELECT>
                            <?php $fieldname = 'dc_dob_' . 'day'; ?>
                            <?php $sel = @array($user_session[$fieldprefix][$fieldname] => "selected"); ?>
                            <SELECT name="<?= $fieldname ?>" id="<?= $fieldprefix ?><?= $fieldname ?>" class="validate[required]">
                                <OPTION value="">Day</OPTION>
                                <?php
                                for ($i = 1; $i <= 31; $i++) {
                                    ?><OPTION value="<?= $i ?>" <?= @$sel[$i] ?>><?= $i ?></OPTION><?php
                                    }
                                    ?>
                            </SELECT>
                            <?php $fieldname = 'dc_dob_' . 'year'; ?>
                            <?php $sel = @array($user_session[$fieldprefix][$fieldname] => "selected"); ?>
                            <SELECT name="<?= $fieldname ?>" id="<?= $fieldprefix ?><?= $fieldname ?>" class="validate[required]">
                                <OPTION value="">Year</OPTION>
                                <?php
                                for ($i = date("Y"); $i >= 1900; $i--) {
                                    ?><OPTION value="<?= $i ?>" <?= @$sel[$i] ?>><?= $i ?></OPTION><?php
                                    }
                                    ?>
                            </SELECT>
                        </div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Place of Birth:</div>
                        <div class="form-field">
                            <?php $fieldname = 'dc_pob_' . 'city'; ?>
                            City:<br><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="" /><br>
                            <?php $fieldname = 'dc_pob_' . 'state'; ?>
                            State:<br><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="" /><br>
                            <?php $fieldname = 'dc_pob_' . 'country'; ?>
                            Country:<br><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="" />
                        </div>
                    </div>
                    <?php
                    if (@$user_session['pkgtype'] == 'atneed') {
                        ?>
                        <div class="form-field-box">
                            <div class="form-lable"><span class="asterisk">*</span>Date of Death:</div>
                            <div class="form-field">
                                <?php $fieldname = 'dc_dod_' . 'month'; ?>
                                <?php $sel = @array($user_session[$fieldprefix][$fieldname] => "selected"); ?>
                                <select name="<?= $fieldname ?>" id="<?= $fieldprefix ?><?= $fieldname ?>" class="validate[required]">
                                    <OPTION value="">Month</OPTION>
                                    <?php
                                    for ($i = 1; $i <= 12; $i++) {
                                        $month = date("F", mktime(0, 0, 0, $i, 1, 2000))
                                        ?><OPTION value="<?= $month ?>" <?= @$sel[$month] ?>><?= $month ?></OPTION><?php
                                        }
                                        ?>
                                </SELECT>
                                <?php $fieldname = 'dc_dod_' . 'day'; ?>
                                <?php $sel = @array($user_session[$fieldprefix][$fieldname] => "selected"); ?>
                                <SELECT name="<?= $fieldname ?>" id="<?= $fieldprefix ?><?= $fieldname ?>" class="validate[required]">
                                    <OPTION value="">Day</OPTION>
                                    <?php
                                    for ($i = 1; $i <= 31; $i++) {
                                        ?><OPTION value="<?= $i ?>" <?= @$sel[$i] ?>><?= $i ?></OPTION><?php
                                        }
                                        ?>
                                </SELECT>
                                <?php $fieldname = 'dc_dod_' . 'year'; ?>
                                <?php $sel = @array($user_session[$fieldprefix][$fieldname] => "selected"); ?>
                                <SELECT name="<?= $fieldname ?>" id="<?= $fieldprefix ?><?= $fieldname ?>" class="validate[required]">
                                    <OPTION value="">Year</OPTION>
                                    <?php
                                    for ($i = date("Y"); $i >= 1900; $i--) {
                                        ?><OPTION value="<?= $i ?>" <?= @$sel[$i] ?>><?= $i ?></OPTION><?php
                                        }
                                        ?>
                                </SELECT>
                            </div>
                        </div>
                        <div class="form-field-box">
                            <div class="form-lable"><span class="asterisk">*</span>City of Death:</div>
                            <?php $fieldname = "dc_city_of_death"; ?>
                            <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="validate[required]" /></div>
                        </div>
                        <div class="form-field-box">
                            <div class="form-lable"><span class="asterisk">*</span>State of Death:</div>
                            <?php $fieldname = "dc_state_of_death"; ?>
                            <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="validate[required]" /></div>
                        </div>



                        <div class="form-field-box">
                            <div class="form-lable">County of Death:</div>
                            <?php $fieldname = "dc_county_of_death"; ?>
                            <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="" /></div>
                        </div>
                        <div class="form-field-box">
                            <div class="form-lable"><span class="asterisk">*</span>Location of Death:</div>
                            <div class="form-field">
                                <?php $fieldname = "dc_location_of_death"; ?>
                                <select size="1" name="<?= $fieldname ?>" class="idleField validate[required]">
                                    <?php $sel = @array($user_session[$fieldprefix][$fieldname] => 'selected'); ?>
                                    <option value="None Selected" <?= @$sel['None Selected'] ?>>Home</option>
                                    <option value="In Transport" <?= @$sel['In Transport'] ?>>In Transport</option>
                                    <option value="Emergency Room" <?= @$sel['Emergency Room'] ?>>Emergency Room</option>
                                    <option value="Hospital" <?= @$sel['Hospital'] ?>>Hospital</option>
                                    <option value="Nursing Home" <?= @$sel['Nursing Home'] ?>>Nursing Home</option>
                                    <option value="Other Place" <?= @$sel['Other Place'] ?>>Other Place</option>
                                </select><br>
                                If other, please indicate address:<br>
                                <?php $fieldname = "dc_location_of_death_other1"; ?>
                                <input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" /><br>
                                <?php $fieldname = "dc_location_of_death_other2"; ?>
                                <input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" /><br>
                                <?php $fieldname = "dc_location_of_death_other3"; ?>
                                <input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" /><br>
                                <?php $fieldname = "dc_location_of_death_other4"; ?>
                                <input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" />
                            </div>
                        </div>
                        <div class="form-field-box">
                            <div class="form-lable"><span class="asterisk">*</span>Name of the Place of Death:</div>
                            <?php $fieldname = "dc_pod"; ?>
                            <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="validate[required]" /></div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="form-field-box">
                        <div class="form-lable">Education</div>
                        <div class="form-field">
                            <?php $fieldname = "dc_edu_pri"; ?>
                            <select name="<?= $fieldname ?>" class="idleField ">
                                <?php $sel = @array($user_session[$fieldprefix][$fieldname] => 'selected'); ?>
                                <option value="">Primary</option>
                                <option value="0" <?= @$sel['0'] ?>>0</option>
                                <option value="1" <?= @$sel['1'] ?>>1</option>
                                <option value="2" <?= @$sel['2'] ?>>2</option>
                                <option value="3" <?= @$sel['3'] ?>>3</option>
                                <option value="4" <?= @$sel['4'] ?>>4</option>
                                <option value="5" <?= @$sel['5'] ?>>5</option>
                                <option value="6" <?= @$sel['6'] ?>>6</option>
                                <option value="7" <?= @$sel['7'] ?>>7</option>
                                <option value="8" <?= @$sel['8'] ?>>8</option>
                                <option value="9" <?= @$sel['9'] ?>>9</option>
                                <option value="10" <?= @$sel['10'] ?>>10</option>
                                <option value="11" <?= @$sel['11'] ?>>11</option>
                                <option value="12" <?= @$sel['12'] ?>>12</option>
                            </select>
                            <?php $fieldname = "dc_edu_col"; ?>
                            <select size="1" name="<?= $fieldname ?>" class="idleField ">
                                <?php $sel = @array($user_session[$fieldprefix][$fieldname] => 'selected'); ?>
                                <option value="">College</option>
                                <option value="0" <?= @$sel['0'] ?>>0</option>
                                <option value="1" <?= @$sel['1'] ?>>1</option>
                                <option value="2" <?= @$sel['2'] ?>>2</option>
                                <option value="3" <?= @$sel['3'] ?>>3</option>
                                <option value="4" <?= @$sel['4'] ?>>4</option>
                                <option value="5+" <?= @$sel['5+'] ?>>5+</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Usual Occupation (most of life):</div>
                        <?php $fieldname = "dc_occupation"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable"><span class="asterisk">*</span>SSN:</div>
                        <?php $fieldname = "ssn"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="validate[required]" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Kind of Business:</div>
                        <?php $fieldname = "dc_business"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Company:</div>
                        <?php $fieldname = "dc_company"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Marital Status:</div>
                        <div class="form-field">
                            <?php $fieldname = "dc_marital"; ?>
                            <select size="1" name="<?= $fieldname ?>" class="idleField ">
                                <?php $sel = @array($user_session[$fieldprefix][$fieldname] => 'selected'); ?>
                                <option value="">Choose</option>
                                <option value="Married" <?= @$sel['Married'] ?>>Married</option>
                                <option value="Never Married" <?= @$sel['Never Married'] ?>>Never Married</option>
                                <option value="Widowed" <?= @$sel['Widowed'] ?>>Widowed</option>
                                <option value="Divorced" <?= @$sel['Divorced'] ?>>Divorced</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Full Name of Surviving Spouse:</div>
                        <?php $fieldname = "dc_spouse_name"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">If Wife, Provide Maiden Name:</div>
                        <?php $fieldname = "dc_wife_middle_name"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable"><span class="asterisk">*</span>Residence - Street Address:</div>
                        <?php $fieldname = "dc_residence_street"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="validate[required]" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable"><span class="asterisk">*</span>City/Town:</div>
                        <?php $fieldname = "dc_residence_city"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="validate[required]" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Inside City Limits:</div>
                        <div class="form-field">
                            <?php $fieldname = "dc_residence_citylimit"; ?>
                            <select name="<?= $fieldname ?>" class="idleField ">
                                <?php $sel = @array($user_session[$fieldprefix][$fieldname] => 'selected'); ?>
                                <option value="">Choose</option>
                                <option value="Yes" <?= @$sel['Yes'] ?>>Yes</option>
                                <option value="No" <?= @$sel['No'] ?>>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">County:</div>
                        <?php $fieldname = "dc_residence_county"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="" /></div>
                    </div>


                    <div class="form-field-box">
                        <div class="form-lable"><span class="asterisk">*</span>State:</div>
                        <?php $fieldname = "dc_residence_state"; ?>
                        <div class="form-field">
                            <select class="validate[required]" id="<?php echo $fieldname ;?>" name="<?= $fieldname ?>">
                                <option value="">Select State</option>
                                <?php $sel = @array(@$user_session[$fieldprefix][$fieldname] => 'selected'); ?>
                                <?php
                                foreach ($cfg['state_codes'] as $st) {
                                    ?><option value="<?= $st['code'] ?>" <?= @$sel[$st['code']] ?>><?= $st['name'] ?></option><?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>



                    <div class="form-field-box">
                        <div class="form-lable"><span class="asterisk">*</span>Zip Code:</div>
                        <?php $fieldname = "dc_residence_zip"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="validate[required]" /></div>
                    </div>

                    <?php
                    if ( $user_session['pkgtype'] == "preneed" ) {
                    ?>
                    <div class="form-field-box">
                        <div class="form-lable"><span class="asterisk">*</span>Telephone Number:</div>
                        <?php $fieldname = "dc_phone"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="validate[required]" /></div>
                    </div>

                    <div class="form-field-box">
                        <div class="form-lable"><span class="asterisk">*</span>Email Address:</div>
                        <?php $fieldname = "dc_email"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="validate[required ,custom[email]]" /></div>
                    </div>
                    <?php
                    }
                    ?>

                    <div class="form-field-box">
                        <div class="form-lable">Length of Residence In County:</div>
                        <?php $fieldname = "dc_length_residence_city"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Father's Full Name:</div>
                        <?php $fieldname = "dc_father_name"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Mother's Full Maiden Name:</div>
                        <?php $fieldname = "dc_mother_name"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="" /></div>
                    </div>

                </div>

                <div class="form-box">
                    <div class="form-sub-title">Type of Disposition</div>
                    <div class="form-field-box">
                        <div class="form-lable">Disposition Will Be:</div>
                        <div class="form-field"> Cremation
                        </div>
                    </div>
                    <!--<div class="form-field-box">
                            <div class="form-lable">If Cremation, Indicate Preference For Disposition of Ashes?:</div>
                            <div class="form-field">
                                    <select name="Cremation Disposition" id="Cremation Disposition" class="idleField">
                                            <option>Choose</option>
                                            <option>Cemetery Burial or Niche Wall</option>
                                            <option>Scatter</option>
                                            <option>Take Home</option>
                                            <option>Other</option>
                                            <option>Not Sure</option>
                                    </select>
                            </div>
                    </div>
                    <div class="form-field-box">
                            <div class="form-lable">Name of Cemetery (if applicable)</div>
                            <div class="form-field"><input name="" type="text" /></div>
                    </div>
                    <div class="form-field-box">
                            <div class="form-lable">City:</div>
                            <div class="form-field"><input name="" type="text" /></div>
                    </div>
                    <div class="form-field-box">
                            <div class="form-lable">State:</div>
                            <div class="form-field"><input name="" type="text" /></div>
                    </div>-->

                </div>

                <!--<div class="form-box">
                        <div class="form-sub-title">Preparation and Viewing</div>
                        <div class="form-text-content">Important Note: Viewing of the body is a choice of the family. In most cases, embalming is required or recommended for public viewing/visitation, mausoleum entombment, or transfer of remains via common carrier (i.e. shipment by air or rail). When possible, the funeral home needs authorization from the next of kin for embalming.</div>
                        <div class="form-text-content">Except in certain cases, embalming is not required by law. Embalming may be necessary, however, if you select certain funeral arrangements, such as a funeral with viewing. If you do not want embalming, you usually have the right to choose an arrangement, which does not require you to pay for it, such as a direct cremation, immediate burial and/or one-time ID viewing for family only. If you elect NOT to order embalming, State law requires refrigeration of an unembalmed body that is held for over 24 hours from the time of death.</div>
                        <div class="form-field-box">
                                <div class="form-lable"><span class="asterisk">*</span>The Family Preference Regarding Viewing/Embalming Is:</div>
                                <div class="form-field">
                <?php $fieldname = "pv_family_viewing_is"; ?>
                                        <select size="1" name="<?= $fieldname ?>" class="idleField validate[required]" style="width:170px;">
                <?php $sel = @array($user_session[$fieldprefix][$fieldname] => 'selected'); ?>
                                                <option value="">Choose</option>
                                                <option value="Public Viewing" <?= @$sel['Public Viewing'] ?>>Public Viewing</option>
                                                <option value="One time ID view for immediate family only" <?= @$sel['One time ID view for immediate family only'] ?>>One time ID view for immediate family only</option>
                                                <option value="Required by choice of disposition" <?= @$sel['Required by choice of disposition'] ?>>Required by choice of disposition</option>
                                                <option value="No viewing, no embalming" <?= @$sel['No viewing, no embalming'] ?>>No viewing, no embalming</option>
                                                <option value="Not sure" <?= @$sel['Not sure'] ?>>Not sure</option>
                                        </select>
                                </div>
                        </div>
                        <div class="form-field-box">
                                <div class="form-lable"><span class="asterisk">*</span>I Authorize Tri-State Cremation Society To Embalm</div>
                                <div class="form-field">
                <?php $fieldname = "pv_i_auth_to_embalm"; ?>
                                        <select size="1" name="<?= $fieldname ?>" class="idleField validate[required]">
                <?php $sel = @array($user_session[$fieldprefix][$fieldname] => 'selected'); ?>
                                                <option value="">Choose</option>
                                                <option value="Yes" <?= @$sel['Yes'] ?>>Yes</option>
                                                <option value="No" <?= @$sel['No'] ?>>No</option>
                                                <option value="Will Advise" <?= @$sel['Will Advise'] ?>>Will Advise</option>
                                        </select>
                                </div>
                        </div>
                        <div class="form-field-box">
                                <div class="form-lable"><span class="asterisk">*</span>Name of Authorizing Person</div>
                <?php $fieldname = "pv_auth_name"; ?>
                                <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="validate[required]" /></div>
                        </div>
                        <div class="form-field-box">
                                <div class="form-lable"><span class="asterisk">*</span>Relationship To Deceased:</div>
                <?php $fieldname = "pv_auth_relationship"; ?>
                                <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="validate[required]" /></div>
                        </div>

                </div>-->

                <div class="form-box">
                    <div class="form-sub-title">Veteran Information</div>
                    <div class="form-field-box">
                        <div class="form-lable"><span class="asterisk">*</span>Was Decedent Ever In the US Armed Forces?:</div>
                        <?php $fieldname = "vt_was_decedent_in_armed_force"; ?>
                        <div class="form-field"><input data-fieldname="<?= $fieldname ?>" name="<?= $fieldname ?>" class="checkboximage2" type="checkbox" <?= @$user_session[$fieldprefix][$fieldname] ? 'checked' : '' ?> /> <span class="form-field-text-small">(if no, continue to next section) </span></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Branch of Service:</div>
                        <?php $fieldname = "vt_branch_of_service"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Date Enlisted</div>
                        <?php $fieldname = "vt_date_enlisted"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Date Discharged</div>
                        <?php $fieldname = "vt_date_discharged"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" /></div>
                    </div>
                    <!--<div class="form-field-box">
                            <div class="form-lable">Honorable Discharge:</div>
                    <?php $fieldname = "vt_honorable_discharge"; ?>
                            <div class="form-field"><input data-fieldname="<?= $fieldname ?>" name="<?= $fieldname ?>" class="checkboximage2" type="checkbox" <?= @$user_session[$fieldprefix][$fieldname] ? 'checked' : '' ?> /></div>
                    </div>
                    <div class="form-field-box">
                            <div class="form-lable">Military Serial Number:</div>
                    <?php $fieldname = "vt_military_serial_no"; ?>
                            <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" /></div>
                    </div>-->
                    <!--<div class="form-field-box">
                            <div class="form-lable">Is A Copy of Discharge Papers Available?</div>
                    <?php $fieldname = "vt_is_copy_of_discharge_available"; ?>
                            <div class="form-field"><input data-fieldname="<?= $fieldname ?>" name="<?= $fieldname ?>" class="checkboximage2" type="checkbox" <?= @$user_session[$fieldprefix][$fieldname] ? 'checked' : '' ?> /> <span class="form-field-text-small"> (if yes, please bring for us to copy)</span></div>
                    </div>-->
                    <div class="form-field-box">
                        <div><em>This information is required for processing of the death certificate. Tri-State Cremation Society will not apply for Veteran Benefits on behalf of the deceased or deceased's family.</em></div>
                    </div>

                </div>

                <div class="form-box">
                    <div class="form-sub-title">Informant/Person In Charge Information</div>

                    <?php
                    if ( $user_session['pkgtype'] == "preneed"  )
                    {
                    ?>
                    <div class="form-field-box">
                        <div class="form-lable" style="width: 100%; text-align:center;">
                            <input type="checkbox" name="self_info"
                            <?php echo (CommonFn::get($user_session[$fieldprefix]['self_info']) == 1) ? "checked" : null;?>
                             id="self_info" value="1">
                            <label for="self_info">Check if planning for self</label>
                        </div>
                    </div>
                    <?php
                    }
                    ?>

                    <div class="form-field-box">
                        <div class="form-lable"><span class="asterisk">*</span>First Name:</div>
                        <?php $fieldname = "pi_first_name"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="validate[required]" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable"><span class="asterisk">*</span>Last Name:</div>
                        <?php $fieldname = "pi_last_name"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="validate[required]" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable"><span class="asterisk">*</span>Relationship To Deceased:</div>
                        <?php $fieldname = "pi_relationship"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="validate[required]" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable"><span class="asterisk">*</span>Email Address:</div>
                        <?php $fieldname = "pi_email"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="validate[required]" data-value="sippaprut@turkenterprisesinc.com" data-value2="kapom@turkenterprisesinc.com" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Address:</div>
                        <?php $fieldname = "pi_address"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">City:</div>
                        <?php $fieldname = "pi_city"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text"  class=""/></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">State:</div>
                        <?php $fieldname = "pi_state"; ?>
                        <div class="form-field">
                            <select class="" name="<?= $fieldname ?>">
                                <option value="">Select State</option>
                                <?php $sel = @array(@$user_session[$fieldprefix][$fieldname] => 'selected'); ?>
                                <?php
                                foreach ($cfg['state_codes'] as $st) {
                                    ?><option value="<?= $st['code'] ?>" <?= @$sel[$st['code']] ?>><?= $st['name'] ?></option><?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Zip Code:</div>
                        <?php $fieldname = "pi_zipcode"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="" data-value="55555" /></div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable"><span class="asterisk">*</span>Telephone Number:</div>
                        <?php $fieldname = "pi_phone"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" class="validate[required]" data-value="9995555555" /></div>
                    </div>

                </div>

                <div class="form-box">
                    <div class="form-sub-title">Funeral/Memorial Service Information</div>
                    <div class="form-field-box" style="display: none;">
                        <div class="form-lable">Preferred Place of Service:</div>
                        <div class="form-field">
                            <?php $fieldname = "ms_other_instruction"; ?>
                            <select size="1" name="<?= $fieldname ?>" class="idleField">
                                <?php $sel = @array($user_session[$fieldprefix][$fieldname] => 'selected'); ?>
                                <option value="">Choose</option>
                                <option value="Funeral Home" <?= @$sel['Funeral Home'] ?>>Funeral Home</option>
                                <option value="Church" <?= @$sel['Church'] ?>>Church</option>
                                <option value="Other Location" <?= @$sel['Other Location'] ?>>Other Location</option>
                                <option value="Will Advise" <?= @$sel['Will Advise'] ?>>Will Advise</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-lable">Religious Denomination:</div>
                        <?php $fieldname = "ms_religious"; ?>
                        <div class="form-field"><input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text" /></div>
                    </div>
                    <?php
                    if (@$user_session['pkgtype'] == 'atneed') {
                        ?>
                        <div class="form-field-box">
                            <div class="form-lable">Has the decedent/person being planned for purchased pre-need funeral insurance through Tri-state Cremation Society?</div>
                            <?php $fieldname = "ms_has_person_being_planned"; ?>
                            <div class="form-field"><input data-fieldname="<?= $fieldname ?>" name="<?= $fieldname ?>" class="checkboximage2" type="checkbox" <?= @$user_session[$fieldprefix][$fieldname] ? 'checked' : '' ?> /></div>
                        </div>
                        <?php
                    }
                    ?>
                </div>

                <?php
                if(isset($user_session['pkgtype'])
                    and ($user_session['pkgtype'] == 'atneed' and (!isset($user_session['is_member']) or $user_session['is_member'] != 1 ) )
                        or ($user_session['pkgtype'] == 'preneed')
                        //Show for become member
                        or (isset($user_session['isbam']) and  $user_session['isbam'] == 1 )
                    ) {
                ?>
                <div class="form-box">
                    <div class="form-sub-title">Membership Card </div>
                    <div class="form-field-box">
                        <?php $fieldname = "mail_membership"; ?>
                        <div class="form-field" style="width: 520px;">
                             <div class="form-radio-block" style="float:left; width:100%;">
                                <label class="radio" style="display:block;">
                                    <input type="radio" class="" name="<?php echo $fieldname;?>" id="<?php echo $fieldname;?>"  value="Mail Membership information to Member"
                                    <?php echo ($user_session[$fieldprefix][$fieldname] == "Mail Membership information to Member") ? "checked" : NULL;?> >
                                    Mail Membership information to Member
                                </label>
                                 <label class="radio" style="display:block;">
                                    <input type="radio" class="" name="<?php echo $fieldname;?>" id="<?php echo $fieldname;?>"  value="Mail Membership information to Informant/Person in Charge"
                                    <?php echo ($user_session[$fieldprefix][$fieldname] == "Mail Membership information to Informant/Person in Charge") ? "checked" : NULL;?> >
                                    Mail Membership information to Informant/Person in Charge
                                </label>
                             </div>
                        </div>
                    </div>

                </div>
                <?php
                }
                ?>

                <div class="form-box">
                    <div class="form-sub-title">Other Information & Instructions</div>
                    <div class="form-field-box">
                        <div class="form-lable-center">Please list any other instruction or information you would like us to have:</div>
                    </div>
                    <div class="form-field-box">
                        <div class="form-field-center">
                            <?php $fieldname = "oi_other_instruction"; ?>
                            <textarea name="<?= $fieldname ?>" cols="60" rows="5"><?= @$user_session[$fieldprefix][$fieldname] ?></textarea>
                        </div>
                    </div>

                </div>


                <?php
                $fieldname = "payfor";
                if ($user_session['pkgtype'] == 'preneed' and 0) {
                    ?>
                    <div class="form-box">
                        <div class="form-field-box">
                            <div class="form-lable-center membershipQuestion">Would you like to pay for membership only at this time ($<?= $settings['member_fee'] ?>) or the full cost of the arrangement?</div>
                        </div>
                        <div class="form-field-box">
                            <div class="form-field-center">
                                <label>
                                    <input type="radio" name="<?= $fieldname ?>" value="member" <?= @$user_session[$fieldname] == 'member' ? 'checked' : '' ?> />
                                    $<?= $settings['member_fee'] ?> Membership Fee Only
                                </label>
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                <label>
                                    <input type="radio" name="<?= $fieldname ?>" value="full" <?= @$user_session[$fieldname] == 'full' ? 'checked' : '' ?> />
                                    Pay in Full for the Arrangement
                                </label>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                 // if (@$user_session['pkgtype'] == 'atneed') {
                  ?>
                <!-- <p>After you have made your choice above, please make sure to click "NEXT", to the right, to proceed to the payment center to complete your arrangement.</p> -->


                    <!--<div class="form-box">
                            <div class="form-field-box">
                                    <div class="form-lable"><span class="asterisk">*</span>Would you like us to publish an obituary on our Website?<br>($20 fee) :</div>
                    <?php $fieldname = "obit_is_publish_on_our_web"; ?>
                                    <div class="form-field"><input data-fieldname="<?= $fieldname ?>" name="<?= $fieldname ?>" class="checkboximage2 chkobitonweb" type="checkbox" <?= @$user_session[$fieldprefix][$fieldname] ? 'checked' : '' ?> /></div>
                            </div>
                    </div>-->
                    <?php
               // }
                ?>
            </div>

            <?php
            $this->load->view('_right_bar');
            if ($isAdmin) {
                ?>
                <center class="noPrint"><input type="button" value="Print" onclick="window.print();" class="noPrintVersion"><br><br></center>
                    <?php
                }
                ?>

        </div>

    </div>
    <?php
    if ($isAdmin) {
        ?><script src="<?= $cfg['root'] ?>/assets/js/form_vital_print.js"></script><?php
    } else {
        ?><script src="<?= $cfg['root'] ?>/assets/js/form_vital.js"></script><?php
    }
    if (@$user_session['memberQuestionAnswered']) {
        ?>
        <script>
                    $(function() {
                        memberQuestionMove();
                    });
        </script>
        <?php
    }
    ?>

    <!--Data-->
</form>
<?php
echo $footer?>