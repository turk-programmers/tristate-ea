<?=$header?>
<form method="post" id="mainform" name="mainform" style="margin:0;">
<input type="hidden" name="scriptaction" 	id="scriptaction"	value="validate" />
<input type="hidden" name="notvalidate" 	id="notvalidate"	value="" />
<input type="hidden" name="next_form" 		id="next_form"		value="<?=$next_form?>" />
<!--Data-->

<div id="ea" class="">
	<?php
	$this->load->view('message_badge');
	?>
	
	<div class="ea-step-title-box">
		<h1 class="ea-step-title">step 4:  PAPERWORK</h1>
	</div> 
	
	<div id="content-ea" >
		<div id="ea-content-inner">
	    	
	        <div class="form-box" style="margin-top: 0px;">
		<div class="form-text-content">Delaware State law requires that all funds collected for Funeral or Cremation Services to be provided in the future be placed in escrow by the funeral home.</div>
		<div class="form-text-content">Tri-State Cremation Society of Delaware Valley will create a whole life insurance policy in your name. Forethought Life Insurance Company will mail the policy directly to you within 30 days.</div>
		<div class="form-text-content">(Note: Forethought Life Insurance Company will only insure those less than 99 years of age.)</div>
	
	        	<div class="form-sub-title">Proposed Insured</div>
	            <div class="form-field-box">
	                <div class="form-lable">&nbsp;</div>
	                <div class="form-field">
	                	<?php $fieldname="pi_prename";?>
	                	<?php $sel = @array($user_session[$fieldprefix][$fieldname] => 'checked');?>
	                	<label><input name="<?=$fieldname?>" type="radio" class="validate[required]" <?=@$sel['Mr.']?> value="Mr." /> Mr.</label>
	                	<label><input name="<?=$fieldname?>" type="radio" class="validate[required]" <?=@$sel['Mrs.']?> value="Mrs." /> Mrs.</label>
	                	<label><input name="<?=$fieldname?>" type="radio" class="validate[required]" <?=@$sel['Ms.']?> value="Ms." /> Ms.</label>
	                	<label><input name="<?=$fieldname?>" type="radio" class="validate[required]" <?=@$sel['Miss']?> value="Miss" /> Miss</label>
	                </div>
	            </div>
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>First Name:</div>
	                <?php $fieldname="pi_first_name";?>
	                <div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
	            </div>
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>Middle Name:</div>
	                <?php $fieldname="pi_middle_name";?>
	                <div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
	            </div>
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>Last Name:</div>
	                <?php $fieldname="pi_last_name";?>
	                <div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
	            </div>
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>Social Security Number:</div>
	                <?php $fieldname="pi_ssn";?>
	                <div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
	            </div>
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>Age:</div>
	                <?php $fieldname="pi_age";?>
	                <div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
	            </div>
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>Date of Birth:</div>
	                <div class="form-field">
						<?php $fieldname = 'pi_dob_'.'month';?>
						<?php $sel=@array($user_session[$fieldprefix][$fieldname]=>"selected");?>
						<select name="<?=$fieldname?>" id="<?=$fieldprefix?><?=$fieldname?>" class="validate[required]">
							<OPTION value="">Month</OPTION>
							<?php
							for($i=1;$i<=12;$i++){
								$month=date("F",mktime(0,0,0,$i,1,2000))
								?><OPTION value="<?=$month?>" <?=@$sel[$month]?>><?=$month?></OPTION><?php
							}
							?>
						</SELECT>
						<?php $fieldname = 'pi_dob_'.'day';?>
						<?php $sel=@array($user_session[$fieldprefix][$fieldname]=>"selected");?>
						<SELECT name="<?=$fieldname?>" id="<?=$fieldprefix?><?=$fieldname?>" class="validate[required]">
							<OPTION value="">Day</OPTION>
							<?php
							for($i=1;$i<=31;$i++){
								?><OPTION value="<?=$i?>" <?=@$sel[$i]?>><?=$i?></OPTION><?php
							}
							?>
						</SELECT>
						<?php $fieldname = 'pi_dob_'.'year';?>
						<?php $sel=@array($user_session[$fieldprefix][$fieldname]=>"selected");?>
						<SELECT name="<?=$fieldname?>" id="<?=$fieldprefix?><?=$fieldname?>" class="validate[required]">
							<OPTION value="">Year</OPTION>
							<?php
							for($i=date("Y");$i>=1900;$i--){
								?><OPTION value="<?=$i?>" <?=@$sel[$i]?>><?=$i?></OPTION><?php
							}
							?>
						</SELECT>
	                </div>
	            </div>
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>Sex:</div>
	                <div class="form-field">
	                	<?php $fieldname="pi_sex";?>
	                	<?php $sel = @array($user_session[$fieldprefix][$fieldname] => 'checked');?>
	                	<label><input name="<?=$fieldname?>" type="radio" class="validate[required]" <?=@$sel['Male']?> value="Female" /> Male</label>
	                	<label><input name="<?=$fieldname?>" type="radio" class="validate[required]" <?=@$sel['Female']?> value="Female" /> Female</label>
	                </div>
	            </div>
	            
	        </div>
	        
	        <div class="form-box">
	        	<div class="form-sub-title">Mailing Address for Insured<span class="form-sub-title-small">(Where to send information about this insurance)</span></div>
	            
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>Telephone Number:</div>
	                <?php $fieldname="ma_phone";?>
	                <div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
	            </div>
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>Street:</div>
	                <?php $fieldname="ma_street";?>
	                <div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
	            </div>
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>City:</div>
	                <?php $fieldname="ma_city";?>
	                <div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
	            </div>
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>State:</div>
	                <?php $fieldname="ma_state";?>
	                <div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
	            </div>
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>Zip Code:</div>
	                <?php $fieldname="ma_zip";?>
	                <div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
	            </div>
	            
	        
	        </div>
	        
	        <div class="form-box">
	        	<!--<div class="form-field-box">
	                <div class="form-lable">Face Amount:</div>
	                <div class="form-field"><input name="" type="text" /></div>
	            </div>-->
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>Single Premium:</div>
	                <div class="form-field"><input name="" type="text" disabled value="$<?=number_format($user_session['summary']['total'],2)?>" /></div>
	            </div>
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>Payment is for $:</div>
	                <div class="form-field"><input name="" type="text" disabled value="$<?=number_format($user_session['summary']['total'],2)?>" /></div>
	            </div>
	            <div class="form-text-content-small-center">
	            	<strong>Payable to Forthought Life Insurance Company.</strong><br>
	            </div>
	            
	        
	        </div>
	        
	        <div class="form-box">
	        	<div class="form-sub-title">Beneficiary</div>
	            <div class="form-text-content">
	            	Death proceeds are to be paid to the Beneficiary which is the estate of the insured. If another Beneficiary is desired, assignment or other directions received from the Certificateholder during the Insured's life
	            </div>
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>Estate of:</div>
	                <?php $fieldname="be_estate_of";?>
	                <div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
	            </div>
	            <!--<div class="form-field-box">
	                <div class="form-lable">First Name:</div>
	                <div class="form-field"><input name="" type="text" /></div>
	            </div>
	            <div class="form-field-box">
	                <div class="form-lable">Middle Name:</div>
	                <div class="form-field"><input name="" type="text" /></div>
	            </div>
	            <div class="form-field-box">
	                <div class="form-lable">Last Name:</div>
	                <div class="form-field"><input name="" type="text" /></div>
	            </div>-->
	        </div>
	        
	        <div class="form-box">
	            <div class="form-text-content-bold-italic">
	            	The above information is true and complete to the best of my knowledge and belief. Any person who with intent to defraud or knowing that he is facilitating fraud against an Insurer, submits an application or files a claim containing a false or deceptive statement, is guilty of insurance fraud. No insurance will take effect until the premium has been paid and a certificate has been issued while the Insured is living.
	            </div>
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>Signature of Proposed Insured:</div>
	                <?php $fieldname="signature_of_proposed_insured";?>
	                <div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
	            </div>
	            <!--<div class="form-field-box">
	                <div class="form-lable">Signature of Certificateholder (only if other than Insured):</div>
	                <div class="form-field"><input name="" type="text" /></div>
	            </div>-->
	        </div>
	        
	    </div>
	     
		<?php
		$this->load->view('_right_bar');
		if($isAdmin){
			?>
			<center class="noPrint"><input type="button" value="Print" onclick="window.print();" class="noPrintVersion"><br><br></center>
			<?php
		}
		?>
			    
	</div>
</div>
<?php
if($isAdmin){
	?><script src="<?=$cfg['root']?>/assets/js/form_insurance_1_print.js"></script><?php
}else{
	?><script src="<?=$cfg['root']?>/assets/js/form_insurance_1.js"></script><?php
}
?>
<!--Data-->
</form>
<?php echo $footer?>