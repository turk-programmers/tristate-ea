<?=$header?>
<form method="post" id="mainform" name="mainform" style="margin:0;">
<input type="hidden" name="scriptaction" 	id="scriptaction"	value="validate" />
<input type="hidden" name="notvalidate" 	id="notvalidate"	value="" />
<input type="hidden" name="next_form" 		id="next_form"		value="<?=$next_form?>" />
<!--Data-->

<div id="ea" class="">
	<?php
	$this->load->view('message_badge');
	?>
	
	<div class="ea-step-title-box"> 
		<h1 class="ea-step-title">step 4:  PAPERWORK</h1>
	</div>
	
	<div id="content-ea" >
		<div id="ea-content-inner">
	    	<div class="form-title">Revocable Assignment of Death Benefit</div>
	        
	        <div class="form-box">
	        	
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>Insured:</div>
	                <?php $fieldname="ra_insured";?>
	                <div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
	            </div>
	            
	            <div class="form-text-content">
	            	I, the Owner, hereby assign the death benefit of the numbered policy/certificate to the Funeral Home identified below, in an amount equal to the retail price of any goods and/or services provided by the Funeral Home. If the death benefit exceeds the amonth due the Funeral Home, the excess death benefit proceeds will be paid to the beneficiary of the policy/certificate.
	            </div>
	            
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>Funeral Home:</div>
	                <?php $fieldname="ra_fh";?>
	                <div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?$user_session[$fieldprefix][$fieldname]:'Tri-State Cremation Society'?>" type="text" class="validate[required]" /></div>
	            </div>
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>Address (City & State):</div>
	                <?php $fieldname="ra_city_state";?>
	                <div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?$user_session[$fieldprefix][$fieldname]:'Wilmington, DE'?>" type="text" class="validate[required]" /></div>
	            </div>
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>Telephone Number (if known):</div>
	                <?php $fieldname="ra_phone";?>
	                <div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?$user_session[$fieldprefix][$fieldname]:'302-764-6246'?>" type="text" class="validate[required]" /></div>
	            </div>
	            
	            <div class="form-text-content">
	            	I understand that I may revoke this assignment at any time by notifying Forethought Life Insurance Company at the above address.
	            </div>
	            
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>Signature of Policyowner:</div>
	                <?php $fieldname="ra_signature";?>
	                <div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?$user_session[$fieldprefix][$fieldname]:''?>" type="text" class="validate[required]" /></div>
	            </div>
	            <div class="form-field-box">
	                <div class="form-lable"><span class="asterisk">*</span>Date:</div>
	                <div class="form-field"><input type="text" value="<?=date('F d, Y', @$user_session['purchasedateint'])?>" disabled /></div>
	            </div>
	        
	        </div>
	    
	    </div>
	    
		<?php
		$this->load->view('_right_bar');
		if($isAdmin){
			?>
			<center class="noPrint"><input type="button" value="Print" onclick="window.print();" class="noPrintVersion"><br><br></center>
			<?php
		}
		?>
	    
	</div>
	
</div>
<script src="<?=$cfg['root']?>/assets/js/form_insurance_2.js"></script>
<?php
if($isAdmin){
	?><script src="<?=$cfg['root']?>/assets/js/form_insurance_2_print.js"></script><?php
}else{
	?><script src="<?=$cfg['root']?>/assets/js/form_insurance_2.js"></script><?php
}
?>
<!--Data-->
</form>
<?php echo $footer?>