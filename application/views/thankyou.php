<?= $header ?>
<!--Data-->
<div id="ea">
    <?php
    $this->load->view('message_badge');
    ?>
    <div class="ea-step-title-box">
        <h1 class="ea-step-title">Thank You</h1>
    </div>

    <?php
    $price = @$user_session['summary']['total'];
    if (@$user_session['payfor'] == 'member') {
        $price = @$user_session['summary']['total_member'];
    }
    ?>

    <div id="content-ea" >

        <link rel="stylesheet" href="/themes/tristate/normalize.css">
        <link rel="stylesheet" href="/themes/tristate/main.css">
        <link rel="stylesheet" href="/themes/tristate/typography.css">
        <link rel="stylesheet" href="<?= $cfg['root'] ?>/assets/css/ea.css" /></head>

        <div id="ea-content-inner">
            <div class="form-text-content"><strong>Your order ID is <?= @$user_session['uniqid'] ?>.</strong></div>
            <?php
            if (@$user_session[$paymentprefix]['payment_method'] == 'cc') {
                ?>
                <div class="form-text-content">The amount of $<?= number_format($price, 2) ?> has been charged to your credit card.</div>
                <?php
            }
            ?>
            <div class="form-text-content">
                Thank you for allowing Tri-State Cremation Society to serve your family. If you have any questions please call us at any time at <?= @$settings['client_phone_local'] ?>.
            </div>
            <div class="form-text-content">
                You will be e-mailed a confirmation of your information and payment within 24 hours. Our office will call you to confirm your order and answer any questions that you may have.
            </div>
            <?php
            if ($user_session['pkgtype'] == 'atneed') {
                ?>
                <div class="form-text-content">
                    If a death has occurred please contact Tri-State over the phone and then complete the online eArrangement with full payment.<br>
                    Our staff will dispatch our removal team to come to your home or designated location of the deceased.<br>
                    Once the removal has occurred, your loved one will be brought to Tri State's crematorium for the cremation process.<br>
                    After the cremation is performed, the cremains will be placed in the selected container and mailed via USPS certified mail to the address provided.
                </div>
                <?php
            }
            ?>

            <?php
            if($user_session['isbam'] != 1){
            ?>
            <div class="form-text-content">
                <i>Unless we are involved in the memorial service or cemetery burial, we do not write up or submit obituary notices. Newspapers will take obituaries directly from the family. If you want to place an obituary on our website, we can assist you with that. For helpful hints on writing an obituary and an easy method to submit the obituary text to us, visit our <a target="_blank" href="/obituary/how-write-obituary/">"How to Write an Obituary"</a> page.</i>
            </div>
            <?php
            }
            else{
            ?>
                <div class="form-text-content">
                    <i>We do not write up or submit obituary notices to the newspaper or any other entity. Newspapers will take obituaries directly from the family so please contact your local newspapers directly. If you purchased an obituary to be placed on our website, instructions on how to write & submit your obituary to us can be found by visiting our <a target="_blank" href="/obituary/how-write-obituary/">"How to Write an Obituary"</a> page.</i>
                </div>
            <?php
            }
            ?>

            <div class="form-text-content">
                <ul>
                    <li>You may <a href="#" onclick="$('#content-ea').printThis();
                            return false;">print this page</a> for your records
                    <li>If you have any other questions you can email <a href="mailto:<?= @$settings['client_email'] ?>"></a>
                    <li>You may print your <a href="<?= $cfg['root'] ?>/sogs" target="_blank">Statement of Goods and Services</a></li>
                </ul>
            </div>
        </div>

        <?php
        $this->load->view('_right_bar');
        ?>

    </div>
</div>
<script src="<?= $cfg['root'] ?>/assets/js/thankyou.js"></script>
<!--Data-->

<!-- Google Code for On-Line Arrangement Conversion Page -->
<script type="text/javascript">
                        /* <![CDATA[ */
                        var google_conversion_id = 968756679;
                        var google_conversion_language = "en";
                        var google_conversion_format = "2";
                        var google_conversion_color = "ffffff";
                        var google_conversion_label = "yrZ6CJb1oVYQx5v4zQM";
                        var google_remarketing_only = false;
                        /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/968756679/?label=yrZ6CJb1oVYQx5v4zQM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


<?=
$footer?>