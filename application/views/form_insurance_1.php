<?=$header?>
<form method="post" id="mainform" name="mainform" style="margin:0;">
<input type="hidden" name="scriptaction" 	id="scriptaction"	value="validate">
<input type="hidden" name="notvalidate" 	id="notvalidate"	value="">
<input type="hidden" name="next_form" 		id="next_form"		value="<?=$next_form?>">
<!--Data-->

<div id="ea" class="">
	<?php
	$this->load->view('message_badge');
	?>

	<div class="ea-step-title-box">
		<h1 class="ea-step-title">step 4:  PAPERWORK</h1>
	</div>

	<div id="content-ea">
		<div id="ea-content-inner">

				<div class="form-box" style="margin-top: 0px;">
					State laws require that all pre-funded funerals be held in a preneed insurance policy.  The information below is required to complete the insurance application with Great Western.
				</div>
				<div class="form-box">
					<div class="form-sub-title">INSURED’S INFORMATION</div>
					<div class="form-field-box">
						<div class="form-lable">Full Name:</div>
						<?php $fieldname="insured_name";?>
						<div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
					</div>
					<div class="form-field-box">
						<div class="form-lable">Social Security #:</div>
						<?php $fieldname="insured_ssn";?>
						<div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
					</div>
					<div class="form-field-box">
						<div class="form-lable">Sex:</div>
						<?php $fieldname="insured_sex";?>
						<div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
					</div>
					<div class="form-field-box">
						<div class="form-lable">Birthdate (M/D/YYYY):</div>
						<div class="form-field">
							<?php $fieldname = 'insured_birth_'.'month';?>
							<?php $sel=@array($user_session[$fieldprefix][$fieldname]=>"selected");?>
							<select id="<?=$fieldprefix?><?=$fieldname?>" class="validate[required]" disabled >
								<OPTION value="">Month</OPTION>
								<?php
								for($i=1;$i<=12;$i++){
									$month=date("F",mktime(0,0,0,$i,1,2000))
									?><OPTION value="<?=$month?>" <?=@$sel[$month]?>><?=$month?></OPTION><?php
								}
								?>
							</SELECT>
							<input type="hidden" name="<?=$fieldname?>" value="<?php echo $user_session[$fieldprefix][$fieldname];?>"/>
							<?php $fieldname = 'insured_birth_'.'day';?>
							<?php $sel=@array($user_session[$fieldprefix][$fieldname]=>"selected");?>
							<SELECT  id="<?=$fieldprefix?><?=$fieldname?>" disabled>
								<OPTION value="">Day</OPTION>
								<?php
								for($i=1;$i<=31;$i++){
									?><OPTION value="<?=$i?>" <?=@$sel[$i]?>><?=$i?></OPTION><?php
								}
								?>
							</SELECT>
							<input type="hidden" name="<?=$fieldname?>" value="<?php echo $user_session[$fieldprefix][$fieldname];?>"/>

							<?php $fieldname = 'insured_birth_'.'year';?>
							<?php $sel=@array($user_session[$fieldprefix][$fieldname]=>"selected");?>
							<SELECT  id="<?=$fieldprefix?><?=$fieldname?>" disabled>
								<OPTION value="">Year</OPTION>
								<?php
								for($i=date("Y");$i>=1900;$i--){
									?><OPTION value="<?=$i?>" <?=@$sel[$i]?>><?=$i?></OPTION><?php
								}
								?>
							</SELECT>
							<input type="hidden" name="<?=$fieldname?>" value="<?php echo $user_session[$fieldprefix][$fieldname];?>"/>
						</div>
					</div>
					<div class="form-field-box">
						<div class="form-lable">Age:</div>
						<?php $fieldname="insured_age";?>
						<div class="form-field">
							<input readonly name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" />
						</div>
					</div>
					<div class="form-field-box">
						<div class="form-lable">Mailing Address:</div>
						<?php $fieldname="insured_mailing_address";?>
						<div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
					</div>
					<div class="form-field-box">
						<div class="form-lable">City:</div>
						<?php $fieldname="insured_city";?>
						<div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
					</div>
					<div class="form-field-box">
						<div class="form-lable">State:</div>
						<?php $fieldname="insured_state";?>
						<div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
					</div>
					<div class="form-field-box">
						<div class="form-lable">Zip:</div>
						<?php $fieldname="insured_zip";?>
						<div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
					</div>
					<div class="form-field-box">
						<div class="form-lable">Telephone #:</div>
						<?php $fieldname="insured_phone";?>
						<div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
					</div>
				</div>

				<div class="form-box">
					<div class="form-sub-title">CERTIFICATE INFORMATION</div>
					<div class="form-field-box">
						<div class="form-lable">Total:</div>
						<div class="form-field">
							Face Amount $<br> <input type="text" value="$<?=number_format($user_session['summary']['total'],2)?>" readonly><br>
							Total Paid to Agent $<br> <input type="text" value="$<?=number_format($user_session['summary']['total'],2)?>" readonly>
						</div>
					</div>

					<div class="form-field-box">
						<div class="form-lable">Payment Method:</div>
						<div class="form-field">
							<input type="checkbox" value="Single" name="insured_payment_method" checked disabled> Single
						</div>
					</div>

				</div>

				<div class="form-box">
					<div class="form-sub-title">BENEFICIARIES</div>
					<div class="form-field-box">
						<div class="form-lable">Primary:</div>
						<?php $fieldname="insured_beneficiaries";?>
						<div class="form-field"><input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]" /></div>
					</div>
				</div>

				<div class="form-text-content">Any person who knowingly and with intent to defraud any
					insurance company or other person files an application for insurance or statement
					of claim containing any materially false information or conceals for the purpose
					of misleading, information concerning any fact material thereto commits a fraudulent
					insurance act, which is a crime and subjects such person to criminal and civil penalties.
				</div>


			<div class="form-box">
					<div class="form-sub-title">ASSIGNMENT</div>
					<div class="form-text-content">
						<table>
							<tr>
								<td valign="top" width="25%">
									<input type="checkbox" value="Yes" name="insured_assignment" disabled> Yes
									<input type="checkbox" value="No"  name="insured_assignment" checked="checked" disabled> No<br>
									Initial Approval<br>
									<?php $fieldname="insured_initial_1";?>
									<input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]"  style="width: 100px;">
								</td>
								<td>I hereby irrevocably assign and transfer all the benefits
									and proceeds of this certificate to as their interest may appear.
									I understand fully the effects of this assignment and transfer.
									It is my intention as owner to continue to pay premiums and retain ownership.
								</td>
							</tr>
						</table>
					</div>
				</div>

				<div class="form-box">
					<div class="form-text-content">Does the applicant have any existing policy or annuity?<br>
						<input type="checkbox" disabled value="No" checked="checked" name="insured_existing_policy"> No
						or  <input type="checkbox" value="Yes" disabled name="insured_existing_policy"> Yes
					</div>

					<div class="form-text-content">Will the proposed insurance replace any existing policy or annuity?<br>
						<input type="checkbox" value="No" checked="checked" disabled name="insured_replace_insurance"> No
						or <input type="checkbox" value="Yes" disabled name="insured_replace_insurance"> Yes <span class="form-field-text-small">If yes, please complete a replacement form.</span>
					</div>

						<?php $fieldname="insured_name2";?>
					<div class="form-text-conten">INSURED’S NAME <input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]"  style="width: 350px;"></div>
				</div>


				<div class="form-box">
					<div class="form-sub-title">AGREEMENT</div>
					<div class="form-text-content">
						By signing below, I agree that: (1) To the best of my knowledge and belief,
						statements in this Application are complete and true. (2) When the certificate
						is delivered, the Insured must be alive and in the same health as described above
						or there will be no insurance. Also, the full premium for the chosen period
						must be paid by the time the certificate is delivered. (3) By accepting the
						certificate, I approve any change(s), correction(s), or addition(s) that
						Great Western made when issuing it. If my approval requires written consent,
						a form will be included.

					</div>
					<div class="form-text-content">
						<strong>Insurable Interest:</strong> If the owner is other than the insured,
						by signing below, the owner certifies that he/she has insurable interest in
						the life of the insured as defined by the state statute in which the policy is issued.

					</div>
					<div class="form-text-content">
						<strong>Authorization:</strong> By signing below, I approve of any healthcare provider,
						medical facility, or other person, including a Veterans Administration Hospital,
						giving the Great Western Insurance Company any records or information it needs
						about the Insured’s health. A copy of this approval will be as effective as
						the original. This approval is only valid for 30 months. The Insured, or a person
						authorized to act on behalf of the Insured, is entitled to receive a copy of this
						authorization upon request. <strong>I affirm that no illustration was used in
							the sale of this product.</strong>

					</div>
					<div class="form-text-content">
						<table width="100%" align="center">
							<tr>
								<td>
									<?php $fieldname="insured_signed_at";?>
									Signed at  <input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" type="text" class="validate[required]"  style="width: 200px;">,
									<?php $fieldname="insured_signed_date";?>
									<input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" readonly type="text" class="validate[required]"  style="width: 220px;">
								</td>
							</tr>
							<tr><td><span style="
										font-size: 12px;
										display: inline-block;
										margin-left: 130px;
										top: -5px;
										position: relative;
										margin-bottom: 10px;">City and State</span></td></tr>
							<tr>
								<td>
									<?php $fieldname="insured_signed_insured";?>
									Insured <input name="<?=$fieldname?>" value="<?=@$user_session[$fieldprefix][$fieldname]?>" readonly type="text" class="validate[required]">


								</td>
							</tr>
						</table>
					</div>
					<div class="form-text-content"><strong>To the Applicant:</strong> You should hear from the Company
						within sixty days of the application date. If you don’t, state the facts of your application
						in a letter to the Secretary of Great Western Insurance Company at the address listed above.
					</div>
			</div>

		</div>

		<?php
		$this->load->view('_right_bar');
		if($isAdmin){
			?>
			<center class="noPrint"><input type="button" value="Print" onclick="window.print();" class="noPrintVersion"><br><br></center>
			<?php
		}
		?>

	</div>
</div>
<?php
if($isAdmin){
	?><script src="<?=$cfg['root']?>/assets/js/form_insurance_1_print.js"></script><?php
}else{
	?><script src="<?=$cfg['root']?>/assets/js/form_insurance_1.js"></script><?php
}
?>
<!--Data-->
</form>
<?php echo $footer?>