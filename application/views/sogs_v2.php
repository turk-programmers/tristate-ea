<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>SOGS</title>
        <link rel="stylesheet" type="text/css" href="<?= $root ?>/assets/css/ea.css" />
    </head>

    <body>
        <div class="sogs">

            <div class="sogs-content">
                <div class="sogs-title">Estimated Statement of Funeral Goods and Services</div>
                <div class="sogs-title">Tri-State Cremation Society of Delaware Valley</div>
                <div class="sogs-title-small"><?= @$settings['client_address_street'] ?>, Suite 100, <?= @$settings['client_address_city'] ?>, <?= @$settings['client_address_state'] ?> <?= @$settings['client_address_zip'] ?><br>
                        <?= @$settings['client_phone_local'] ?> &nbsp; &bull;  &nbsp; fax <?= @$settings['client_fax'] ?> </div>

                <div class="sogs-text-top">
                    <strong>NAME:</strong> <div style="width:460px;" class="input"><?= $deceased_name ?>&nbsp;</div>
                </div>

                <div class="sogs-left-side">
                    <div class="title-bold">SERVICE CHARGES</div>
                    <?php
                    foreach ($sogs_services as $i => $s) {
                        ?>
                        <div class="statement-of-goods-box">
                            <div class="statement-of-goods-box-prices"><div class="input">
                                    <?php
                                    if (in_array($pkg, $s['includein'])) {
                                        if ($s['price']) {
                                            echo $s['price'];
                                        } else {
                                            echo 'Included';
                                        }
                                    } elseif ($s['price']) {
                                        echo $s['price'];
                                    } else {
                                        echo 'N/A';
                                    }
                                    ?>
                                </div></div>
                            <div class="statement-of-goods-box-details"><?= $s['name'] ?></div>
                            <div class="statement-of-goods-box-mark">$</div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="statement-of-goods-total">
                        <div class="statement-of-goods-box-total">TOTAL SERVICE CHARGES</div>
                        <div class="statement-of-goods-box-mark">$</div>
                        <div class="statement-of-goods-box-prices"><div class="input"><?= $totalsprice ? number_format($totalsprice, 2) : '&nbsp;' ?></div></div>
                    </div>

                    <div class="title-bold">MERCHANDISE</div>
                    <?php
                    foreach ($sogs_merchs as $i => $m) {
                        ?>
                        <div class="statement-of-goods-box">
                            <?php
                            if (!is_array(@$m['items'])) {
                                ?>
                                <div class="statement-of-goods-box-prices"><div class="input">
                                        <?php
                                        if (@$m['price']) {
                                            echo $m['price'];
                                        } elseif (in_array($pkg, $m['includein'])) {
                                            echo 'Included';
                                        } else {
                                            echo 'N/A';
                                        }
                                        ?>
                                    </div></div>
                                <?php
                            }
                            ?>
                            <div class="statement-of-goods-box-details"><?= $m['name'] ?></div>
                            <?php
                            if (!is_array(@$m['items'])) {
                                ?>
                                <div class="statement-of-goods-box-mark">$</div>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                        if (is_array(@$m['items'])) {
                            foreach ($m['items'] as $sitem) {
                                ?>
                                <div class="statement-of-goods-box subitem">
                                    <div class="statement-of-goods-box-prices"><div class="input"><?= $sitem['price'] ?></div></div>
                                    <div class="statement-of-goods-box-details"><?= $sitem['name'] ?></div>
                                    <div class="statement-of-goods-box-mark">$</div>
                                </div>
                                <?php
                            }
                        }
                    }
                    ?>
                    <div class="statement-of-goods-total">
                        <div class="statement-of-goods-box-prices"><div class="input"><?= $totalmprice ? number_format($totalmprice, 2) : '&nbsp;' ?></div></div>
                        <div class="statement-of-goods-box-total">TOTAL MERCHANDISE CHARGES</div>
                        <div class="statement-of-goods-box-mark">$</div>
                    </div>

                    <div class="title-bold">MONIES ADVANCED BY FUNERAL HOME</div>
                    <!-- <?php print_r($sogs_advances); ?> -->
                    <?php
                    foreach ($sogs_advances as $i => $s) {
                        ?>
                        <div class="statement-of-goods-box">
                            <div class="statement-of-goods-box-prices"><div class="input">
                                    <?php
//                                    echo "\n $pkg | " . print_r($s, 1) . "  \n";
                                    if (in_array($pkg, $s['includein'])) {
                                        if ($s['price']) {
                                            echo $s['price'];
                                        } else {
                                            echo 'Included';
                                        }
                                    } elseif ($s['price']) {
                                        echo $s['price'];
                                    } else {
                                        echo 'N/A';
                                    }
                                    ?>
                                </div></div>
                            <div class="statement-of-goods-box-details"><?= $s['name'] ?></div>
                            <div class="statement-of-goods-box-mark">$</div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="statement-of-goods-total">
                        <div class="statement-of-goods-box-prices"><div class="input"><?= number_format(@$totalaprice, 2) ?></div></div>
                        <div class="statement-of-goods-box-total">TOTAL MONIES ADVANCED</div>
                        <div class="statement-of-goods-box-mark">$</div>
                    </div>


                </div>



                <div class="sogs-right-side">
                    <?php $totalFuneral = $totalaprice + $totalmprice + $totalsprice; ?>
                    <div class="statement-of-goods-box">
                        <div class="statement-of-goods-box-prices"><div class="input"><?= number_format($totalFuneral , 2) ?></div></div>
                        <div class="statement-of-goods-box-details-bold">TOTAL FUNERAL CHARGES</div>
                        <div class="statement-of-goods-box-mark">$</div>
                    </div>
                    <div class="statement-of-goods-box">
                        <div class="statement-of-goods-box-prices">
                            <?php

                            switch ($user_session['packageselected']['id']) {
                                case 2:
                                    $dc = 187;
                                    if (@$user_session['pkgtype'] == 'preneed' or @ $user_session['is_member']) {
                                        $dc = 387;
                                    }
                                    break;
                                case 3:
                                    $dc = 1336;
                                    if (@$user_session['pkgtype'] == 'preneed' or @ $user_session['is_member']) {
                                        $dc = 1536;
                                    }
                                    break;
                                case 4:
                                    $dc = 1790;
                                    if (@$user_session['pkgtype'] == 'preneed' or @ $user_session['is_member']) {
                                        $dc = 1990;
                                    }
                                    break;
                            }
                            ?>
                            <div class="input"><?= number_format($dc, 2) ?></div></div>
                        <div class="statement-of-goods-box-details-bold">
                            CREDITS -
                            MEMBER DISCOUNT -
                            <?php
                            switch ($user_session['packageselected']['id']) {
                                case 2:
                                    echo 'GOLD';
                                    break;
                                case 3:
                                    echo 'SILVER';
                                    break;
                                case 4:
                                    echo 'BRONZE';
                                    break;
                            }
                            ?>
                        </div>
                        <div class="statement-of-goods-box-mark">$</div>
                    </div>
                    <?php
                    if( (isset($user_session['pkgtype']) and $user_session['pkgtype'] == "preneed") ){
                        ?>
                        <div class="statement-of-goods-box">
                            <div class="statement-of-goods-box-prices">
                                <div class="input"><?= number_format($user_session['summary']['total_member'], 2) ?></div></div>
                            <div class="statement-of-goods-box-details-bold">CREDITS - MEMBERSHIP FEE</div>
                            <div class="statement-of-goods-box-mark">$</div>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="statement-of-goods-box">
                            <div class="statement-of-goods-box-prices">
                                <div class="input">0.00</div></div>
                            <div class="statement-of-goods-box-details-bold">CREDITS - MEMBERSHIP FEE</div>
                            <div class="statement-of-goods-box-mark">$</div>
                        </div>
                        <?php
                    }
                    ?>
                    <!--<div class="statement-of-goods-box">
                        <div class="statement-of-goods-box-prices"><div class="input"><?= number_format((($totalaprice + $totalmprice + $totalsprice) - $dc), 2) ?></div></div>
                        <div class="statement-of-goods-box-details-bold">TOTAL ESTIMATED FUNERAL COST</div>
                        <div class="statement-of-goods-box-mark">$</div>
                    </div>-->
                    <div class="statement-of-goods-box">
                        <div class="statement-of-goods-box-prices">
                            <div class="input">
                            <?php
                                // $totalSummary = $totalaprice + $totalmprice + $totalsprice - $user_session['summary']['total_member'] - $dc; https://funeralnet.teamwork.com/tasks/8730092
                                //$totalSummary = $totalaprice + $totalmprice + $totalsprice + $user_session['summary']['total_member'] - $dc;


                                /*
                                * It will NOT need to match the order archive total, because the Client does not want the membership $30 fee reflected in the SOGS total.  2016/08/29 by Sip
                                * /https://funeralnet.teamwork.com/#/tasks/8730092?c=4028443&
                                **/
                                $totalSummary = $totalaprice + $totalmprice + $totalsprice - $dc - $user_session['summary']['total_member'];
                                echo number_format($totalSummary, 2);
                            ?>
                           </div>
                        </div>
                        <div class="statement-of-goods-box-details-bold">TOTAL ESTIMATED FUNERAL COST</div>
                        <div class="statement-of-goods-box-mark">$</div>
                    </div>

                    <div class="title-bold">THIS IS AN ESTIMATED BILL</div>
                    <div class="sogs-text" >
                        I/We jointly and severally authorize Tri-State Cremation Society remove and prepare the above-named deceased, and do agree to become responsible for the payment of the above funeral expenses, and also agree that any additional items ordered become part of this agreement. The liability hereby assumed is in addition to the liability imposed by law upon the estate and others. A FINANCE CHARGE at the rate of one and a half percent (1.5%) per month (ANNUAL PERCENTAGE RATE OF 18%) will be added to the unpaid balance after 30 days from date of death.
                    </div>
                    <div class="sogs-text" >
                        Payment Terms: <div style="width:240px;" class="input">&nbsp;</div><br>
                            <div style="width:334px;" class="input">&nbsp;</div><br>
                                <div style="width:334px;" class="input">&nbsp;</div>
                                </div>
                                <div class="sogs-text" >
                                    Agreed and Accepted:<br>
                                        <div style="width:334px;" class="input">&nbsp;</div><br>
                                            dated: <div style="width:290px;" class="input">&nbsp;</div>
                                            </div>
                                            <div class="sogs-text" >
                                                Reason for Embalming: <div style="width:198px;" class="input">&nbsp;</div><br>
                                                    <div style="width:334px;" class="input">&nbsp;</div><br>
                                                        <div style="width:334px;" class="input">&nbsp;</div>
                                                        </div>
                                                        <div class="sogs-text" >
                                                            Accepted by: <div style="width:255px;" class="input">&nbsp;</div><br>
                                                                <span style="text-align:right">Tri-State Cremation Society</span>
                                                        </div>
                                                        <div class="sogs-text" >
                                                            dated:<div style="width:290px;" class="input">&nbsp;</div>
                                                        </div>
                                                        <div class="sogs-text" >
                                                            In accordance with the Federal Trade Commission, Trade Regulation Rule for the Funeral Industry Practice, Tri-State Cremation Society makes the following statements related to Funeral Goods and Services Selected.
                                                        </div>

                                                        <div class="sogs-text-box" >
                                                            <div class="sogs-text-box-number">1.</div>
                                                            <div class="sogs-text-box-details">Charges for only those items that are selected or that are required. If we are required by law or by a cemetery or crematory to use any Items, we will explain the reason(s) in writing below.
                                                            </div>
                                                        </div>

                                                        <div class="sogs-text-box" >
                                                            <div class="sogs-text-box-number">2.</div>
                                                            <div class="sogs-text-box-details">If you selected a funeral that may require embalming, such as a funeral with a viewing, you may have to pay for embalming. You do not have to pay for embalming you did not approve if you selected arrangements such as a direct cremation or immediate burial/ If we charged for embalming, we will explain why below.
                                                            </div>
                                                        </div>

                                                        <div class="sogs-text-box" >
                                                            <div class="sogs-text-box-number">3.</div>
                                                            <div class="sogs-text-box-details">If any cemetery or crematory regulations have required the purchase of any items, the regulation is explained below.
                                                            </div>
                                                        </div>

                                                        <div class="sogs-text-box" >
                                                            <div class="sogs-text-box-number">&nbsp;</div>
                                                            <div class="sogs-text-box-details">Explanations: </div>
                                                        </div>

                                                        <div class="sogs-text-box" >
                                                            <div class="sogs-text-box-number">&nbsp;</div>
                                                            <div class="sogs-text-box-details">
                                                                <div class="sogs-text-box" >
                                                                    <div class="sogs-text-box-number">1.</div>
                                                                    <div class="sogs-text-box-details">General <div style="width:230px;" class="input">&nbsp;</div></div>
                                                                </div>
                                                                <div class="sogs-text-box" >
                                                                    <div class="sogs-text-box-number">2.</div>
                                                                    <div class="sogs-text-box-details">
                                                                        <div style="width:275px;" class="input">&nbsp;</div><br>
                                                                            <div style="width:275px;" class="input">&nbsp;</div>
                                                                    </div>
                                                                </div>
                                                                <div class="sogs-text-box" >
                                                                    <div class="sogs-text-box-number">3.</div>
                                                                    <div class="sogs-text-box-details">Reasons for Embalming<br>
                                                                            <div class="sogs-text-box" >
                                                                                <div class="sogs-text-box-number">a.</div>
                                                                                <div class="sogs-text-box-details-int">Board of Health Regulation</div>
                                                                            </div>
                                                                            <div class="sogs-text-box" >
                                                                                <div class="sogs-text-box-number">b.</div>
                                                                                <div class="sogs-text-box-details-int">Viewing</div>
                                                                            </div>
                                                                            <div class="sogs-text-box" >
                                                                                <div class="sogs-text-box-number">c.</div>
                                                                                <div class="sogs-text-box-details-int">Both A & B</div>
                                                                            </div>

                                                                    </div>
                                                                </div>
                                                                <div class="sogs-text-box" >
                                                                    <div class="sogs-text-box-number">4.</div>
                                                                    <div class="sogs-text-box-details">Cemetery regulations require use of outside receptacle or crematory regulations require use of container.</div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        </div>
                                                        <br style="clear:both;">
                                                            <center class="noPrint"><br><input type="button" value="Print" onclick="window.print();" class="noPrintVersion"><br><br></center>

                                                                                </div>
                                                                                </div>

                                                                                </body>
                                                                                </html>