<?= $header ?>
<form method="post" id="mainform" name="mainform" style="margin:0;">
    <input type="hidden" name="scriptaction" 	id="scriptaction"	value="validate" />
    <input type="hidden" name="notvalidate" 	id="notvalidate"	value="" />
    <input type="hidden" name="next_form" 		id="next_form"		value="<?= $next_form ?>" />
    <!--Data-->
    <div id="ea">
        <?php
        $this->load->view('message_badge');

        if ($stepkey === 'authstd') {
            ?>
            <div class="ea-step-title-box">
                <h1 class="ea-step-title">Cremation Authorization Form</h1>
            </div>
            <?php
        } else {
            ?>
            <div class="ea-step-title-box">
                <h1 class="ea-step-title">step 4:  PAPERWORK</h1>
            </div>
            <?php
        }
        ?>


        <div id="content-ea" >
            <div id="ea-content-inner">

                <div class="form-title">Cremation Authorization Form</div>

                <div class="form-box">
                    <div class="form-sub-title">AUTHORIZATION FOR CREMATION AND DISPOSITION</div>
                    <div class="form-text-content">
                        I(we) the undersigned the "Authorizing Agent(s)" hereby authorize and request Brandywine Valley Cremation Care (hereinafter known as B.V.C.C.), in accordance with and subject to its rules and regulations, and any state and local laws and regulations to cremate the human remains of
                        <?php $fieldname = "decedent_name"; ?>
                        <input size="30" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" class="validate[required]"> (the "decedent"), and certifies that he or she has the right to make such authorization. I understand that all jewelry and valuable material, including dental gold, if not removed from the deceased prior to cremation, if not destroyed by the cremation process, will be disposed of by B.V.C.C.
                    </div>

                </div>

                <div class="form-box">
                    <div class="form-sub-title">PACEMAKERS, PROSTHESES, SILICON AND RADIOACTIVE IMPLANTS ALL PACEMAKERS AND RADIOACTIVE IMPLANTS/SEEDS MAY BE DANGEROUS WHEN PLACED IN A CREMATION CHAMBER AND MUST BE REMOVED PRIOR TO DELIVERING THE DECEDENT TO B.V.C.C.</div>
                    <div class="form-text-content" style="text-align:center;"><em><strong>Please initial ONE of the next two paragraphs; (for prearrangement, answer as to current status)</strong></em></div>
                    <div class="form-text-content">The decedent's remains <strong>DO NOT</strong> contain a pacemaker, radioactive implant or any other device that could be harmful to the crematory. THEY ARE SAFE TO CREMATE.</div>
                    <?php $fieldname = "init_1"; ?>
                    <div class="form-text-content-initial">INITIALS OF AUTHORIZING AGENT <input name="<?= $fieldname ?>" id="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text"  size="10" class="validate[required]"/></div>
                    <div class="form-text-content">
                        The decedent's remains DO contain a pacemaker, or radioactive implant. By my initials below I hereby grant B.V.C.C. authority to surgically remove or, in the case of radioactive implants, cause to be removed by a competent medical provider. I hereby agree to indemnify B.V.C.C. for all charges thus incurred.
                    </div>
                    <?php $fieldname = "init_2"; ?>
                    <div class="form-text-content-initial">INITIALS OF AUTHORIZING AGENT <input name="<?= $fieldname ?>" id="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text"  size="10" class="validate[required]"/></div>

                    <?php $fieldname = "init_2_items"; ?>
                    <div class="form-text-content">
                        The following list contains all existing devices (including all mechanical, radioactive implants and prosthetic devices) which are implanted in or attached to the decedent that should be removed prior to cremation:<br>
                        <input name="<?= $fieldname ?>" id="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text"  size="60" class=""/>

                    </div>

                </div>

                <div class="form-box">
                    <div class="form-sub-title">FINAL DISPOSITION</div>
                    <div class="form-text-content">After the cremation has taken place, the cremated remains have been processed and the processed cremated remains placed in the designated receptacle, B.V.C.C. will arrange for the disposition of the cremated remains as follows, and the Authorizing Agent(s) here authorize B.V.C.C. to release, deliver, transport, or ship the cremated remains as specified. Check one of the following:<br>
                    </div>
                    <div class="form-final-disposition-box">
                        <div class="form-final-disposition-text">Deliver the cremated remains to the U.S. Postal Service for shipment by Registered Return Receipt mail to: <br>
                            <div class="form-final-disposition-deliver-box">
                                <div class="form-final-disposition-deliver-lable">Address:</div>
                                <div class="form-final-disposition-deliver-field">
                                    <?php $fieldname = "fd_addres"; ?>
                                    <input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text"  size="10" class="validate[required]"/><br>
                                    City:<br/>
                                    <?php $fieldname = "fd_city"; ?>
                                    <input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text"  size="10" class="validate[required]"/><br>
                                    State:<br>
                                    <?php $fieldname = "fd_state"; ?>
                                    <input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text"  size="10" class="validate[required]"/><br>
                                    Zip Code:<br>
                                    <?php $fieldname = "fd_zip"; ?>
                                    <input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text"  size="10" class="validate[required]"/>
                                </div>
                            </div>
                            <div class="form-final-disposition-deliver-box">
                                <div class="form-final-disposition-deliver-lable">Other specific instructions:</div>
                                <?php $fieldname = "fd_other_instruction"; ?>
                                <div class="form-final-disposition-deliver-field"><textarea name="<?= $fieldname ?>" cols="20" rows="5" class=""><?= @$user_session[$fieldprefix][$fieldname] ?></textarea></div>
                            </div>

                        </div>
                    </div>
                    <div class="form-final-disposition-text"><em>If option five is selected, then I(we) agree to assume aIl liability that may arise from such shipment; and to indemnify and hold B.V.C.C. harmless from any and aIl claims that may arise from such shipment.</em></div>
                    <?php $fieldname = "init_3"; ?>
                    <div class="form-text-content-initial">INITIALS OF AUTHORIZING AGENT <input name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" type="text"  size="10" class="validate[required]"/></div>
                </div>

                <div class="form-box">
                    <div class="form-sub-title">AUTHORITY OF AUTHORIZING AGENT</div>
                    <div class="form-text-content">
                        I(we), the undersigned, hereby certify that I am the closest living next of kin of the decedent and that I am related to the decedent as his/her
                        <?php $fieldname = "aa_hisher"; ?>
                        <input size="30" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" class="validate[groupRequired[authagant]]" >, or that I otherwise serve (served) in the capacity of
                        <?php $fieldname = "aa_capacityof"; ?>
                        <input size="30" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" class="validate[groupRequired[authagant]]" >  to the decedent, that I have charge of the remains of the decedent and as such possess full legal authority and power, according the laws of the state of
                        <?php $fieldname = "aa_stateof"; ?>
                        <input size="30" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" class="validate[required]" >, to execute the authorization form and to arrange for the cremation and disposition of the cremated remains of the decedent. In addition, I am aware of no objection to this cremation by any spouse, child, parent, or sibling.
                    </div>

                </div>

                <div class="form-box">
                    <div class="form-sub-title">LIMITATION OF LIABILITY</div>
                    <div class="form-text-content">
                        As the Authorizing Agent(s), I(we) hereby agree to indemnify, defend, and hold harmless B.V.C.C., its officers, agents and employees, of and from any and all claims, demands, causes of action, and suits of every kind, nature and description, in law or equity, including any legal fees, costs and expenses of litigation, arising as a result of, based upon or connected with this authorization, including the failure to properly identify the decedent or the human remains transmitted to B.V.C.C., the processing, shipping and final disposition of the decedent's cremated remains, the failure to take possession of or make proper arrangements for the final disposition of the cremated remains, any damage due to harmful or explodable implants, claims brought by any other person(s) claiming the right to control the disposition of the decedent or the decedent's cremated remains, or any other action performed by B.V.C.C., its officers, agents, or employees, pursuant to this authorization, excepting only acts of willful negligence.
                    </div>
                    <?php $fieldname = "init_4"; ?>
                    <div class="form-text-content-initial">INITIALS OF AUTHORIZING AGENT <input type="text" size="10" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" class="validate[required]" /></div>

                </div>

                <div class="form-box">
                    <div class="form-sub-title">SIGNATURE OF AUTHORIZING AGENT(S)</div>
                    <div class="form-text-content">
                        THIS IS A LEGAL DOCUMENT, IT CONTAINS IMPORTANT PROVISIONS CONCERNING CREMATION. CREMATION IS IRREVERSIBLE AND FINAL. READ THIS DOCUMENT CAREFULLY BEFORE SIGNING.
                    </div>
                    <div class="form-text-content">
                        By executing this Cremation Authorization Form, as Authorizing Agent(s), the undersigned warrant that all representations and statements contained on this form are true and correct, that these statements were made to induce B.V.C.C. to cremate the human remains of the decedent, and that the undersigned have read and understand the provisions contained on this form and the document entitled "<a class="policies" href="/themes/directcrem/elements/ea/step4-2.php#policies">B.V.C.C. POLICIES, PROCEDURES AND REQUIREMENTS</a>," and hereby authorize B.V.C.C. to perform the cremation of the decedent in accordance with that document.
                    </div>
                    <div class="form-inner-cremation-box">
                        <div class="form-inner-cremaion-lable">Executed at: </div>
                        <div class="form-inner-cremation-field">
                            <?php $fieldname = "sa_executedat"; ?>
                            <select size="1" name="<?= $fieldname ?>" class="idleField validate[required]" style="width:170px;">
                                <?php $sel = @array($user_session[$fieldprefix][$fieldname] => 'selected'); ?>
                                <option value="">Choose</option>
                                <option value="Home" <?= @$sel['Home'] ?>>Home</option>
                                <option value="Place of Work" <?= @$sel['Place of Work'] ?>>Place of Work</option>
                                <option value="Attorney`s Office" <?= @$sel['Attorney`s Office'] ?>>Attorney`s Office</option>
                                <option value="Notary Office" <?= @$sel['Notary Office'] ?>>Notary Office</option>
                                <option value="Funeral Home" <?= @$sel['Funeral Home'] ?>>Funeral Home</option>
                                <option value="Other" <?= @$sel['Other'] ?>>Other</option>
                            </select>

                            <br>
                            <?php $fieldname = "sa_this"; ?>
                            this <input type="text"  size="1" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" class="validate[required]"/>
                            <?php $fieldname = "sa_dayof_month"; ?>
                            day of, <input type="text"  size="8" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" class="validate[required]"/>,
                            <?php $fieldname = "sa_dayof_year"; ?>
                            <input type="text"  size="2" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" class="validate[required]"/>
                        </div>
                    </div>
                    <div class="form-inner-cremation-box">
                        <div class="form-inner-cremaion-lable">Enter name: </div>
                        <?php $fieldname = "sa_name"; ?>
                        <div class="form-inner-cremation-field"><input type="text" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" class="validate[required]"/></div>
                    </div>
                    <div class="form-inner-cremation-box">
                        <div class="form-inner-cremaion-lable">Enter name again: </div>
                        <?php $fieldname = "sa_name_again"; ?>
                        <div class="form-inner-cremation-field"><input type="text" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" class="validate[required]"/></div>
                    </div>
                    <div class="form-inner-cremation-box">
                        <div class="form-inner-cremaion-lable">Relationship to Decedent: </div>
                        <?php $fieldname = "sa_relationship"; ?>
                        <div class="form-inner-cremation-field"><input type="text" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" class="validate[required]"/></div>
                    </div>
                    <div class="form-inner-cremation-box">
                        <div class="form-inner-cremaion-lable">E-mail: </div>
                        <?php $fieldname = "sa_email"; ?>
                        <div class="form-inner-cremation-field"><input type="text" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" class="validate[required]" data-value="kapom@turkenterprisesinc.com"/></div>
                    </div>
                    <div class="form-inner-cremation-box">
                        <div class="form-inner-cremaion-lable">Phone number: </div>
                        <?php $fieldname = "sa_phone"; ?>
                        <div class="form-inner-cremation-field"><input type="text" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" class="validate[required]"/></div>
                    </div>
                    <div class="form-inner-cremation-box">
                        <div class="form-inner-cremaion-lable">Address: </div>
                        <div class="form-inner-cremation-field">
                            <?php $fieldname = "sa_address"; ?>
                            <input type="text" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" class="validate[required]"/><br>
                            City:<br>
                            <?php $fieldname = "sa_city"; ?>
                            <input type="text" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" class="validate[required]"/><br>
                            State:<br>
                            <?php $fieldname = "sa_state"; ?>
                            <input type="text" size="5" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" class="validate[required]"/><br>
                            Zip Code:<br>
                            <?php $fieldname = "sa_zip"; ?>
                            <input type="text" size="5" name="<?= $fieldname ?>" value="<?= @$user_session[$fieldprefix][$fieldname] ?>" class="validate[required]"/><br>
                        </div>
                    </div>

                </div>

            </div>

            <?php
            $this->load->view('_right_bar');
            if ($isAdmin) {
                ?>
                <center class="noPrint"><input type="button" value="Print" onclick="window.print();" class="noPrintVersion"><br><br></center>
                <?php
            }
            ?>

        </div>

        <div style="visibility:hidden; position:absolute; left:-2000px; top:-2000px;">

            <div id="policies">
                <div class="ea-additional-option-lightbox-box-inner">
                    <div class="ea-additional-option-lightbox-box-title">B.V.C.C. POLICIES, PROCEDURES AND REQUIREMENTS</div>
                    <div class="ea-additional-option-lightbox-box-text">The cremation, processing and disposition of the remains of the deceased shall be performed in accordance with all governing laws, and the policies, procedures and requirements of B.V.C.C. and the designated funeral home. This document describes many of the policies and requirements of B.V.C.C. and is incorporated in our Cremation Authorization Form. We suggest you take the time to read this document carefully before executing the Cremation Authorization Form.
                    </div>

                    <div class="ea-additional-option-lightbox-box-title">B.V.C.C. REQUIREMENTS FOR CREMATION</div>
                    <div class="ea-additional-option-lightbox-box-text">
                        Cremation will take place only after all the following conditions have been met.<br>
                        1.  Any scheduled ceremonies or viewings have been completed.<br>
                        2.  Civil and medical authorities have issued all required permits.<br>
                        3.  All necessary authorizations have been obtained, and no objections have been raised.
                    </div>

                    <div class="ea-additional-option-lightbox-box-title">CASKETS/CONTAINERS</div>
                    <div class="ea-additional-option-lightbox-box-text">B.V.C.C. does not accept metal caskets. In the interest of providing appropriate sanitation for staff, and respect for the deceased, all wooden caskets and alternative containers must meet the following standard; (1) be composed of materials suitable for cremation; (2) be able to be closed to provide a complete covering of the human remains; (3) be sufficient for handling with ease; and (4) be able to provide protection for the health and safety of crematory personnel.
                    </div>
                    <div class="ea-additional-option-lightbox-box-text">Many caskets that are comprised primarily of combustible material also contain some exterior parts, e.g., decorative handles or rails, that are not combustible and that may cause damage to the cremation equipment. B.V.C.C., at its sole discretion, reserves the right to remove these non-combustible materials prior to cremation and to discard them with similar materials from other cremations ans other refuse in a non-recoverable manner.
                    </div>

                    <div class="ea-additional-option-lightbox-box-title">PACEMAKERS, PROSTHESES AND RADIOACTIVE DEVICES</div>
                    <div class="ea-additional-option-lightbox-box-text">
                        Pacemakers and prosthesis, as well as any other mechanical or radioactive devices or implants in the decedent, may create a hazardous condition when placed in the cremation chamber. It is imperative that pacemakers and radioactive devices be removed prior to cremation. If the funeral home is not notified about such devices and implants, and not instructed to remove them, then the person(s) authorizing the cremation will be responsible for any damages caused to B.V.C.C. or crematory personnel by such devices or implants.
                    </div>

                    <div class="ea-additional-option-lightbox-box-title">THE CREMATION PROCESS</div>
                    <div class="ea-additional-option-lightbox-box-text">All cremations are performed individually. Exceptions are only made in the case of close relatives, and then only with the prior express written instructions of the Authorizing Agent(s).
                    </div>
                    <div class="ea-additional-option-lightbox-box-text">The cremation of a human being is a process whereby the body is prepared for its ultimate and final disposition by earth burial, entombment, placement into a niche or in a garden within a cemetery,  burial at sea, or holding and safekeeping by a family member or their designated representative
                    </div>
                    <div class="ea-additional-option-lightbox-box-text">The body is placed into a cremation casket or rigid container. This container with the body enclosed in it is placed inside a cremation chamber. The body is totally consumed (incinerated) by open flames and intense heat. The temperature will generally range between 1500 to 2200 degrees Fahrenheit. The soft tissues of the body are vaporized. The skeletal framework is reduced to bone fragments and particles (not ashes). These fragments and particles are called cremated remains. The process of cremation may take from three (3) to five (5) hours. During the cremation, the contents of the chamber may be moved to facilitate incineration. The chamber is composed of ceramic or other material which disintegrates slightly during each cremation. The product of that disintegration is commingled with the cremated remains.
                    </div>
                    <div class="ea-additional-option-lightbox-box-text">The cremated remains are collected from the cremation chamber by using technical and speciallzed equipment. Among these are a hoe, brooms and brushes, and a receiving pan. This equipment is solely used for the collection of cremated remains. The remains are cleaned of any metal or other foreign matter. The bone particles removed vary in size and need to be processed. This is done by pulverization in a specialized machine used only for this purpose, to render them to a size for placement in an urn.
                    </div>
                    <div class="ea-additional-option-lightbox-box-text">It is beyond anyone's capability to conserve or to collect every particle of crematied remains and dust. Inadvertent and unintentional commingling of cremated remains may occur. Due to the nature of the cremation process any personal possessions or valuable materials, such as dental gold or jewelry (as well as amy body prosthesis or dental bridgework), that are left with the decedent and not removed from the casket or container prior to cremation will be destroyed or if not destroyed, will be disposed of by B.V.C.C.
                    </div>
                    <div class="ea-additional-option-lightbox-box-text">Following the cooling period, the cremated remains, which will normally weigh several pounds in the case of an average size adult, are then swept from the cremation chamber. After the cremated remains are removed from the cremation chamber, all non-combustible materials (insofar as possible) such as bridgework, and materials from the casket or container, such as hinges, latches, nails, etc., will be separated and removed from the human bone fragments by visible or magnetic selection and will be disposed of by B.V.C.C. with similar materials from other cremations in a non-recoverable manner.
                    </div>
                    <div class="ea-additional-option-lightbox-box-text">When the cremated remains are removed from the cremation chamber, the skeletal reamins often contain recognizable bone fragments. After the bone fragments have been separated from the other material, they will then be mechanically processed (pulverized).  This process of crushing may cause incidental commingling of the remains with the residue from the processing of previously cremated remains. These granulated particles of unidentifiable dimensions will be virtually unrecognizable as human remains.
                    </div>

                    <div class="ea-additional-option-lightbox-box-title">URNS/CONTAINERS</div>
                    <div class="ea-additional-option-lightbox-box-text">After the cremated remains have been processed, they will be placed in the designated urn or container. B.V.C.C. will make a reasonable effort to put all of the cremated remains in the urn or container, with the exception of dust or other residue that may remain on the processing equipment. In the event the client provided urn or container is insufficient to accommodate all of the cremated remains, the excess will be placed in a separate receptacle. The separate receptacle will be kept with the primary receptacle and handled according to the disposition instructions on the Cremation Authorization Form.
                    </div>

                    <div class="ea-additional-option-lightbox-box-title">FINAL DISPOSITION</div>
                    <div class="ea-additional-option-lightbox-box-text">Cremation is NOT final disposition, nor is placing the cremated remains in storage at a funeral home final disposition. The cremation process simply reduces the decedent's body to cremated remains. Some provision must be made for the final disposition of these cremated remains. Therefore, B.V.C.C. strongly suggests that arrangements for final disposition be made at the time the cremation arrangements are made and that the Cremation Authorization Form is completed.
                    </div>

                    <div class="ea-additional-option-lightbox-box-title">LIMITATION OF LIABILITY</div>
                    <div class="ea-additional-option-lightbox-box-text">The obligations of B.V.C.C. shall be limited to the cremation of the decedent and the disposition of the decedent's cremated remains as authorized on the Cremation Authorization Form. No Warranties, Express or Implied are made, and damages shall be limited to the amount of the cremation fee paid.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    if ($isAdmin) {
        ?><script src="<?= $cfg['root'] ?>/assets/js/form_auth_print.js"></script><?php
    } else {
        ?><script src="<?= $cfg['root'] ?>/assets/js/form_auth.js"></script><?php
    }
    ?>
    <!--Data-->
</form>
<?=
$footer?>