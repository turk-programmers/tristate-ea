<div class="button-link noPrint">
	<?php
	if($stepkey == 'thankyou'){
		?>
		<div class="btnborder noAdmin noPrint"><button type="button" title="Home" class="btn btn-ope btn-home" style="display:none;">home</button></div>
		<noscript>
			<a title="Home" href="http://www.utahcremationsociety.com/" class="noAdmin">home</a>
		</noscript>
		<?php
	}else{
		if(@$prev_form != ''){
			?>
			<div class="btnborder noAdmin noPrint"><button type="button" title="Previous step" data-nextform='<?=$cfg['root'].'/'.$prev_form?>' class="btn btn-ope btn-prev" style="display:none;">back</button></div>
			<noscript>
				<a title="Previous step" href="<?=$cfg['root'].'/'.$prev_form?>" class="noAdmin">back</a>
			</noscript>
			<?php
		}
		?>
		<div class="btnborder noAdmin noPrint btn-wait"><button type="button" title="Please wait..." class="btn btn-ope"><i class="fa fa-refresh fa-spin"></i></button></div>
		<?php
		if($stepkey == 'payment'){
			?>
			<div class="btnborder noAdmin noPrint"><button type="submit" title="Submit <?=@$user_session['isbam'] ? 'membership' : 'arrangement'?>" class="btn btn-ope btn-next btn-submitea" onclick="$('#mainform').submit(); return false;">submit <?=@$user_session['isbam'] ? 'membership' : 'arrangement'?></button></div>
			<?php
		}else{
			?>
			<div class="btnborder noAdmin noPrint btn-next"><button type="submit" title="Next step" class="btn btn-ope btn-next" onclick="$('#mainform').submit(); return false;">next</button></div>
			<?php
		}
	}
	?>
	<div class="btnborder noPrint adminOnly btn-next"><button type="button" title="Print" class="btn btn-ope btn-next" onclick="window.print();">print</button>
</div>
<script src="<?=$cfg['root']?>/assets/js/btn_operate.js"></script></div>