<?= $header ?>
<form method="post" id="mainform" name="mainform" style="margin:0;">
    <input type="hidden" name="scriptaction" 	id="scriptaction"	value="validate" />
    <input type="hidden" name="notvalidate" 	id="notvalidate"	value="" />
    <input type="hidden" name="next_form" 		id="next_form"		value="<?= $next_form ?>" />
    <!--Data-->

    <input type="hidden" name="pkgid"	 		id="pkgid"			value="<?= $user_session['packageselected']['id'] ?>" />


    <style>

        .itemDesc {
            text-align: center;
            width: 550px;
        }
        .itemDesc > img {
            max-width: 530px;
        }
        .itemDesc > .descContent {
            margin: 20px auto 0;
            text-align: left;
            width: 500px;
        }
        .product-box.hideItem {
            display: none;
        }
        /*
        Colorbox force fix CSS
        */
        #colorbox, #cboxOverlay, #cboxWrapper {
            overflow: visible !important;
        }
        #cboxTitle {
            background-color: rgba(0, 0, 0, 0.65);
            border-radius: 5px;
            color: white;
            float: none !important;
            margin-top: 40px;
            padding: 10px;
            position: relative !important;
            width: auto;
        }
        #cboxLoadedContent {
            margin-bottom: 0px !important;
            padding: 30px 60px;
        }
        #cboxContent {
            overflow: visible !important;
        }
    </style>
    <div id="ea" class="">
        <?php
        $this->load->view('message_badge');
        ?>

        <img class="img_opt_no" src="<?= $root ?>/assets/images/btn-choose-merch-no.png" style="display:none;">
        <img class="img_opt_yes" src="<?= $root ?>/assets/images/btn-choose-merch-yes.png" style="display:none;">

        <div class="ea-step-title-box">
            <h1 class="ea-step-title">step 2: Select MERCHANDISE</h1>
        </div>
        <div id="content-ea" >
            <div id="ea-content-inner">

                <div class="providing-cremation-urn-box" style="display:none;">
                    <div class="providing-cremation-urn-text">Will you be providing the cremation urn?</div>
                    <div class="providing-cremation-urn-select"><input class="checkboximage2" name="providingurn" type="checkbox" data-rel-detail-tmp="#if-yes-providing" /></div>
                </div>
                <div class="if-yes-providing-cremation-urn-box" id="if-yes-providing"  style="display:none;">
                    <div class="if-yes-providing-cremation-urn-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
                    </div>
                </div>

                <?php
                foreach ($merch_types as $typekey => $type) {
                    ?>
                    <div class="merchandise-box <?= $typekey ?>" data-type="<?= $typekey ?>">
                        <?php
                        if ($typekey == 'urn') {
                            switch ($user_session['packageselected']['id']) {
                                case 2:
                                    ?>
                                    <div>You've chosen the Gold Package, which includes your choice of an urn. Please choose from one of the urns below. You may purchase additional items once your included item is selected.</div>
                                    <?php
                                    break;

                                case 3:
                                    ?>
                                    <div>You've chosen the Silver Package, which includes a wood urn. Please choose one below. You may purchase additional items once your included item is selected.</div>
                                    <?php
                                    break;

                                case 4:
                                    ?>
                                    <div>You've chosen the Bronze Package, which includes a <a href="https://fnetclients.s3.amazonaws.com/mastercart/clients/tristate/pictures/Urn/69_image.jpg" title="Basic Urn (Black Acrylic)" class="cboxElement">Basic Urn<br>(Black Acrylic)</a>. You may select additional items for purchase below.</div>
                                    <?php
                                    break;
                            }
                        } elseif ($typekey == 'keepsake') {
                            switch ($user_session['packageselected']['id']) {
                                case 2:
                                    ?>
                                    <div>A keepsake urn is included in the Gold Package selection. Please choose a keepsake below. You may purchase additional items once your included item is selected.</div>
                                    <?php
                                    break;

                                case 3:
                                    ?>
                                    <div>A wood keepsake urn is included in the Silver Package selection. Please choose a wood keepsake below. You may purchase additional items once your included item is selected.</div>
                                    <?php
                                    break;
                            }
                        } elseif ($typekey == 'flower') {
                            switch ($user_session['packageselected']['id']) {
                                case 2:
                                    ?>
                                    <div>A floral arrangement or plant is included in the Gold Package selection. Please choose an arrangement below. You may purchase additional items once your included item is selected.</div>
                                    <?php
                                    break;
                            }
                        } elseif ($typekey == 'memorial') {
                            switch ($user_session['packageselected']['id']) {
                                case 2:
                                    ?>
                                    <div>Stationary is included in the Gold Package selection. Please choose your stationary theme below. You may purchase additional items once your included item is selected.</div>
                                    <?php
                                    break;

                                case 3:
                                    ?>
                                    <div>Stationary is included in the Silver Package selection. Please choose your stationary theme below. You may purchase additional items once your included item is selected.</div>
                                    <?php
                                    break;
                            }
                        }
                        ?>
                        <div class="merchandise-title"><div class="message_pointer"></div><span><?= $type['name'] ?></span></div>
                        <?php
                        foreach ($type['items'] as $subtypekey => $subtype) {
                            ?>
                            <div class="product-area <?= $subtypekey ?> <?= @$show ?> show" data-subtype="<?= $subtypekey ?>">
                                <?php
                                foreach ($subtype as $id => $item) {
                                    $hide = '';
                                    if (@$user_session['packageselected']['id'] == 3) {
                                        if ($typekey == 'urn' or $typekey == 'keepsake') {
                                            if (!in_array($id, array(24, 25, 26, 27))) {
                                                $hide = 'hideItem';
                                            }
                                        }
                                    }
                                    ?>
                                    <div class="product-box <?= @$hide ?>">
                                        <div class="big-border">
                                            <?php
                                            if (strip_tags($item['desc1'])) {
                                                ?>
                                                <div class="product-img hasDesc"><img title='<?= str_replace('\'', '`', $item['desc1']) ?>' src="<?= $item['thumb_url'] ?>" href="<?= $item['image_url'] ?>"/></div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="product-img"><img title='<?= str_replace('\'', '`', $item['name']) ?>' src="<?= $item['thumb_url'] ?>" href="<?= $item['image_url'] ?>"/></div>
                                                <?php
                                            }
                                            ?>
                                            <div class="product-name"><?= $item['name'] ?></div>
                                            <div class="product-prices" data-price="<?= $item['price'] ?>"><?= $item['price'] ?></div>
                                            <div class="product-quality">QTY:
                                                <span class="product-quality-select">
                                                    <select class="select-quan quan_<?= $id ?>" data-id="<?= $id ?>" data-type="<?= $typekey ?>">
                                                        <?php
                                                        for ($q = 0; $q <= 5; $q++) {
                                                            ?><option value="<?= $q ?>"><?= $q ?></option><?php
                                                        }
                                                        ?>
                                                    </select>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                /* if($typekey == 'keepsake' and $subtypekey != 'Jewelry' and 0){
                                  ?>
                                  <div class="venderlink">
                                  <?php
                                  if($subtypekey == 'Thumbies'){
                                  ?><a target="_blank" href="https://meadowhillco.jsp-servlet.net/">Visit the Thumbies Website for more selection.</a><?php
                                  }else{
                                  ?><a target="_blank" href="http://www.memoryglass.com/products.asp">Visit the Memory Glass Website for more selection.</a><?php
                                  }
                                  ?>
                                  <br>
                                  <br>
                                  Is there an item you would like to purchase that isn't listed in our selection? If so, enter it here:<br>
                                  <input class="lookingitem <?=$subtypekey?>" data-subtype="<?=$subtypekey?>"></input>
                                  </div>
                                  <?php
                                  } */
                                ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
            <?php
            $this->load->view('_right_bar');
            ?>

        </div>
    </div>
    <div style="display: none;">
        <div class="county-selection-balloon" style="width: 600px; font-size: 14px; padding: 5px;">Keepsakes provide a method of memorializing cremated remains in smaller urns, or micro urn jewelry*.<br><br>
            <i>*We will delicately take care to fill if ordered</i>.
        </div>
    </div>
    <script>
        var flower_credit = <?= @$user_session['packageselected']['flower_credit'] ? $user_session['packageselected']['flower_credit'] : 0 ?>;
    </script>
    <script src="<?= $cfg['root'] ?>/assets/js/merchandise.js"></script>
    <script>
        $(function() {
            $('.merchandise-box[data-type="keepsake"] .merchandise-title > span').balloon({
                contents: $('.county-selection-balloon').clone(),
                offsetX: -100,
                position: 'top right',
                css: {
                    opacity: "1"
                }
            });
<?php
if (is_array(@$user_session['merch_more_item_looking'])) {
    foreach ($user_session['merch_more_item_looking'] as $skey => $val) {
        ?>
                    $('.lookingitem[data-subtype="<?= $skey ?>"]').val('<?= $val ?>');
        <?php
    }
}
if (@$user_session['merch_providing_urn']) {
    ?>
                check_opt('input.checkboximage2');
    <?php
}
$cart = @$user_session['cart'];
if (is_array($cart) and count($cart)) {
    foreach ($cart as $type => $items) {
        foreach ($items as $id => $item) {
            ?>
                        chooseItem(<?= $id ?>,<?= $item['quan'] ?>);
            <?php
        }
    }
}

/*
 * For memorial item included
 */
if (is_array(@$user_session['serviceselected'][5]) and $user_session['packageselected']['id'] < 4) {
    $sets = array();
    foreach ($merch_types['memorial']['items']['memorial'] as $id => $item) {
        $sets[] = '\'' . $id . '\'';
    }
    ?>
                includeItemForceById(new Array(<?= implode(',', $sets) ?>), 61);
    <?php
} elseif (is_array(@$user_session['serviceselected'][5])) {
    ?>
                forceSelectItem(61);
    <?php
} elseif ($user_session['packageselected']['id'] < 4) {
    $sets = array();
    foreach ($merch_types['memorial']['items']['memorial'] as $id => $item) {
        $sets[] = '\'' . $id . '\'';
    }
    ?>
                //memorial for gold and silver
                includeItem(new Array(<?= implode(',', $sets) ?>));
    <?php
}

/*
 * For included item of GOLD package
 */
if ($user_session['packageselected']['id'] == 2) {
    ?>
                $('.merchandise-box.flower .merchandise-sub-title > a[data-subtype="gold_items"]').click();
                $('.merchandise-box.flower .merchandise-sub-title').hide();
    <?php
    $sets = array();
    foreach ($merch_types['urn']['items'] as $items) {
        foreach ($items as $id => $item) {
            $sets[] = '\'' . $id . '\'';
        }
    }
    ?>
                //urn for gold
                includeItem(new Array(<?= implode(',', $sets) ?>));
    <?php
    $sets = array();
    foreach ($merch_types['keepsake']['items']['all'] as $id => $item) {
        $sets[] = '\'' . $id . '\'';
        //foreach($items as $id=>$item){
        //}
    }
    ?>
                //keepsake for gold
                includeItem(new Array(<?= implode(',', $sets) ?>));
    <?php
    $sets = array();
    foreach ($merch_types['flower']['items'] as $items) {
        foreach ($items as $id => $item) {
            $sets[] = '\'' . $id . '\'';
        }
    }
    ?>
                //flower for gold
                includeItem(new Array(<?= implode(',', $sets) ?>));
    <?php
}

/*
 * For included item of SILVER package
 */
if ($user_session['packageselected']['id'] == 3) {
    /* $sets = array();
      foreach($merch_types['urn']['items'] as $items){
      foreach($items as $id=>$item){
      $sets[] = '\''.$id.'\'';
      }
      } */
    ?>
                //includeItemById(new Array(<?= implode(',', $sets) ?>), 37);

                //urn for silver
                includeItem(new Array(24, 26));
                $('.select-quan.quan_24,.select-quan.quan_26').change(function() {
                    var show = false;
                    var parent = $(this).parent().parent().parent().parent().parent();
                    $('.select-quan.quan_24,.select-quan.quan_26').each(function() {
                        if (parseInt($(this).val())) {
                            show = true;
                        }
                    });
                    if (show) {
                        $('.product-box.hideItem', parent).addClass('showItem').removeClass('hideItem');
                    } else {
                        $('.product-box.showItem', parent).addClass('hideItem').removeClass('showItem').find('.select-quan').each(function() {
                            $(this).val(0).change();
                        });
                    }
                }).change();

                //keepsake for silver
                includeItem(new Array(25, 27));
                $('.select-quan.quan_25,.select-quan.quan_27').change(function() {
                    var show = false;
                    var parent = $(this).parent().parent().parent().parent().parent();
                    $('.select-quan.quan_25,.select-quan.quan_27').each(function() {
                        if (parseInt($(this).val())) {
                            show = true;
                        }
                    });
                    if (show) {
                        $('.product-box.hideItem', parent).addClass('showItem').removeClass('hideItem');
                    } else {
                        $('.product-box.showItem', parent).addClass('hideItem').removeClass('showItem').find('.select-quan').each(function() {
                            $(this).val(0).change();
                        });
                    }
                }).change();
    <?php
}

/*
 * For included item of BRONZE package
 */
if ($user_session['packageselected']['id'] == 4) {
    $sets = array();
    foreach ($merch_types['urn']['items'] as $items) {
        foreach ($items as $id => $item) {
            $sets[] = '\'' . $id . '\'';
        }
    }
    ?>
                //urn for bronze
                includeItemById(new Array(<?= implode(',', $sets) ?>), 69);
    <?php
}
?>
            showSelectedSubtype();
        });
        $(window).load(function() {
<?php
if (@$gotosection) {
    ?>
                scrollTo('.merchandise-box.<?= $gotosection ?>');
    <?php
}
?>
        });
    </script>
    <!--Data-->
</form>
<?php
echo $footer?>