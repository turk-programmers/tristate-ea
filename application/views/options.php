<?=$header?>
<form method="post" id="mainform" name="mainform" style="margin:0;">
<input type="hidden" name="scriptaction" 	id="scriptaction"	value="validate" />
<input type="hidden" name="notvalidate" 	id="notvalidate"	value="" />
<input type="hidden" name="next_form" 		id="next_form"		value="<?=$next_form?>" />
<!--Data-->
<div id="ea">
    <div class="ea-content">
    	<div class="ea-title">ONLINE CREMATION ARRANGEMENTS</div>
    	<div class="ea-subtitle">Select Options</div>
    	<?php
    	foreach($services as $id=>$serv){
    		if($serv['grouping'] != 'checkbox') continue;
			
			$isHide = false;
			if(($user_session['packageselected']['id'] == 2 or $user_session['packageselected']['id'] == 3) and ($id == 6)){
				$isHide = true;
			}
			
    		$checked = false;
			if(@$user_session['serviceselected'][$id]){
				$checked = true;
			}
    		?>
			<div class="checkbox-option <?=($checked and !$isHide) ? 'checkbox-option-select' : ''?>" data-id="<?=$id?>" style="<?=$isHide ? 'display:none;' : ''?>">
				<?=$serv['name']?>
				<?php
				if($id == 4){
					echo "<b>(requires Embalming)</b>&nbsp;";
				}
				if($serv['price']){
					echo '$'.number_format($serv['price'],2);
				}
				if(!empty($serv['description'])){
					echo '<br>';
					echo $serv['description'];
				}
				?>
				<input type="hidden" name="service[<?=$id?>]" value="<?=($checked and !$isHide) ? $id : ''?>" data-id="<?=$id?>">
			</div>
    		<?php
    	}
    	?>
		<div class="select-space">
			<div class="select-option"><span class="select-option-text">Select Release Preference</span>	
				<select name="release" class="activeOnTheFly">
			    	<?php
			    	$sel = array(@$user_session['releasepreferenceselected']['id'] => 'selected');
			    	foreach($services as $id=>$serv){
			    		if($serv['grouping'] != 'release') continue;
						
						?>
						<option value="<?=$id?>" <?=@$sel[$id]?>><?=$serv['name']?> <?=$serv['price'] ? '$'.number_format($serv['price'],2) : 'No Charge'?></option>
						<?php
			    	}
			    	?>
				</select>
			</div>
			<div class="select-option"><span class="select-option-text">Select a weight</span>
				<select name="weight" class="activeOnTheFly">
			    	<?php
			    	$sel = array(@$user_session['weightselected']['id'] => 'selected');
			    	foreach($services as $id=>$serv){
			    		if($serv['grouping'] != 'weight') continue;
						?>
						<option value="<?=$id?>" <?=@$sel[$id]?>><?=$serv['name']?> <?=$serv['price'] ? '$'.number_format($serv['price'],2) : 'No Charge'?></option>
						<?php
			    	}
			    	?>
				</select>
			</div>
			<div class="select-option"><span class="select-option-text">Death Certificates</span>
				<select name="dcc" class="activeOnTheFly">
					<?php
					$sel = array(@$user_session['dcc']['quan'] => 'selected');
					for($i=1;$i<=30;$i++){
						?>
						<option value="<?=$i?>" <?=@$sel[$i]?>><?=$i?></option>
						<?php
					}
					?>
				</select> 
				<span style="font-size: 15px;font-weight:normal;">($<?=$settings['dcc_price']?> for first certificate, $<?=$settings['dcc_price_add']?> for each additional)</span>
			</div>
		</div>
    </div>
    <div class="ea-sidebar">
        <?php
        $this->load->view('_right_bar');
        ?>
    </div>
    <?php
    $this->load->view('_btn_operate');
    ?>
</div>
<script src="<?=$cfg['root']?>/assets/js/options.js"></script>
<!--Data-->
</form>
<?=$footer?>