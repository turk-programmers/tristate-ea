<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Cremation Authorization Form :: Tri-State Cremation Society</title>
        <link rel="stylesheet" type="text/css" href="#" />
        <style>
            /*
             * GLOBAL style
             */
            html {
                font-family: "Cordia New";
                font-size: 10px;
            }
            body {
                font-family: Cordia New !important;
                font-size: 1.7em;
                line-height: 0.7em;
                margin: 0;
            }
            h1 {
                font-size: 1.7em;
                line-height: 0.8em;
                margin: 0;
            }
            h2 {
                font-size: 1.2em;
                font-weight: normal;
                line-height: 0.75em;
                margin: 0;
            }
            h3 {
                font-size: 1em;
                line-height: 0.75em;
                margin: 0;
            }
            pre {
                font-family: courier new;
                font-size: 0.7em;
                line-height: 1em;
            }
            @media print{
                pre{
                    display: none;
                }
            }

            /*
             * SPECIFIC style
             */
            .content{
                width: 680px;
                margin: 0 auto;
                text-align: justify;
                page-break-after:always;
            }
            .content .mainTitle {
                margin: 10px 0 5px;
                text-align: center;
            }
            .content .title,
            .content .subTitle {
                margin: 5px 0;
                text-align: center;
            }
            .content .subTitle {
                font-style: italic;
                font-weight: bold;
            }
            .content .fieldValue {
                /*background-color: #FFA500;*/
                border-bottom: 1px solid #000000;
                box-sizing: border-box;
                display: inline-block;
                font-weight: bold;
                height: 0.7em;
                padding: 0 5px;
            }
            .content .initialRow {
                font-weight: bold;
                text-align: right;
            }
            .content .inline {
                display: inline-block;
                vertical-align: top;
            }
            .content2 span {
                display: block;
                margin: 5px 0;
            }
            /*
             * Field value width
             */
            <?php
            for ($i = 1; $i <= 100; $i++) {
                ?>
                .content .size-<?= $i ?> {
                    width: <?= $i ?>%;
                }
                <?php
            }

            $formauth = $user_session['formauth'];
            ?>
        </style>
    </head>

    <body>
        <div class="content">
            <center>
                <h1>Brandywine Valley Cremation Care</h1>
              <!--   <h2>7230 Lancaster Pike, Hockessin, DE 19707</h2> -->
                <h2>412 Philadelphia Pike, Wilmington, DE 19809</h2>
            </center>
            <div class="mainTitle">
                <h3>AUTHORIZATION FOR CREMATION AND DISPOSITION</h3>
            </div>
            <span>I(we) the undersigned the "Authorizing Agent(s)" hereby authorize and request Brandywine Valley Cremation Care (hereinafter known as B.V.C.C.), in accordance with and subject to its rules and regulations, and any state and local laws and regulations to cremate the human remains of</span><br>
            <div class="fieldValue size-100"><?= @$formauth['decedent_name'] ?>&nbsp;</div>
            <span>(the "decedent"), and certifies that he or she has the right to make such authorization. I understand that all jewelry and valuable material, including dental gold, if not removed from the deceased prior to cremation, if not destroyed by the cremation process, will be disposed of by B.V.C.C.</span>
            <div class="title">
                <h3>PACEMAKERS, PROSTHESES, SILICON AND RADIOACTIVE IMPLANTS ALL PACEMAKERS AND RADIOACTIVE IMPLANTS/SEEDS MAY BE DANGEROUS WHEN PLACED IN A CREMATION CHAMBER AND MUST BE REMOVED PRIOR TO DELIVERING THE DECEDENT TO B.V.C.C.</h3>
            </div>
            <div class="subTitle">
                <h3>Please initial ONE of the next two paragraphs; (for prearrangement, answer as to current status)</h3>
            </div>
            <span>The decedent's remains <b>DO NOT</b> contain a pacemaker, radioactive implant or any other device that could be harmful to the crematory. THEY ARE SAFE TO CREMATE.</span>
            <div class="initialRow">
                <span>INITIALS OF AUTHORIZING AGENT</span>
                <div class="fieldValue size-20"><?= @$formauth['init_1'] ?>&nbsp;</div>
            </div>
            <span>The decedent's remains <b>DO</b> contain a pacemaker, or radioactive implant. By my initials below I hereby grant B.V.C.C. authority to surgically remove or, in the case of radioactive implants, cause to be removed by a competent medical provider. I hereby agree to indemnify B.V.C.C. for all charges thus incurred. </span>
            <div class="initialRow">
                <span>INITIALS OF AUTHORIZING AGENT</span>
                <div class="fieldValue size-20"><?= @$formauth['init_2'] ?>&nbsp;</div>
            </div>
            <span>The following list contains all existing devices (including all mechanical, radioactive implants and prosthetic devices) which are implanted in or attached to the decedent that should be removed prior to cremation:</span>
            <div class="fieldValue size-37">&nbsp;</div>
            <div class="fieldValue size-100"><?= @$formauth['init_2_items'] ?>&nbsp;</div>
            <hr>
            <div class="title">
                <h3>FINAL DISPOSITION</h3>
            </div>
            <span>After the cremation has taken place, the cremated remains have been processed and the processed cremated remains
                placed in the designated receptacle, B.V.C.C. will arrange for the disposition of the cremated remains as follows,
                and the Authorizing Agent(s) here authorize B.V.C.C. to release, deliver, transport, or ship the cremated remains
                as specified.
                <!--Check one of the following:-->
            </span>
            <!--            <div>
                            <div class="inline size-10">
                                <span>1.</span>
                                <div class="fieldValue size-70">&nbsp;</div>
                            </div>
                            <div class="inline size-89">
                                <span>Deliver the cremated remains to</span>
                                <div class="fieldValue size-30">&nbsp;</div>
                                <span>by (date and time)</span>
                                <div class="fieldValue size-20">&nbsp;</div>
                            </div>
                        </div>
                        <div>
                            <div class="inline size-10">
                                <span>2.</span>
                                <div class="fieldValue size-70">&nbsp;</div>
                            </div>
                            <div class="inline size-89">
                                <span>Hold the cremated remains at Brandywine Valley Cremation Care.</span>
                                <span>(If the facility is to retain custody of the cremated remains for longer than 30days, Brandywine Valley Cremation Care requires the use of a permanent, non-combustible container.)</span>
                            </div>
                        </div>
                        <div>
                            <div class="inline size-10">
                                <span>3.</span>
                                <div class="fieldValue size-70">&nbsp;</div>
                            </div>
                            <div class="inline size-89">
                                <span>Release cremated remains to B.V.C.C. for burial at sea.</span>
                            </div>
                        </div>
                        <div>
                            <div class="inline size-10">
                                <span>4.</span>
                                <div class="fieldValue size-70">&nbsp;</div>
                            </div>
                            <div class="inline size-89">
                                <span>Release cremated remains to B.V.C.C. to consign to earth.</span>
                            </div>
                        </div>-->
            <div>
                <div class="inline size-10">
                    <!--<span>5.</span>-->
                    <div class="fieldValue size-70">&nbsp;&nbsp;&nbsp;X</div>
                </div>
                <div class="inline size-89">
                    <span>Deliver the cremated remains to the U.S. Postal Service for shipment by Registered Return Receipt mail to:</span>
                </div>
            </div>
            <div class="fieldValue size-100"><?= @$formauth['fd_addres'] ?> <?= @$formauth['fd_city'] ?> <?= @$formauth['fd_state'] ?> <?= @$formauth['fd_zip'] ?>&nbsp;</div>
            <span>(or other specific instructions)</span>
            <div class="fieldValue size-77"><?= @$formauth['fd_other_instruction'] ?>&nbsp;</div>
            <div class="fieldValue size-100">&nbsp;</div>
            <span><i>I(we) agree to assume all liability that may arise from such shipment; and to indemnify and hold B.V.C.C. harmless from any and all claims that may arise from such shipment.</i></span>
            <div class="initialRow">
                <span>INITIALS OF AUTHORIZING AGENT</span>
                <div class="fieldValue size-20"><?= @$formauth['init_3'] ?>&nbsp;</div>
            </div>
            <hr>
            <div class="title">
                <h3>AUTHORITY OF AUTHORIZING AGENT</h3>
            </div>
            <span>I(we), the undersigned, hereby certify that I am the closest living next of kin of the decedent and that I am related to the decedent as his/her</span>
            <div class="fieldValue size-20"><?= @$formauth['aa_hisher'] ?>&nbsp;</div>
            <span>, or that I otherwise serve (served) in the capacity of</span>
            <div class="fieldValue size-20"><?= @$formauth['aa_capacityof'] ?>&nbsp;</div>
            <span>to the decedent, that I have charge of the remains of the decedent and as such possess full legal authority and power, according the laws of the state of</span>
            <div class="fieldValue size-20"><?= @$formauth['aa_stateof'] ?>&nbsp;</div>
            <span>, to execute the authorization form and to arrange for the cremation and disposition of the cremated remains of the decedent. In addition, I am aware of no objection to this cremation by any spouse, child, parent, or sibling.</span>
            <div class="title">
                <h3>LIMITATION OF LIABILITY</h3>
            </div>
            <span>As the Authorizing Agent(s), I(we) hereby agree to indemnify, defend, and hold harmless B.V.C.C., its officers, agents and employees, of and from any and all claims, demands, causes of action, and suits of every kind, nature and description, in law or equity, including any legal fees, costs and expenses of litigation, arising as a result of, based upon or connected with this authorization, including the failure to properly identify the decedent or the human remains transmitted to B.V.C.C., the processing, shipping and final disposition of the decedent's cremated remains, the failure to take possession of or make proper arrangements for the final disposition of the cremated remains, any damage due to harmful or explodable implants, claims brought by any other person(s) claiming the right to control the disposition of the decedent or the decedent's cremated remains, or any other action performed by B.V.C.C., its officers, agents, or employees, pursuant to this authorization, excepting only acts of willful negligence. </span>
            <div class="initialRow">
                <span>INITIALS OF AUTHORIZING AGENT</span>
                <div class="fieldValue size-20"><?= @$formauth['init_4'] ?>&nbsp;</div>
            </div>
            <hr>
            <div class="title">
                <h3>SIGNATURE OF AUTHORIZING AGENT(S)</h3>
            </div>
            <span>THIS IS A LEGAL DOCUMENT, IT CONTAINS IMPORTANT PROVISIONS CONCERNING CREMATION. CREMATION IS IRREVERSIBLE AND FINAL. READ THIS DOCUMENT CAREFULLY BEFORE SIGNING.</span>
            <span>By executing this Cremation Authorization Form, as Authorizing Agent(s), the undersigned warrant that all representations and statements contained on this form are true and correct, that these statements were made to induce B.V.C.C. to cremate the human remains of the decedent, and that the undersigned have read and understand the provisions contained on this form and the document entitled "B.V.C.C. POLICIES, PROCEDURES AND REQUIREMENTS," and hereby authorize B.V.C.C. to perform the cremation of the decedent in accordance with that document.</span>
            <div>
                <span>Executed at</span>
                <div class="fieldValue size-46"><?= @$formauth['sa_executedat'] ?>&nbsp;</div>
                <span>this</span>
                <div class="fieldValue size-15"><?= @$formauth['sa_this'] ?>&nbsp;</div>
                <span>day of</span>
                <div class="fieldValue size-10"><?= @$formauth['sa_dayof_month'] ?>&nbsp;</div>
                <span>,20</span>
                <div class="fieldValue size-5"><?= substr(@$formauth['sa_dayof_year'], -2) ?>&nbsp;</div>
            </div>
            <!-- Person -->
            <div>
                <span>Name</span>
                <div class="fieldValue size-44"><?= @$formauth['sa_name'] ?>&nbsp;</div>
                <span><b>SIGNATURE</b></span>
                <div class="fieldValue size-40"><?= @$formauth['sa_name_again'] ?>&nbsp;</div>
            </div>
            <div>
                <span>Relationship to Decedent</span>
                <div class="fieldValue size-42"><?= @$formauth['sa_relationship'] ?>&nbsp;</div>
                <span>Phone number</span>
                <div class="fieldValue size-27"><?= @$formauth['sa_phone'] ?>&nbsp;</div>
            </div>
            <div>
                <span>Address</span>
                <?php
                $address = array();
                @$formauth['sa_address'] and $address[] = @$formauth['sa_address'];
                @$formauth['sa_city'] and $address[] = @$formauth['sa_city'] . ',';
                @$formauth['sa_state'] and $address[] = @$formauth['sa_state'];
                @$formauth['sa_zip'] and $address[] = @$formauth['sa_zip'];
                ?>
                <div class="fieldValue size-92"><?= implode(' ', $address) ?></div>
            </div>
            <!-- Person -->
            <div>
                <span>Name</span>
                <div class="fieldValue size-44">&nbsp;</div>
                <span><b>SIGNATURE</b></span>
                <div class="fieldValue size-40">&nbsp;</div>
            </div>
            <div>
                <span>Relationship to Decedent</span>
                <div class="fieldValue size-42">&nbsp;</div>
                <span>Phone number</span>
                <div class="fieldValue size-27">&nbsp;</div>
            </div>
            <div>
                <span>Address</span>
                <div class="fieldValue size-92">&nbsp;</div>
            </div>
        </div>

        <div class="content content2">
            <div class="mainTitle">
                <h3>B.V.C.C. POLICIES, PROCEDURES AND REQUIREMENTS</h3>
            </div>
            <span>The cremation, processing and disposition of the remains of the deceased shall be performed in accordance with all governing laws, and the policies, procedures and requirements of B.V.C.C. and the designated funeral home. This document describes many of the policies and requirements of B.V.C.C. and is incorporated in our Cremation Authorization Form. We suggest you take the time to read this document carefully before executing the Cremation Authorization Form. </span>
            <div class="mainTitle">
                <h3>B.V.C.C. REQUIREMENTS FOR CREMATION</h3>
            </div>
            <center>
                <span>Cremation will take place only after all the following conditions have been met.</span>
            </center>
            <div>1. Any scheduled ceremonies or viewings have been completed.</div>
            <div>2. Civil and medical authorities have issued all required permits.</div>
            <div>3. All necessary authorizations have been obtained, and no objections have been raised.</div>
            <div class="mainTitle">
                <h3>CASKETS/CONTAINERS</h3>
            </div>
            <span>B.V.C.C. does not accept metal caskets. In the interest of providing appropriate sanitation for staff, and respect for the deceased, all wooden caskets and alternative containers must meet the following standard; (1) be composed of materials suitable for cremation; (2) be able to be closed to provide a complete covering of the human remains; (3) be sufficient for handling with ease; and (4) be able to provide protection for the health and safety of crematory personnel.</span>
            <span>Many caskets that are comprised primarily of combustible material also contain some exterior parts, e.g., decorative handles or rails, that are not combustible and that may cause damage to the cremation equipment. B.V.C.C., at its sole discretion, reserves the right to remove these non-combustible materials prior to cremation and to discard them with similar materials from other cremations ans other refuse in a non-recoverable manner. </span>
            <div class="mainTitle">
                <h3>PACEMAKERS, PROSTHESES AND RADIOACTIVE DEVICES</h3>
            </div>
            <span>Pacemakers and prosthesis, as well as any other mechanical or radioactive devices or implants in the decedent, may create a hazardous condition when placed in the cremation chamber. It is imperative that pacemakers and radioactive devices be removed prior to cremation. If the funeral home is not notified about such devices and implants, and not instructed to remove them, then the person(s) authorizing the cremation will be responsible for any damages caused to B.V.C.C. or crematory personnel by such devices or implants. </span>
            <div class="mainTitle">
                <h3>THE CREMATION PROCESS</h3>
            </div>
            <span>All cremations are performed individually. Exceptions are only made in the case of close relatives, and then only with the prior express written instructions of the Authorizing Agent(s).</span>
            <span>The cremation of a human being is a process whereby the body is prepared for its ultimate and final disposition by earth burial, entombment, placement into a niche or in a garden within a cemetery, burial at sea, or holding and safekeeping by a family member or their designated representative</span>
            <span>The body is placed into a cremation casket or rigid container. This container with the body enclosed in it is placed inside a cremation chamber. The body is totally consumed (incinerated) by open flames and intense heat. The temperature will generally range between 1500 to 2200 degrees Fahrenheit. The soft tissues of the body are vaporized. The skeletal framework is reduced to bone fragments and particles (not ashes). These fragments and particles are called cremated remains. The process of cremation may take from three (3) to five (5) hours. During the cremation, the contents of the chamber may be moved to facilitate incineration. The chamber is composed of ceramic or other material which disintegrates slightly during each cremation. The product of that disintegration is commingled with the cremated remains.</span>
            <span>The cremated remains are collected from the cremation chamber by using technical and speciallzed equipment. Among these are a hoe, brooms and brushes, and a receiving pan. This equipment is solely used for the collection of cremated remains. The remains are cleaned of any metal or other foreign matter. The bone particles removed vary in size and need to be processed. This is done by pulverization in a specialized machine used only for this purpose, to render them to a size for placement in an urn.</span>
            <span>It is beyond anyone's capability to conserve or to collect every particle of crematied remains and dust. Inadvertent and unintentional commingling of cremated remains may occur. Due to the nature of the cremation process any personal possessions or valuable materials, such as dental gold or jewelry (as well as amy body prosthesis or dental bridgework), that are left with the decedent and not removed from the casket or container prior to cremation will be destroyed or if not destroyed, will be disposed of by B.V.C.C.</span>
            <span>Following the cooling period, the cremated remains, which will normally weigh several pounds in the case of an average size adult, are then swept from the cremation chamber. After the cremated remains are removed from the cremation chamber, all non-combustible materials (insofar as possible) such as bridgework, and materials from the casket or container, such as hinges, latches, nails, etc., will be separated and removed from the human bone fragments by visible or magnetic selection and will be disposed of by B.V.C.C. with similar materials from other cremations in a non-recoverable manner.</span>
            <span>When the cremated remains are removed from the cremation chamber, the skeletal reamins often contain recognizable bone fragments. After the bone fragments have been separated from the other material, they will then be mechanically processed (pulverized). This process of crushing may cause incidental commingling of the remains with the residue from the processing of previously cremated remains. These granulated particles of unidentifiable dimensions will be virtually unrecognizable as human remains.</span>
            <div class="mainTitle">
                <h3>URNS/CONTAINERS</h3>
            </div>
            <span>After the cremated remains have been processed, they will be placed in the designated urn or container. B.V.C.C. will make a reasonable effort to put all of the cremated remains in the urn or container, with the exception of dust or other residue that may remain on the processing equipment. In the event the client provided urn or container is insufficient to accommodate all of the cremated remains, the excess will be placed in a separate receptacle. The separate receptacle will be kept with the primary receptacle and handled according to the disposition instructions on the Cremation Authorization Form.</span>
            <div class="mainTitle">
                <h3>FINAL DISPOSITION</h3>
            </div>
            <span>Cremation is NOT final disposition, nor is placing the cremated remains in storage at a funeral home final disposition. The cremation process simply reduces the decedent's body to cremated remains. Some provision must be made for the final disposition of these cremated remains. Therefore, B.V.C.C. strongly suggests that arrangements for final disposition be made at the time the cremation arrangements are made and that the Cremation Authorization Form is completed.</span>
            <div class="mainTitle">
                <h3>LIMITATION OF LIABILITY</h3>
            </div>
            <span>The obligations of B.V.C.C. shall be limited to the cremation of the decedent and the disposition of the decedent's cremated remains as authorized on the Cremation Authorization Form. No Warranties, Express or Implied are made, and damages shall be limited to the amount of the cremation fee paid. </span>
        </div>

        <!--
        <pre>
        <?php //print_r($user_session);?>
        <?php //print_r($cfg);?>
        <?php //print_r($settings);?>
        </pre>
        -->
    </body>
</html>