<?= $header ?>
<form method="post" id="mainform" name="mainform" style="margin:0;">
    <input type="hidden" name="scriptaction" 	id="scriptaction"	value="validate" />
    <input type="hidden" name="notvalidate" 	id="notvalidate"	value="" />
    <input type="hidden" name="next_form" 		id="next_form"		value="<?= $next_form ?>" />
    <!--Data-->

    <input type="hidden" name="state"	 		id="state"			value="" />
    <input type="hidden" name="county"	 		id="county"			value="" />
    <input type="hidden" name="pkgid"	 		id="pkgid"			value="" />
    <style>
        .bammessage {
            color: #FAE16A;
            font-size: 15px;
            margin-left: 25px;
            margin-top: 5px;
            position: absolute;
        }
        .county-selection-info {
            clear: both;
            color: #FFFFFF;
            font-size: 14px;
            margin-top: -10px;
            position: absolute;
            right: 10px;
        }
        .gpl-view-link {
            display: block;
            float: right;
            font-size: 14px;
            margin-right: 10px;
            margin-top: 20px;
        }
        .gpl-view-link > a:link,
        .gpl-view-link > a:visited,
        .gpl-view-link > a:hover,
        .gpl-view-link > a:active{
            color: white;
        }
        .ea_county_asterisk {
            color: #FFFFFF;
            display: inline-block !important;
        }
        .clickable{
            cursor:pointer;
        }
        .options-check.clickable {
            float: left;
            margin-left: 8px;
            margin-right: 15px;
        }

        .bam-notification {
            background-color: #d9792f;
            color: white;
            padding: 10px 20px;
        }
    </style>
    <div id="ea" class="">
        <?php
        $this->load->view('message_badge');
        ?>

        <img class="img_check_no" src="<?= $root ?>/assets/images/btn-choose-options-check-no.jpg" style="display:none;">
        <img class="img_check_yes" src="<?= $root ?>/assets/images/btn-choose-options-check-yes.jpg" style="display:none;">
        <img class="img_option_no" src="<?= $root ?>/assets/images/btn-choose-options-no.jpg" style="display:none;">
        <img class="img_option_yes" src="<?= $root ?>/assets/images/btn-choose-options-yes.jpg" style="display:none;">

        <div class="ea-step-title-box">
            <?php
            if ($user_session['pkgtype'] == "preneed") {
                ?>
                <!-- <div class="bammessage">*Planning choices are required, but only a membership fee of $<?= $settings['member_fee'] ?> will be necessary to complete your membership registration.</div> -->
                <h1 class="ea-step-title">step 1: MAKE SELECTIONS</h1>
                <?php
            }else{
                ?>
                <h1 class="ea-step-title">step 1: Select LOCATION aND cremation package INFORMATION</h1>
                <?php
            }
            ?>
            <p>
                <?php
                if ($user_session['pkgtype'] == 'preneed') {
                    ?>
                    <span class="county-selection-info">
                        *County Selection Info
                    </span>
                    <?php
                }
                ?>
                <span class="gpl-view-link">
                    <a target="_blank" href="/files/9814/0545/6025/TriState-GPL-07.2014.pdf">View General Price List (PDF)</a>
                </span>
                <span class="ea-styled-select">
                    <select class="select_state validate[required]">
                        <option value="" selected>Select State</option>
                        <?php
                        foreach ($option_states as $skey => $state) {
                            ?>
                            <option value="<?= $skey ?>"><?= $skey ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </span>
                <?php
                foreach ($option_states as $skey => $state) {
                    $display = 'display:none;';
                    $class = '';
                    if (!@$fdisplay) {
                        $display = '';
                        $class = 'validate[required]';
                        $fdisplay = true;
                    }
                    ?>
                    <span class="ea-styled-select ea_county" style="<?= $display ?>">
                        <select class="select_county <?= $class ?>" data-state="<?= $skey ?>">
                            <option value="" selected>Select county</option>
                            <?php
                            foreach ($state as $county) {
                                ?>
                                <option value="<?= $county ?>"><?= $county ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </span>
                    <?php
                }
                if ($user_session['pkgtype'] == 'preneed') {
                    ?><span class="ea_county_asterisk" style="<?= $display ?>">*</span><?php
                }
                ?>

            </p>
        </div>

        <?php
        if ($user_session['pkgtype'] == "preneed") {
            ?>

            <div class="bam-notification">

                <!-- Please make your choices and you may pay only the $30 membership fee. <br/>You pay in full now to lock in August's promotional prices.-->
               <!-- While you may pay only $30 at this time to Become a Member to receive future <br/>Membership pricing, you must pay in full now to receive our promotional pricing.-->
               You may pay only $30 at this time to Become a Member, or you may pay in full to lock in today’s Member price.
            </div>
            <?php
        } else{
        ?>
            <!--
            <div class="bam-notification">
                You may pay in full now to lock in today's promotional prices.
            </div>-->
        <?php
        }
        ?>

        <div id="content-ea" <?php /* ?>style="visibility:hidden; display:block; height:120px; overflow:hidden;"<?php */ ?>>
            <div id="ea-content-inner">
                <?php
                if ($user_session['pkgtype'] == 'atneed') {
                    ?>
                   <!--  <div class="is_member_checkbox clickable" style="margin-bottom: 20px;">
                        Are you currently a member?
                        <div class="options-check clickable <?php//= @$user_session['is_member'] ? 'option-yes' : '' ?>">
                            <input type="checkbox" value="1" name="is_member" class="checkboximage2 is_member" style="display: none;">
                        </div>
                    </div> -->
                    <?php
                }
                ?>
                <span class="package_alert_pointer" style="position: absolute; left: 60px;"></span>
                <div class="ea-choose-options-box">
                    <?php
                    foreach ($packages as $pkg => $package) {
                        if ($pkg == 1)
                            continue;
                        ?>
                        <div class="ea-choose-options-box-inner package_opt" data-pkg="<?= $pkg ?>" data-price="<?= $package['price'] ?>" data-member-price="<?= $package['member_price'] ?>">
                            <div class="ea-choose-options-select clickable"><input class="checkboximage pkgid" type="checkbox" value="<?= $pkg ?>" /></div>
                            <div class="ea-choose-options-text clickable"><?= $package['name'] ?></div>
                            <?php
                            if (@$user_session['is_member']) {
                                ?>
                                <div class="ea-choose-options-price">$<?= number_format($package['member_price'], 2) ?></div>
                                <?php
                            } else {
                                ?>
                                <div class="ea-choose-options-price">$<?= number_format($package['price'], 2) ?></div>
                                <?php
                            }
                            ?>
                            <div class="ea-choose-options-view-details"><a href="#" onclick="toggle('description_<?= $pkg ?>', this);
                                    return false;">View Details</a></div>
                        </div>
                        <div class="package-include-box" id="description_<?= $pkg ?>" style="display:none;" >
                            <?= $package['description'] ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="merch_providing_urn_checkbox clickable" style="margin-bottom: 20px;">
                    <div class="options-check clickable <?= @is_array(@$user_session['merch_providing_urn']) ? 'option-yes' : '' ?>">
                        <input type="checkbox"
                               name="merch_providing_urn"
                               class="checkboximage2 merch_providing_urn"
                               value="1"
                               style="display: none;"
                               />
                    </div>
                    All packages include a cremation urn.
                    You may also provide your own urn.
                    Will you be providing the cremation urn?
                </div>
                <div class="ea-choose-additional-options-box">
                    <div class="ea-sub-title">CHOOSE ADDITIONAL OPTIONS</div>
                    <?php
                    foreach ($services as $sid => $service) {
                        $show_in = array();
                        foreach ($service['show_in'] as $pkg) {
                            $show_in[] = 'show_in_' . $pkg;
                        }
                        ?>
                        <div class="ea-choose-additional-options-box-inner service_opt <?= implode(' ', $show_in) ?>" data-id="<?= $sid ?>">
                            <div class="ea-choose-additional-options-select clickable"><input class="checkboximage2 optid" name="service[<?= $sid ?>]" type="checkbox" value="<?= $sid ?>" /></div>
                            <div class="ea-choose-additional-options-text clickable"><?= $service['name'] ?></div>
                            <div class="ea-choose-additional-options-price">$<?= number_format($service['price'], 2) ?></div>
                            <?php
                            if ($service['description']) {
                                ?>
                                <div class="ea-choose-additional-options-view-details"><a class="service_opt_<?= $sid ?>" href="<?= $cfg['root'] ?>/#service_desc_<?= $sid ?>">View Details</a></div>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>

            <?php
            $this->load->view('_right_bar');
            ?>

        </div>

        <div style="visibility:hidden; position:absolute; left:-2000px; top:-2000px;">
            <?php
            foreach ($services as $sid => $service) {
                ?>
                <div id="service_desc_<?= $sid ?>">
                    <?= $service['description'] ?>
                </div>
                <script>
                    $(function() {
                        $(".service_opt_<?= $sid ?>").colorbox({inline: true, href: '#service_desc_<?= $sid ?>', width: 700, heihgt: 500, scrolling: false});
                    });
                </script>
                <?php
            }
            ?>
        </div>

    </div>
    <div style="display: none;">
        <div class="county-selection-balloon" style="width: 600px; font-size: 14px; padding: 5px;">Only serving these preselected States and Counties at this time. Should the deceased move outside our service area prior to time of death, we will be unable to render services.</div>
        <div class="gpl-view-balloon" style="width: 600px; font-size: 14px; padding: 5px;">FTC regulations govern the content of this General Price List, however, Tri-State Cremation Society does not provide all services listed as we are solely a cremation society providing specific cremation services.</div>
    </div>
    <script>
        var isBam = <?= $isBam ? '1' : '0' ?>;
    </script>
    <script src="<?= $cfg['root'] ?>/assets/js/packages.js"></script>
    <script>
        $(function() {
<?php
if ($user_session['pkgtype'] == 'preneed') {
    ?>
                $('.county-selection-info').balloon({
                    contents: $('.county-selection-balloon').clone(),
                    offsetX: -230,
                    position: 'top',
                    css: {
                        opacity: "1",
                        backgroundColor: "#EFE9E5"
                    }
                });
                $('.gpl-view-link a').balloon({
                    contents: $('.gpl-view-balloon').clone(),
                    offsetX: -210,
                    position: 'bottom',
                    css: {
                        opacity: "1",
                        backgroundColor: "#EFE9E5"
                    }
                });
    <?php
} else {
    ?>
                $('.gpl-view-link a').balloon({
                    contents: $('.gpl-view-balloon').clone(),
                    offsetX: -210,
                    css: {
                        opacity: "1",
                        backgroundColor: "#EFE9E5"
                    }
                });
    <?php
}
?>
            $('.service_opt').each(function() {
                if (!$(this).hasClass('show_in_4')) {
                    //$(this).hide();
                }
            });
<?php
if (@$user_session['stateselected']) {
    ?>
                $('.select_state').val('<?= $user_session['stateselected'] ?>').change();
    <?php
    if (@$user_session['countyselected']) {
        ?>
                    $('.select_county[data-state="<?= $user_session['stateselected'] ?>"]').val('<?= $user_session['countyselected'] ?>').change();
        <?php
    }
}
if (@$user_session['packageselected']['id']) {
    ?>
                $('input#pkgid').val('<?= $user_session['packageselected']['id'] ?>');
                check_pkg(<?= $user_session['packageselected']['id'] ?>, true);
    <?php
}
if (is_array(@$user_session['serviceselected'])) {
    foreach ($user_session['serviceselected'] as $sid => $s) {
        ?>
                    check_opt(<?= $sid ?>, true);
        <?php
    }
}
?>

            if ($('.is_member_checkbox .options-check').hasClass('option-yes')) {
                $('.is_member_checkbox .options-check img').attr('src', '/earrangement/assets/images/btn-choose-options-yes.jpg');
            } else {
                $('.is_member_checkbox .options-check img').attr('src', '/earrangement/assets/images/btn-choose-options-no.jpg');
            }
            if ($('.merch_providing_urn_checkbox .options-check').hasClass('option-yes')) {
                $('.merch_providing_urn_checkbox .options-check img').attr('src', '/earrangement/assets/images/btn-choose-options-yes.jpg');
            } else {
                $('.merch_providing_urn_checkbox .options-check img').attr('src', '/earrangement/assets/images/btn-choose-options-no.jpg');
            }
        });
    </script>
    <!--Data-->
</form>
<?php
echo $footer?>