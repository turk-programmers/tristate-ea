<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Earrangement_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_settings() {
        $query = $this->db->order_by('var_name')->get('ea_setting');
        $settings = array();
        foreach ($query->result_array() as $setting) {
            if ($setting['var_type'] == 'ARRAY') {
                eval('$settings[$setting["var_name"]] = ' . $setting["var_value"] . ';');
            } else {
                $settings[$setting['var_name']] = $setting['var_value'];
            }
        }
        return $settings;
    }

    function get_packages($pkgtype) {
        $this->db->order_by('price_'.$pkgtype.' asc');
        $query = $this->db->get('packages');
        $items = array();
        foreach ($query->result_array() as $item) {
            $price = $item['price_atneed'];
            if ($pkgtype == 'preneed') {
                $price = $item['price_preneed'];
            }
            $item['price'] = $price;
            $item['member_price'] = $item['price_preneed'];

            unset($item['price_preneed']);
            unset($item['price_atneed']);
            $items[$item['id']] = $item;
        }
        return $items;
    }

    function get_services() {
        $query = $this->db->where('show_in !=', '')->get('services');
        $items = array();
        foreach ($query->result_array() as $item) {
            $item['show_in'] = explode(',', $item['show_in']);
            $items[$item['id']] = $item;
        }
        return $items;
    }

    function get_merch_items() {
        $query = $this->db
                ->where('type', 'Urn')
                ->or_where('type', 'UrnVaults')
                ->or_where('type', 'Jewelry')
                ->or_where('type', 'Keepsake')
                ->or_where('type', 'MemoryGlass')
                ->or_where('type', 'Thumbies')
                ->or_where('type', 'MemorialPackageStationery')
                ->or_where('type', 'Flower')
                ->order_by('price');
        $query = $this->db->get('products');
        $items = array();
        foreach ($query->result_array() as $item) {
            $items[$item['pid']] = $item;
        }
        return $items;
    }

    /* function get_urns(){
      $query = $this->db
      ->select('pid,type,subtype')
      ->where('type', 'Urn')
      ->or_where('type', 'UrnVaults')
      ->order_by('price');
      $query = $this->db->get('products');
      $items = array();
      foreach($query->result_array() as $item){
      $items[$item['pid']] = $item;
      }
      return $items;
      }
      function get_keepsakes(){
      $query = $this->db
      ->select('pid,type,subtype')
      ->where('type', 'Jewelry')
      //->or_where('type', 'MemoryGlass')
      //->or_where('type', 'Thumbies')
      ->or_where('type', 'Keepsake')
      ->order_by('price');
      $query = $this->db->get('products');
      $items = array();
      foreach($query->result_array() as $item){
      $items[$item['pid']] = $item;
      }
      return $items;
      }
      function get_memorial_package_stationaries(){
      $query = $this->db
      ->select('pid,type,subtype')
      ->where('type', 'MemorialPackageStationery')
      ->order_by('price');
      $query = $this->db->get('products');
      $items = array();
      foreach($query->result_array() as $item){
      $items[$item['pid']] = $item;
      }
      return $items;
      }
      function get_flowers(){
      $query = $this->db
      ->select('pid,type,subtype')
      ->where('type', 'Flower')
      ->order_by('price');
      $query = $this->db->get('products');
      $items = array();
      foreach($query->result_array() as $item){
      $items[$item['pid']] = $item;
      }
      return $items;
      } */

    function get_products($group = false) {
        switch (strtolower($group)) {
            case 'urn':
                $this->db
                        ->select('pid,type,subtype')
                        ->where('type', 'Urn')
                        ->order_by('price')
                        ->order_by('name')
                ;
                break;
            case 'keepsake':
                $this->db
                        ->select('pid,type,subtype')
                        ->where('type', 'Keepsake')
                        ->order_by('price')
                        ->order_by('name')
                ;
                break;
            case 'jewelry':
                $this->db
                        ->select('pid,type,subtype')
                        ->where('type', 'Jewelry')
                        ->order_by('price')
                //->order_by('FIELD(pid,40,42,44,47,41,43,45,48)', '', false)
                ;
                break;
            case 'memorial':
                $this->db
                        ->select('pid,type,subtype')
                        ->where('type', 'MemorialPackageStationery')
                        ->order_by('price');
                break;
            case 'flower':
                $this->db
                        ->select('pid,type,subtype')
                        ->where('type', 'Flower')
                        //->order_by('if(subtype = "Plant", 0, if(subtype="Basket",1,if(subtype = "Standing", 2, 3 ))), price');
                        ->order_by("FIELD(subtype, 'Plant', 'Basket', 'Standing', 'Urn' )", '', FALSE);
                break;
            default:
                $this->db
                        ->select('pid,type,subtype')
                        ->order_by('price');
                break;
        }
        $query = $this->db->get('products');
        $items = array();
        foreach ($query->result_array() as $item) {
            $items[$item['pid']] = $item;
        }
        return $items;
    }

    function get_product_by_id($id) {
        $query = $this->db
                ->where('pid', $id)
                ->limit(1)
                ->get('products');
        $item = $query->result_array();
        return $item[0];
    }

    function get_state() {
        $states = array();
        $states['AL'] = array("code" => "AL", "name" => "Alabama", "timezone" => "America/Chicago");
        $states['AK'] = array("code" => "AK", "name" => "Alaska", "timezone" => "America/Anchorage");
        $states['AZ'] = array("code" => "AZ", "name" => "Arizona", "timezone" => "America/Phoenix");
        $states['AR'] = array("code" => "AR", "name" => "Arkansas", "timezone" => "America/Chicago");
        $states['CA'] = array("code" => "CA", "name" => "California", "timezone" => "America/Los_Angeles");
        $states['CO'] = array("code" => "CO", "name" => "Colorado", "timezone" => "America/Denver");
        $states['CT'] = array("code" => "CT", "name" => "Connecticut", "timezone" => "America/New_York");
        $states['DE'] = array("code" => "DE", "name" => "Delaware", "timezone" => "America/New_York");
        $states['FL'] = array("code" => "FL", "name" => "Florida", "timezone" => "America/New_York");
        $states['GA'] = array("code" => "GA", "name" => "Georgia", "timezone" => "America/New_York");
        $states['HI'] = array("code" => "HI", "name" => "Hawaii", "timezone" => "Pacific/Honolulu");
        $states['ID'] = array("code" => "ID", "name" => "Idaho", "timezone" => "America/Denver");
        $states['IL'] = array("code" => "IL", "name" => "Illinois", "timezone" => "America/Chicago");
        $states['IN'] = array("code" => "IN", "name" => "Indiana", "timezone" => "America/Indianapolis");
        $states['IA'] = array("code" => "IA", "name" => "Iowa", "timezone" => "America/Chicago");
        $states['KS'] = array("code" => "KS", "name" => "Kansas", "timezone" => "America/Chicago");
        $states['KY'] = array("code" => "KY", "name" => "Kentucky", "timezone" => "America/New_York");
        $states['LA'] = array("code" => "LA", "name" => "Louisiana", "timezone" => "America/Chicago");
        $states['ME'] = array("code" => "ME", "name" => "Maine", "timezone" => "America/New_York");
        $states['MD'] = array("code" => "MD", "name" => "Maryland", "timezone" => "America/New_York");
        $states['MA'] = array("code" => "MA", "name" => "Massachusetts", "timezone" => "America/New_York");
        $states['MI'] = array("code" => "MI", "name" => "Michigan", "timezone" => "America/New_York");
        $states['MN'] = array("code" => "MN", "name" => "Minnesota", "timezone" => "America/Chicago");
        $states['MS'] = array("code" => "MS", "name" => "Mississippi", "timezone" => "America/Chicago");
        $states['MO'] = array("code" => "MO", "name" => "Missouri", "timezone" => "America/Chicago");
        $states['MT'] = array("code" => "MT", "name" => "Montana", "timezone" => "America/Denver");
        $states['NE'] = array("code" => "NE", "name" => "Nebraska", "timezone" => "America/Chicago");
        $states['NV'] = array("code" => "NV", "name" => "Nevada", "timezone" => "America/Los_Angeles");
        $states['NH'] = array("code" => "NH", "name" => "New Hampshire", "timezone" => "America/New_York");
        $states['NJ'] = array("code" => "NJ", "name" => "New Jersey", "timezone" => "America/New_York");
        $states['NM'] = array("code" => "NM", "name" => "New Mexico", "timezone" => "America/Denver");
        $states['NY'] = array("code" => "NY", "name" => "New York", "timezone" => "America/New_York");
        $states['NC'] = array("code" => "NC", "name" => "North Carolina", "timezone" => "America/New_York");
        $states['ND'] = array("code" => "ND", "name" => "North Dakota", "timezone" => "America/Chicago");
        $states['OH'] = array("code" => "OH", "name" => "Ohio", "timezone" => "America/New_York");
        $states['OK'] = array("code" => "OK", "name" => "Oklahoma", "timezone" => "America/Chicago");
        $states['OR'] = array("code" => "OR", "name" => "Oregon", "timezone" => "America/Los_Angeles");
        $states['PA'] = array("code" => "PA", "name" => "Pennsylvania", "timezone" => "America/New_York");
        $states['RI'] = array("code" => "RI", "name" => "Rhode Island", "timezone" => "America/New_York");
        $states['SC'] = array("code" => "SC", "name" => "South Carolina", "timezone" => "America/New_York");
        $states['SD'] = array("code" => "SD", "name" => "South Dakota", "timezone" => "America/Chicago");
        $states['TN'] = array("code" => "TN", "name" => "Tennessee", "timezone" => "America/Chicago");
        $states['TX'] = array("code" => "TX", "name" => "Texas", "timezone" => "America/Chicago");
        $states['UT'] = array("code" => "UT", "name" => "Utah", "timezone" => "America/Denver");
        $states['VT'] = array("code" => "VT", "name" => "Vermont", "timezone" => "America/New_York");
        $states['VA'] = array("code" => "VA", "name" => "Virginia", "timezone" => "America/New_York");
        $states['WA'] = array("code" => "WA", "name" => "Washington", "timezone" => "America/Los_Angeles");
        $states['WV'] = array("code" => "WV", "name" => "West Virginia", "timezone" => "America/New_York");
        $states['WI'] = array("code" => "WI", "name" => "Wisconsin", "timezone" => "America/Chicago");
        $states['WY'] = array("code" => "WY", "name" => "Wyoming", "timezone" => "America/Denver");
        return $states;
    }

    /*
     * Order archive functions
     */

    function get_order_by_id($id) {
        $query = $this->db
                ->where('id', $id)
                ->limit(1)
                ->get('order_archive');
        $item = $query->result_array();
        return $item[0];
    }

    function get_order_by_hash($hash) {
        $query = $this->db
                ->where('hash', $hash)
                ->limit(1)
                ->get('order_archive');
        $item = $query->result_array();
        return $item[0];
    }

    function add_order_archive($data) {
        $this->db->insert('order_archive', $data);
        return $this->db->insert_id();
    }

    function update_order_archive($data, $id) {
        $this->db
                ->where('id', $id)
                ->update('order_archive', $data);
    }

    function get_last_uniq_id() {
        $query = $this->db
                ->where('uniq like "' . date('y') . '%"')
                ->order_by('uniq', 'desc')
                ->limit(1)
                ->get('order_archive');
        $item = $query->result_array();
        if (@$item[0]['uniq']) {
            return $item[0]['uniq'];
        } else {
            return false;
        }
    }

    /*
     * Test function
     */

    function set_limit($limit = false) {
        $this->test_limit = $limit;
    }

    function turn_test_mode($on = false) {
        $this->db
                ->where('var_name', 'test_mode')
                ->update('ea_setting', array(
                    'var_value' => $on ? '1' : '0',
        ));
    }

}
